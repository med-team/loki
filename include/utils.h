#ifndef _UTILS_H_
#define _UTILS_H_

#include <time.h>

#ifndef WORD_BIT
#define WORD_BIT (sizeof(int)<<3)
#endif

#ifndef LONG_BIT
#define LONG_BIT (sizeof(long)<<3)
#endif

#define   READ	   0
#define   WRITE	1

#define DEFAULT_FILE_PREFIX "loki"

#define UTL_NULL_POINTER 1
#define UTL_NO_MEM 2
#define UTL_BAD_STAT 3
#define UTL_BAD_DIR 4
#define UTL_BAD_PERM 5
#define UTL_MAX_ERR 5

typedef struct {
	time_t start_time;
	double extra_time,extra_stime,extra_utime;
} loki_time;

const char *FMsg,*IntErr,*MMsg,*AbMsg;
int from_abt;

void abt(const char *, const int, const char *, ...);
char *copy_string(const char *);
char *make_file_name( const char *);
void print_start_time(const char *,const char *,char *,loki_time *);
void print_end_time(void);
int mystrcmp(const char *, const char *);
void qstrip(char *);
char **tokenize(char *,const int);
void gen_perm(int *,int);
void gnu_qsort(void *const,size_t,size_t,int(*)(const void *,const void *));
int txt_print_double(double,FILE *);
int txt_get_double(char *,char **,double *);
int set_file_prefix(const char *);
int set_file_dir(const char *);
const char *utl_error(int);
char *add_file_dir(const char *);

#define ABT_FUNC(msg) abt(__FILE__,__LINE__,"%s(): %s",FUNC_NAME,msg)

#endif
