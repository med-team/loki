#ifndef _LIBHDR_H_
#define _LIBHDR_H_

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

extern int child_open(const int,const char *,const char *);
extern int child_open1(const int,const char *,const char *,const char *);
#ifndef HAVE_POPEN
FILE *popen(const char *,const char *);
#endif
extern int getseed(const char *,const int);
int writeseed(const char *,const int);
int dumpseed(FILE *,const int);
int mkbackup(const char *,int);
int bindumpseed(FILE *);
int binreadseed(FILE *,char *);
char *my_strsep(char **,const char *);

extern double cumchic(double,int);
extern double cumchn(double,int,double);
extern double critchi(double,int);
extern double critchn(double,int,double);

#define cumchi(x,df) (1.0-cumchic(x,df))
#define cnorm(x) (.5*(1+erf(x/sqrt(2.0))))

#endif
