/* A Bison parser, made from control_parse.y, by GNU bison 1.75.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON	1

/* Pure parsers.  */
#define YYPURE	0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     FILEC = 258,
     MARKER = 259,
     LOCUS = 260,
     TRAIT = 261,
     RANDOM = 262,
     PEDIGREE = 263,
     LOG = 264,
     MODEL = 265,
     FILTER = 266,
     LINK = 267,
     MISSING = 268,
     FACTOR = 269,
     BREAK = 270,
     DOLOOP = 271,
     WHILE = 272,
     USE = 273,
     WHERE = 274,
     ORSYMBOL = 275,
     ANDSYMBOL = 276,
     NEQSYMBOL = 277,
     LEQSYMBOL = 278,
     GEQSYMBOL = 279,
     NOTSYMBOL = 280,
     LOGICAL = 281,
     SHELL = 282,
     ARRAY = 283,
     PRINTEXP = 284,
     INCLUDE = 285,
     RAWOUTPUT = 286,
     LOOP_CLAUSE_START = 287,
     LOOP_CLAUSE_END = 288,
     CONSTANT = 289,
     MULTIPLE = 290,
     RSFORMAT = 291,
     FSFORMAT = 292,
     SKIPFORMAT = 293,
     GSFORMAT = 294,
     CENSORED = 295,
     GROUP = 296,
     SET = 297,
     GENDER = 298,
     AFFECTED = 299,
     OUTPUT = 300,
     ERRORDIR = 301,
     LAUROUTPUT = 302,
     UNAFFECTED = 303,
     POSITION = 304,
     FREQUENCY = 305,
     STRING = 306,
     VARIABLE = 307,
     ASSIGN = 308,
     ARRAY_VAR = 309,
     INTEGER = 310,
     SYSTEM_VAR = 311,
     REAL = 312,
     UMINUS = 313
   };
#endif
#define FILEC 258
#define MARKER 259
#define LOCUS 260
#define TRAIT 261
#define RANDOM 262
#define PEDIGREE 263
#define LOG 264
#define MODEL 265
#define FILTER 266
#define LINK 267
#define MISSING 268
#define FACTOR 269
#define BREAK 270
#define DOLOOP 271
#define WHILE 272
#define USE 273
#define WHERE 274
#define ORSYMBOL 275
#define ANDSYMBOL 276
#define NEQSYMBOL 277
#define LEQSYMBOL 278
#define GEQSYMBOL 279
#define NOTSYMBOL 280
#define LOGICAL 281
#define SHELL 282
#define ARRAY 283
#define PRINTEXP 284
#define INCLUDE 285
#define RAWOUTPUT 286
#define LOOP_CLAUSE_START 287
#define LOOP_CLAUSE_END 288
#define CONSTANT 289
#define MULTIPLE 290
#define RSFORMAT 291
#define FSFORMAT 292
#define SKIPFORMAT 293
#define GSFORMAT 294
#define CENSORED 295
#define GROUP 296
#define SET 297
#define GENDER 298
#define AFFECTED 299
#define OUTPUT 300
#define ERRORDIR 301
#define LAUROUTPUT 302
#define UNAFFECTED 303
#define POSITION 304
#define FREQUENCY 305
#define STRING 306
#define VARIABLE 307
#define ASSIGN 308
#define ARRAY_VAR 309
#define INTEGER 310
#define SYSTEM_VAR 311
#define REAL 312
#define UMINUS 313




/* Copy the first part of user declarations.  */
#line 1 "control_parse.y"

/****************************************************************************
 *                                                                          *
 *     Loki - Programs for genetic analysis of complex traits using MCMC    *
 *                                                                          *
 *             Simon Heath - University of Washington                       *
 *                                                                          *
 *                       March 1997                                         *
 *                                                                          *
 * control_parse.y:                                                         *
 *                                                                          *
 * yacc source for control file parser.                                     *
 *                                                                          *
 * Copyright (C) Simon C. Heath 1997, 2000, 2002                            *
 * This is free software.  You can distribute it and/or modify it           *
 * under the terms of the Modified BSD license, see the file COPYING        *
 *                                                                          *
 ****************************************************************************/

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#ifdef USE_DMALLOC
#include <dmalloc.h>
#endif
#include <stdio.h>
#include <math.h>
#include <ctype.h>

#include "utils.h"
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#ifndef YYMAXDEPTH
#define YYMAXDEPTH 0
#endif
#ifndef __GNUC__
#define __GNUC__ 0
#endif



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#ifndef YYSTYPE
#line 44 "control_parse.y"
typedef union {
	char *string;
	struct bin_node *var;
	int value;
	double rvalue;
	struct format_clause *format_clause;
	struct fformat *fformat;
	struct format_atom *f_atom;
	struct model_list *model_list;
	struct var_list *var_list;
	struct var_element *element;
	struct express *express;
} yystype;
/* Line 193 of /usr/local/share/bison/yacc.c.  */
#line 245 "y.tab.c"
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif

#ifndef YYLTYPE
typedef struct yyltype
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} yyltype;
# define YYLTYPE yyltype
# define YYLTYPE_IS_TRIVIAL 1
#endif

/* Copy the second part of user declarations.  */
#line 90 "control_parse.y"

#include "scan.h"
#include "scanner.h"

static struct format_atom *make_f_atom(int,int);
static struct format_clause *add_f_atom(struct format_clause *,struct format_atom *);
static struct format_clause *add_f_list(struct format_clause *,struct format_clause *,int);
static struct format *setup_format(struct format_clause *);
static struct bin_node *create_var(char *);
static struct model_list *add_to_model(struct model_list *,struct var_list *);
static struct var_list *add_to_var_list(struct var_list *,struct bin_node *,struct express *);
static struct var_list *add_var_lists(struct var_list *,struct var_list *);
static struct var_element *get_element(struct bin_node *,struct express *);
static struct var_element *assign_var(struct bin_node *,struct express *,struct express *);
static struct express *alloc_express(void);
static struct express *do_express_op(struct express *,struct express *,int);
static struct express *do_logical_op(struct express *,struct express *,int);
static struct fformat *add_fformat(struct fformat *,struct fformat *);
static struct fformat *create_fformat(void *,int);
static void begin_looping(struct var_element *, struct express *, struct express *);
static void free_vlist(struct var_list *);
static void do_ped_com(struct var_list *);
static void add_restriction(struct var_list *);
static void add_censored(struct var_element *,const int);
static void do_file_com(char *,struct format_clause *,struct fformat *,struct var_list *);
static void set_locus_array(struct bin_node *);
static void set_locus_element(struct var_element *);
static void set_haplo_element(struct var_element *,struct var_element *);
static void do_link_com(char *s,int type, struct var_list *);
static void do_missing_com(struct express *,struct var_list *,char *);
static void change_type(int,struct var_list *);
static void do_model_com(struct model_list *,struct bin_node *,struct express *);
static void add_operation(void *,int,int);
static void set_array_var(struct scan_data *,struct express *);
static void check_element_add_op(struct var_element *);
static void enter_loop(void);
static void start_loopclause(void);
static void do_while_com(struct express *);
static void print_exp(struct express *);
static void new_command(void);
static void set_sex(struct var_element *,struct express *,struct express *);
static void set_group(struct var_element *);
static int count_var_list(struct var_list *),shell_flag;
static struct format_atom *f_atom_list;
static struct var_element *pedlist[4];
static int f_atom_n,f_atom_size=32,pedflag;
struct operation *Affected,*Unaffected;
struct bin_node *root_var;
struct InFile *Infiles;
struct Link *links;
struct Miss *Miss;
struct Restrict *Restrictions;
struct Censor *Censored;
struct model *Models;
static struct operation *Op_List;
struct Marker *markers,*traitlocus;
static struct var_element *hap_list[2];
struct express *sex_exp[2];
struct sex_def *sex_def;
struct var_element *group_elem;
static char *string_copy(char *s1,char *s2);
static char *LogFile;
	
int scan_error,scan_error_n,scan_warn_n;
int max_scan_errors=30,max_scan_warnings=30,n_markers,iflag,file_skip;
char *Filter,*ErrorDir,*rsformat,*fsformat,*gsformat,*OutputFile,*OutputRawFile,*OutputLaurFile;
int loop_level,loop_ptr[MAX_LOOP],loop_stat[MAX_LOOP],loop_record,loop_stack_size=256;
int loop_main_ptr,in_loopclause,loop_clause_end,loop_clause_step,loop_clause_ptr;
int syst_var[NUM_SYSTEM_VAR];
int family_id;
struct var_element *loop_clause_element;
struct token_store *loop_stack;
	  


/* Line 213 of /usr/local/share/bison/yacc.c.  */
#line 340 "y.tab.c"

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYLTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];	\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  133
#define YYLAST   838

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  75
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  58
/* YYNRULES -- Number of rules. */
#define YYNRULES  195
/* YYNRULES -- Number of states. */
#define YYNSTATES  388

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   313

#define YYTRANSLATE(X) \
  ((unsigned)(X) <= YYMAXUTOK ? yytranslate[X] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      67,    68,    63,    61,    69,    62,    64,    65,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    72,
      59,    58,    60,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    70,     2,    71,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      73,    74,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    66
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short yyprhs[] =
{
       0,     0,     3,     5,     7,    10,    14,    18,    20,    22,
      24,    26,    28,    30,    32,    34,    36,    38,    40,    42,
      44,    46,    48,    50,    52,    54,    56,    58,    60,    62,
      64,    66,    68,    70,    74,    81,    83,    85,    87,    89,
      93,    97,   101,   105,   108,   112,   116,   120,   122,   123,
     127,   134,   140,   146,   152,   157,   159,   163,   167,   171,
     175,   179,   183,   187,   191,   194,   197,   199,   203,   206,
     209,   212,   215,   218,   223,   230,   237,   243,   250,   255,
     257,   259,   261,   263,   267,   271,   275,   279,   283,   286,
     290,   294,   298,   302,   306,   310,   314,   318,   321,   326,
     334,   342,   344,   349,   351,   355,   360,   367,   369,   373,
     377,   380,   383,   386,   389,   391,   394,   396,   399,   401,
     404,   407,   410,   413,   416,   419,   423,   428,   434,   437,
     441,   445,   448,   451,   454,   457,   460,   463,   465,   470,
     475,   478,   483,   486,   489,   494,   502,   505,   509,   513,
     515,   517,   521,   525,   529,   533,   535,   537,   539,   541,
     546,   548,   553,   555,   559,   562,   564,   566,   570,   576,
     580,   582,   586,   588,   597,   608,   613,   615,   616,   618,
     620,   625,   627,   629,   633,   637,   639,   641,   645,   649,
     651,   655,   659,   661,   663,   667
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short yyrhs[] =
{
      76,     0,    -1,    77,    -1,     1,    -1,    76,    15,    -1,
      76,    15,    77,    -1,    76,    15,     1,    -1,    98,    -1,
     113,    -1,    83,    -1,   104,    -1,   108,    -1,   109,    -1,
     110,    -1,   112,    -1,   114,    -1,   107,    -1,    95,    -1,
      96,    -1,    78,    -1,    93,    -1,    90,    -1,    82,    -1,
      88,    -1,    85,    -1,    86,    -1,   115,    -1,    81,    -1,
      92,    -1,    87,    -1,   105,    -1,   106,    -1,    79,    -1,
      53,    58,    80,    -1,    53,    67,    80,    68,    58,    80,
      -1,    57,    -1,    55,    -1,    51,    -1,   120,    -1,    80,
      61,    80,    -1,    80,    62,    80,    -1,    80,    63,    80,
      -1,    80,    65,    80,    -1,    62,    80,    -1,    67,    80,
      68,    -1,    42,    56,    55,    -1,    42,   118,    55,    -1,
      16,    -1,    -1,    30,    84,   132,    -1,    40,   120,    19,
      67,    97,    68,    -1,    44,    19,    67,    97,    68,    -1,
      48,    19,    67,    97,    68,    -1,    43,   120,    80,    69,
      80,    -1,    17,    67,    89,    68,    -1,    80,    -1,    89,
      58,    89,    -1,    89,    22,    89,    -1,    89,    23,    89,
      -1,    89,    24,    89,    -1,    89,    59,    89,    -1,    89,
      60,    89,    -1,    89,    20,    89,    -1,    89,    21,    89,
      -1,    25,    89,    -1,    29,    91,    -1,    80,    -1,    91,
      69,    80,    -1,    36,   132,    -1,    37,   132,    -1,    39,
     132,    -1,    38,    55,    -1,    28,    94,    -1,    52,    67,
      80,    68,    -1,    94,    69,    52,    67,    80,    68,    -1,
      18,   130,    19,    67,    97,    68,    -1,    18,    19,    67,
      97,    68,    -1,    19,    67,    97,    68,    18,   130,    -1,
      19,    67,    97,    68,    -1,    55,    -1,    57,    -1,    51,
      -1,   120,    -1,    97,    61,    97,    -1,    97,    62,    97,
      -1,    97,    63,    97,    -1,    97,    65,    97,    -1,    67,
      97,    68,    -1,    62,    97,    -1,    97,    58,    97,    -1,
      97,    22,    97,    -1,    97,    23,    97,    -1,    97,    24,
      97,    -1,    97,    59,    97,    -1,    97,    60,    97,    -1,
      97,    20,    97,    -1,    97,    21,    97,    -1,    25,    97,
      -1,     3,    99,    69,   129,    -1,     3,    70,   100,    71,
      99,    69,   129,    -1,     3,    70,   101,    71,    99,    69,
     129,    -1,   132,    -1,    27,    67,   132,    68,    -1,   103,
      -1,   100,    69,   103,    -1,    55,    67,   100,    68,    -1,
     100,    69,    55,    67,   100,    68,    -1,   102,    -1,   101,
      69,   102,    -1,   101,    72,   102,    -1,    37,   132,    -1,
      36,   132,    -1,    39,   132,    -1,    38,    55,    -1,    55,
      -1,    55,    73,    -1,    73,    -1,    55,     1,    -1,     1,
      -1,     9,   132,    -1,    45,   132,    -1,    47,   132,    -1,
      31,   132,    -1,    46,   132,    -1,    13,    80,    -1,    13,
      80,   130,    -1,    13,    80,    69,   130,    -1,    13,    70,
     132,    71,    80,    -1,     8,   130,    -1,     4,     5,   121,
      -1,     6,     5,   130,    -1,    34,   130,    -1,    35,   130,
      -1,     7,   130,    -1,    14,   130,    -1,    57,   130,    -1,
      55,   130,    -1,    12,    -1,    12,    70,    73,    71,    -1,
      12,    70,    74,    71,    -1,   111,   130,    -1,   111,   131,
      69,   130,    -1,   111,   131,    -1,    11,   132,    -1,    10,
     118,    58,   116,    -1,    10,    54,    67,    80,    68,    58,
     116,    -1,    41,   120,    -1,   116,    61,   119,    -1,   116,
      61,   117,    -1,   117,    -1,   119,    -1,   119,    63,   119,
      -1,   119,    64,   119,    -1,   117,    63,   119,    -1,   117,
      64,   119,    -1,    52,    -1,    43,    -1,   118,    -1,    54,
      -1,    54,    67,    80,    68,    -1,   118,    -1,    54,    67,
      80,    68,    -1,   122,    -1,   121,    69,   122,    -1,   120,
     123,    -1,   120,    -1,    54,    -1,    70,   120,    71,    -1,
      70,   120,    69,   120,    71,    -1,    70,     1,    71,    -1,
     119,    -1,   124,    69,   119,    -1,    67,    -1,   125,   124,
      69,    15,    79,    69,    80,    68,    -1,   125,   124,    69,
      15,    79,    69,    80,    69,    80,    68,    -1,   126,    32,
     124,    33,    -1,   126,    -1,    -1,   118,    -1,    54,    -1,
      54,    67,    80,    68,    -1,   128,    -1,   127,    -1,   129,
      69,   128,    -1,   129,    69,   127,    -1,   119,    -1,   127,
      -1,   130,    69,   119,    -1,   130,    69,   127,    -1,    51,
      -1,   131,    61,    51,    -1,   120,    61,   131,    -1,    51,
      -1,   120,    -1,   132,    61,    51,    -1,   132,    61,   120,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short yyrline[] =
{
       0,   167,   167,   168,   169,   170,   171,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   198,   201,   204,   205,   208,   209,   210,   211,   213,
     214,   215,   216,   217,   218,   221,   222,   225,   228,   228,
     231,   234,   235,   238,   241,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,   256,   259,   260,   263,   264,
     265,   266,   269,   271,   272,   275,   276,   279,   280,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   293,
     294,   295,   296,   297,   298,   299,   300,   301,   304,   305,
     306,   309,   310,   313,   314,   315,   316,   319,   320,   321,
     324,   325,   326,   327,   330,   331,   332,   333,   334,   337,
     341,   342,   343,   346,   350,   351,   352,   353,   356,   359,
     360,   363,   364,   365,   366,   367,   368,   371,   372,   373,
     376,   377,   378,   381,   389,   390,   393,   396,   397,   398,
     399,   402,   403,   404,   405,   408,   409,   412,   413,   414,
     417,   418,   421,   421,   423,   424,   425,   428,   429,   430,
     433,   434,   437,   440,   441,   444,   445,   448,   449,   450,
     451,   454,   455,   456,   457,   460,   461,   462,   463,   466,
     467,   468,   471,   472,   473,   474
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "FILEC", "MARKER", "LOCUS", "TRAIT", 
  "RANDOM", "PEDIGREE", "LOG", "MODEL", "FILTER", "LINK", "MISSING", 
  "FACTOR", "BREAK", "DOLOOP", "WHILE", "USE", "WHERE", "ORSYMBOL", 
  "ANDSYMBOL", "NEQSYMBOL", "LEQSYMBOL", "GEQSYMBOL", "NOTSYMBOL", 
  "LOGICAL", "SHELL", "ARRAY", "PRINTEXP", "INCLUDE", "RAWOUTPUT", 
  "LOOP_CLAUSE_START", "LOOP_CLAUSE_END", "CONSTANT", "MULTIPLE", 
  "RSFORMAT", "FSFORMAT", "SKIPFORMAT", "GSFORMAT", "CENSORED", "GROUP", 
  "SET", "GENDER", "AFFECTED", "OUTPUT", "ERRORDIR", "LAUROUTPUT", 
  "UNAFFECTED", "POSITION", "FREQUENCY", "STRING", "VARIABLE", "ASSIGN", 
  "ARRAY_VAR", "INTEGER", "SYSTEM_VAR", "REAL", "'='", "'<'", "'>'", 
  "'+'", "'-'", "'*'", "'.'", "'/'", "UMINUS", "'('", "')'", "','", "'['", 
  "']'", "';'", "'x'", "'y'", "$accept", "comfile", "command", 
  "assigncommand", "assignment", "expression", "setcommand", "docommand", 
  "includecommand", "@1", "censorcommand", "affectedcommand", 
  "sexcommand", "whilecommand", "condition", "printcommand", "printlist", 
  "defformatcommand", "arraycommand", "arraylist", "usecommand", 
  "wherecommand", "res_condition", "filecommand", "filename_string", 
  "formatlist", "fformatlist", "fformat", "format", "logcommand", 
  "outputcommand", "errordircommand", "missingcommand", "pedcommand", 
  "locicommand", "changetypecommand", "linkcom1", "linkcommand", 
  "filtercommand", "modelcommand", "groupcommand", "modellist", 
  "interactionlist", "variable", "single_vlist", "single_element", 
  "locuslist", "locus", "lociclause", "simple_varlist", "open_bracket", 
  "loop_clause1", "loop_clause", "fsingle_vlist", "filevarlist", 
  "varlist", "complex_string1", "complex_string", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,    61,    60,
      62,    43,    45,    42,    46,    47,   313,    40,    41,    44,
      91,    93,    59,   120,   121
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    75,    76,    76,    76,    76,    76,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    78,    79,    79,    80,    80,    80,    80,    80,
      80,    80,    80,    80,    80,    81,    81,    82,    84,    83,
      85,    86,    86,    87,    88,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    90,    91,    91,    92,    92,
      92,    92,    93,    94,    94,    95,    95,    96,    96,    97,
      97,    97,    97,    97,    97,    97,    97,    97,    97,    97,
      97,    97,    97,    97,    97,    97,    97,    97,    98,    98,
      98,    99,    99,   100,   100,   100,   100,   101,   101,   101,
     102,   102,   102,   102,   103,   103,   103,   103,   103,   104,
     105,   105,   105,   106,   107,   107,   107,   107,   108,   109,
     109,   110,   110,   110,   110,   110,   110,   111,   111,   111,
     112,   112,   112,   113,   114,   114,   115,   116,   116,   116,
     116,   117,   117,   117,   117,   118,   118,   119,   119,   119,
     120,   120,   121,   121,   122,   122,   122,   123,   123,   123,
     124,   124,   125,   126,   126,   127,   127,   128,   128,   128,
     128,   129,   129,   129,   129,   130,   130,   130,   130,   131,
     131,   131,   132,   132,   132,   132
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     1,     2,     3,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     6,     1,     1,     1,     1,     3,
       3,     3,     3,     2,     3,     3,     3,     1,     0,     3,
       6,     5,     5,     5,     4,     1,     3,     3,     3,     3,
       3,     3,     3,     3,     2,     2,     1,     3,     2,     2,
       2,     2,     2,     4,     6,     6,     5,     6,     4,     1,
       1,     1,     1,     3,     3,     3,     3,     3,     2,     3,
       3,     3,     3,     3,     3,     3,     3,     2,     4,     7,
       7,     1,     4,     1,     3,     4,     6,     1,     3,     3,
       2,     2,     2,     2,     1,     2,     1,     2,     1,     2,
       2,     2,     2,     2,     2,     3,     4,     5,     2,     3,
       3,     2,     2,     2,     2,     2,     2,     1,     4,     4,
       2,     4,     2,     2,     4,     7,     2,     3,     3,     1,
       1,     3,     3,     3,     3,     1,     1,     1,     1,     4,
       1,     4,     1,     3,     2,     1,     1,     3,     5,     3,
       1,     3,     1,     8,    10,     4,     1,     0,     1,     1,
       4,     1,     1,     3,     3,     1,     1,     3,     3,     1,
       3,     3,     1,     1,     3,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       0,     3,     0,     0,     0,     0,     0,     0,     0,     0,
     137,     0,     0,    47,     0,     0,     0,     0,     0,    48,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       2,    19,    32,    27,    22,     9,    24,    25,    29,    23,
      21,    28,    20,    17,    18,     7,    10,    30,    31,    16,
      11,    12,    13,     0,    14,     8,    15,    26,     0,   156,
     192,   155,     0,     0,     0,   160,   193,   101,     0,     0,
     158,   172,   157,   185,     0,   176,   186,   133,   128,   119,
       0,     0,   143,     0,    37,    36,    35,     0,     0,     0,
     124,    38,   134,     0,     0,     0,     0,     0,    72,    66,
      65,     0,   122,   131,   132,    68,    69,    71,    70,     0,
     146,     0,     0,     0,     0,   120,   123,   121,     0,     0,
       0,   136,   135,     1,     0,   189,   158,   157,     0,   140,
     142,     0,     0,   118,     0,     0,     0,     0,     0,   116,
       0,     0,   107,   103,   177,     0,   166,   165,   129,   162,
     130,     0,   170,     0,     0,     0,     0,     0,     0,     0,
      43,     0,     0,     0,     0,     0,     0,     0,   125,     0,
      55,     0,     0,     0,     0,    81,    79,    80,     0,     0,
       0,    82,     0,     0,     0,    49,     0,    45,    46,     0,
       0,     0,    33,     0,     6,     5,     0,     0,     0,     0,
       0,     0,   111,   110,   113,   112,   117,     0,   115,     0,
       0,     0,     0,     0,   179,   178,   182,   181,    98,   194,
     195,     0,   164,     0,     0,     0,     0,   187,   188,     0,
     144,   149,   150,   138,   139,    44,     0,    39,    40,    41,
      42,   126,    64,     0,     0,     0,     0,     0,     0,     0,
       0,    54,     0,     0,    97,    88,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    78,
       0,     0,    67,     0,     0,     0,     0,     0,     0,   191,
     190,   141,   102,   161,     0,     0,   104,     0,   108,     0,
     109,     0,   177,     0,     0,   163,   159,     0,   171,   175,
       0,     0,     0,     0,     0,     0,     0,   127,    62,    63,
      57,    58,    59,    56,    60,    61,    76,     0,    87,    95,
      96,    90,    91,    92,    89,    93,    94,    83,    84,    85,
      86,     0,    73,     0,     0,    53,    51,    52,     0,   159,
     105,     0,   177,   177,     0,   184,   183,   169,     0,   167,
       0,     0,   148,   147,   153,   154,   151,   152,    75,    77,
       0,    50,    34,     0,    99,   100,   180,     0,     0,   145,
      74,   106,   168,     0,   173,     0,     0,   174
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short yydefgoto[] =
{
      -1,    39,    40,    41,    42,   180,    43,    44,    45,   111,
      46,    47,    48,    49,   181,    50,   110,    51,    52,   108,
      53,    54,   190,    55,    74,   150,   151,   152,   153,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,   240,   241,    75,    83,   101,   158,   159,   232,   163,
      84,    85,    86,   227,   228,    87,   140,    77
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -216
static const short yypact[] =
{
     541,  -216,   -22,    -1,    22,    94,    94,   202,    71,   202,
     -54,   658,    94,  -216,   -34,    -9,     9,    -2,   715,  -216,
     202,    94,    94,   202,   202,    25,   202,   106,   106,   -17,
     106,    65,   202,   202,   202,    89,    43,    94,    94,     8,
    -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,
    -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,
    -216,  -216,  -216,   224,  -216,  -216,  -216,  -216,    23,  -216,
    -216,  -216,    62,    36,    70,  -216,  -216,    82,   156,    94,
      86,  -216,  -216,  -216,   234,   127,  -216,    98,    98,    82,
     115,   114,    82,   105,  -216,  -216,  -216,   715,   715,   202,
     694,  -216,    98,    90,   125,   -18,   318,   131,   134,   316,
     151,   202,    82,    98,    98,    82,    82,  -216,    82,   208,
    -216,   177,   192,   715,   199,    82,    82,    82,   203,   715,
     715,    98,    98,  -216,   421,  -216,   212,   191,   222,    98,
     -41,   202,   715,  -216,   202,   202,   229,   202,    10,  -216,
      61,   -25,  -216,  -216,   142,   282,    62,   217,   220,  -216,
      98,   715,  -216,   225,   234,    94,   715,   234,   242,   244,
    -216,   454,   -49,   715,   715,   715,   715,    94,    98,    90,
     316,   684,   318,   226,   318,  -216,  -216,  -216,   318,   318,
     423,  -216,   715,   243,   715,    82,   254,  -216,  -216,   321,
     318,   318,   316,   656,  -216,  -216,   715,   302,   275,    94,
      66,   713,    82,    82,  -216,    82,  -216,    12,  -216,    16,
     207,   272,   207,   272,   256,  -216,  -216,  -216,   258,  -216,
    -216,    59,  -216,   156,   722,   123,   -27,  -216,  -216,   730,
     269,     1,   152,  -216,  -216,  -216,   715,    91,    91,  -216,
    -216,    98,  -216,    90,    90,    90,    90,    90,    90,    90,
      90,  -216,   542,   318,  -216,  -216,   591,   318,   318,   318,
     318,   318,   318,   318,   318,   318,   318,   318,   318,   313,
     738,   268,   316,   318,   715,   602,   613,   280,   746,  -216,
    -216,    98,  -216,  -216,    48,    53,  -216,   273,  -216,   277,
    -216,   715,   142,   284,   150,  -216,  -216,   295,  -216,  -216,
     234,   293,   234,   234,   234,   234,   234,   316,   481,    46,
     165,  -216,  -216,   165,  -216,  -216,  -216,   624,  -216,   449,
     470,   181,   462,   462,   181,   462,   462,   163,   163,  -216,
    -216,    94,  -216,   715,   673,   316,  -216,  -216,   715,   296,
    -216,    12,   142,   142,   754,  -216,  -216,  -216,   106,  -216,
     289,   234,     1,   152,  -216,  -216,  -216,  -216,  -216,    98,
     762,  -216,   316,   118,   258,   258,  -216,   292,   715,   269,
    -216,  -216,  -216,   336,  -216,   715,   770,  -216
};

/* YYPGOTO[NTERM-NUM].  */
static const short yypgoto[] =
{
    -216,  -216,   230,  -216,    64,   -11,  -216,  -216,  -216,  -216,
    -216,  -216,  -216,  -216,  -160,  -216,  -216,  -216,  -216,  -216,
    -216,  -216,   140,  -216,    11,  -215,  -216,    42,   141,  -216,
    -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,  -216,
    -216,     5,    56,   185,     4,    29,  -216,   143,  -216,   227,
    -216,  -216,  -151,   100,  -115,     3,   189,   248
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, parse error.  */
#define YYTABLE_NINF -162
static const short yytable[] =
{
     100,   183,   294,   226,    78,    68,   309,   109,   133,    88,
     104,   216,   155,   143,   238,   102,    93,   143,   105,   252,
     208,    69,   246,   134,   113,   114,    69,    79,   209,    70,
      71,    76,    72,   103,    69,    71,    76,   143,    76,   121,
     131,   132,   310,    71,   221,    80,   222,   223,    73,    76,
     107,   165,    76,    76,   216,    76,   119,   120,    81,   123,
     303,    76,    76,    76,   313,   314,   139,   148,   255,   256,
     257,   295,   144,   145,   146,   147,   106,   217,  -114,  -114,
     117,  -114,   160,   218,   124,   149,   170,   171,   162,   149,
     141,   148,   138,   318,   319,   320,   321,   322,   323,   324,
     325,   129,    69,   178,   258,   259,   260,   157,   128,   149,
     130,    71,   199,    72,    69,   179,   350,   219,   202,   203,
     351,  -114,  -114,    71,  -114,    90,   218,   155,    76,   142,
     219,   211,   220,    69,   292,   191,   373,    69,   307,   154,
      76,    94,    71,   155,    72,    95,    71,    96,    80,    69,
     234,   355,    97,   161,   175,   239,   176,    98,    71,   164,
      72,    81,   247,   248,   249,   250,    69,   165,   162,   237,
      76,   242,   167,    76,    76,    71,    76,    80,   168,   169,
     251,   280,   166,   282,   230,    69,   381,   219,   256,   257,
      82,    82,   182,    91,    71,   288,   224,    82,   192,    69,
      82,   226,   226,   193,   270,   271,    82,    82,    71,    81,
     156,   191,   291,   191,   122,   315,   316,   191,   191,   358,
     194,   359,    82,    82,   259,   260,   277,   196,   278,   191,
     191,   297,   197,   299,    68,   317,   138,   374,   375,   308,
     273,   274,   275,   276,   277,    69,   278,   198,   137,    76,
      69,    76,  -160,    70,    71,    89,    72,    92,    70,    71,
     304,    72,   157,   298,    82,   300,   200,    69,   112,    82,
     201,   115,   116,   345,   118,   135,    71,    69,   136,   206,
     125,   126,   127,   207,   214,    82,    71,   231,    80,   233,
     354,    81,   191,   263,   235,   281,   191,   191,   191,   191,
     191,   191,   191,   191,   191,   191,   191,   191,   144,   145,
     146,   147,   191,   243,   308,   244,   363,   364,   365,   366,
     367,   283,   262,   301,   264,    69,   290,   302,   265,   266,
     312,   341,   370,   229,    71,   343,    72,   372,   348,   225,
     285,   286,   352,   184,   369,    69,   353,   172,    36,    82,
      82,   361,    82,   135,    71,   357,    72,  -161,   378,   195,
     296,    69,    82,   382,   205,   242,   379,   383,   362,   185,
      71,   360,    72,   186,   386,   187,   305,   173,   174,   175,
     188,   176,   173,   174,   175,   189,   176,   377,     0,   210,
     284,   236,   212,   213,    82,   215,   289,   173,   174,   175,
       0,   176,   356,   327,   384,   385,     0,   329,   330,   331,
     332,   333,   334,   335,   336,   337,   338,   339,   340,     0,
      82,    -4,   204,   344,     2,     3,     0,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -4,    13,    14,    15,
      16,     0,     0,   267,   268,   269,   270,   271,     0,    17,
      18,    19,    20,     0,     0,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
     268,   269,   270,   271,    36,     0,    37,     0,    38,     0,
       0,   272,   273,   274,   275,   276,   277,   225,   278,     0,
       0,   279,   269,   270,   271,    82,     0,    82,    82,    82,
      82,    82,   254,   255,   256,   257,     0,   272,   273,   274,
     275,   276,   277,     0,   278,   173,   174,   175,     0,   176,
       0,     0,   245,   275,   276,   277,    82,   278,   272,   273,
     274,   275,   276,   277,     0,   278,     0,   225,   225,   258,
     259,   260,     1,     0,     2,     3,    82,     4,     5,     6,
       7,     8,     9,    10,    11,    12,     0,    13,    14,    15,
      16,     0,   267,   268,   269,   270,   271,     0,     0,    17,
      18,    19,    20,     0,     0,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
       0,     0,     0,     0,    36,     0,    37,     0,    38,     0,
     272,   273,   274,   275,   276,   277,     0,   278,     0,     0,
     326,   267,   268,   269,   270,   271,     0,     0,     0,     0,
       0,     0,   267,   268,   269,   270,   271,     0,     0,     0,
       0,     0,     0,   267,   268,   269,   270,   271,     0,     0,
       0,     0,     0,     0,   267,   268,   269,   270,   271,   272,
     273,   274,   275,   276,   277,     0,   278,     0,     0,   328,
     272,   273,   274,   275,   276,   277,     0,   278,     0,     0,
     346,   272,   273,   274,   275,   276,   277,     0,   278,     0,
       0,   347,   272,   273,   274,   275,   276,   277,     0,   278,
       0,     0,   368,   267,   268,   269,   270,   271,     0,     0,
       0,    69,     0,     0,   253,   254,   255,   256,   257,    94,
      71,     0,    72,    95,     0,    96,     0,   173,   174,   175,
      97,   176,     0,     0,   287,    98,     0,     0,    99,     0,
       0,   272,   273,   274,   275,   276,   277,    69,   278,     0,
       0,   371,   258,   259,   260,     0,    71,     0,    80,     0,
       0,     0,   261,     0,     0,   173,   174,   175,    69,   176,
       0,    81,     0,   177,     0,     0,    94,    71,     0,    72,
      95,     0,    96,     0,   173,   174,   175,    97,   176,     0,
       0,   293,    98,   173,   174,   175,     0,   176,     0,     0,
     306,   173,   174,   175,     0,   176,     0,     0,   311,   173,
     174,   175,     0,   176,     0,     0,   342,   173,   174,   175,
       0,   176,     0,     0,   349,   173,   174,   175,     0,   176,
       0,     0,   376,   173,   174,   175,     0,   176,     0,     0,
     380,   173,   174,   175,     0,   176,     0,     0,   387
};

static const short yycheck[] =
{
      11,    19,   217,   154,     5,    27,    33,    18,     0,     6,
      19,     1,    61,     1,   165,    12,    70,     1,    15,   179,
      61,    43,    71,    15,    21,    22,    43,     5,    69,    51,
      52,     2,    54,    67,    43,    52,     7,     1,     9,    56,
      37,    38,    69,    52,    69,    54,    71,    72,    70,    20,
      52,    69,    23,    24,     1,    26,    27,    28,    67,    30,
       1,    32,    33,    34,    63,    64,    63,    55,    22,    23,
      24,    55,    36,    37,    38,    39,    67,    67,    68,    69,
      55,    71,    79,    73,    19,    73,    97,    98,    84,    73,
      67,    55,    63,   253,   254,   255,   256,   257,   258,   259,
     260,    58,    43,   100,    58,    59,    60,    78,    19,    73,
      67,    52,   123,    54,    43,    25,    68,    69,   129,   130,
      67,    68,    69,    52,    71,    54,    73,    61,    99,    67,
      69,   142,    71,    43,    68,   106,   351,    43,    15,    69,
     111,    51,    52,    61,    54,    55,    52,    57,    54,    43,
     161,   302,    62,    67,    63,   166,    65,    67,    52,    32,
      54,    67,   173,   174,   175,   176,    43,    69,   164,   165,
     141,   167,    58,   144,   145,    52,   147,    54,    73,    74,
     177,   192,    67,   194,   155,    43,    68,    69,    23,    24,
       5,     6,    67,     8,    52,   206,    54,    12,    67,    43,
      15,   352,   353,    69,    23,    24,    21,    22,    52,    67,
      54,   182,   209,   184,    29,    63,    64,   188,   189,    69,
      69,    71,    37,    38,    59,    60,    63,    19,    65,   200,
     201,   220,    55,   222,    27,   246,   207,   352,   353,   235,
      59,    60,    61,    62,    63,    43,    65,    55,    63,   220,
      43,   222,    61,    51,    52,     7,    54,     9,    51,    52,
     231,    54,   233,   221,    79,   223,    67,    43,    20,    84,
      67,    23,    24,   284,    26,    51,    52,    43,    54,    67,
      32,    33,    34,    61,    55,   100,    52,    70,    54,    69,
     301,    67,   263,    67,    69,    52,   267,   268,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,    36,    37,
      38,    39,   283,    71,   310,    71,   312,   313,   314,   315,
     316,    67,   182,    67,   184,    43,    51,    69,   188,   189,
      61,    18,   343,    51,    52,    67,    54,   348,    58,   154,
     200,   201,    69,    25,   341,    43,    69,    99,    53,   164,
     165,    58,   167,    51,    52,    71,    54,    61,    69,   111,
     219,    43,   177,    71,   134,   361,   361,   378,   312,    51,
      52,   307,    54,    55,   385,    57,   233,    61,    62,    63,
      62,    65,    61,    62,    63,    67,    65,   358,    -1,   141,
      69,   164,   144,   145,   209,   147,   207,    61,    62,    63,
      -1,    65,   302,   263,    68,    69,    -1,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,    -1,
     235,     0,     1,   283,     3,     4,    -1,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    -1,    -1,    20,    21,    22,    23,    24,    -1,    28,
      29,    30,    31,    -1,    -1,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      21,    22,    23,    24,    53,    -1,    55,    -1,    57,    -1,
      -1,    58,    59,    60,    61,    62,    63,   302,    65,    -1,
      -1,    68,    22,    23,    24,   310,    -1,   312,   313,   314,
     315,   316,    21,    22,    23,    24,    -1,    58,    59,    60,
      61,    62,    63,    -1,    65,    61,    62,    63,    -1,    65,
      -1,    -1,    68,    61,    62,    63,   341,    65,    58,    59,
      60,    61,    62,    63,    -1,    65,    -1,   352,   353,    58,
      59,    60,     1,    -1,     3,     4,   361,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    -1,    16,    17,    18,
      19,    -1,    20,    21,    22,    23,    24,    -1,    -1,    28,
      29,    30,    31,    -1,    -1,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      -1,    -1,    -1,    -1,    53,    -1,    55,    -1,    57,    -1,
      58,    59,    60,    61,    62,    63,    -1,    65,    -1,    -1,
      68,    20,    21,    22,    23,    24,    -1,    -1,    -1,    -1,
      -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,
      -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,    58,
      59,    60,    61,    62,    63,    -1,    65,    -1,    -1,    68,
      58,    59,    60,    61,    62,    63,    -1,    65,    -1,    -1,
      68,    58,    59,    60,    61,    62,    63,    -1,    65,    -1,
      -1,    68,    58,    59,    60,    61,    62,    63,    -1,    65,
      -1,    -1,    68,    20,    21,    22,    23,    24,    -1,    -1,
      -1,    43,    -1,    -1,    20,    21,    22,    23,    24,    51,
      52,    -1,    54,    55,    -1,    57,    -1,    61,    62,    63,
      62,    65,    -1,    -1,    68,    67,    -1,    -1,    70,    -1,
      -1,    58,    59,    60,    61,    62,    63,    43,    65,    -1,
      -1,    68,    58,    59,    60,    -1,    52,    -1,    54,    -1,
      -1,    -1,    68,    -1,    -1,    61,    62,    63,    43,    65,
      -1,    67,    -1,    69,    -1,    -1,    51,    52,    -1,    54,
      55,    -1,    57,    -1,    61,    62,    63,    62,    65,    -1,
      -1,    68,    67,    61,    62,    63,    -1,    65,    -1,    -1,
      68,    61,    62,    63,    -1,    65,    -1,    -1,    68,    61,
      62,    63,    -1,    65,    -1,    -1,    68,    61,    62,    63,
      -1,    65,    -1,    -1,    68,    61,    62,    63,    -1,    65,
      -1,    -1,    68,    61,    62,    63,    -1,    65,    -1,    -1,
      68,    61,    62,    63,    -1,    65,    -1,    -1,    68
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,     1,     3,     4,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    16,    17,    18,    19,    28,    29,    30,
      31,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    53,    55,    57,    76,
      77,    78,    79,    81,    82,    83,    85,    86,    87,    88,
      90,    92,    93,    95,    96,    98,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,    27,    43,
      51,    52,    54,    70,    99,   118,   120,   132,     5,     5,
      54,    67,   118,   119,   125,   126,   127,   130,   130,   132,
      54,   118,   132,    70,    51,    55,    57,    62,    67,    70,
      80,   120,   130,    67,    19,   130,    67,    52,    94,    80,
      91,    84,   132,   130,   130,   132,   132,    55,   132,   120,
     120,    56,   118,   120,    19,   132,   132,   132,    19,    58,
      67,   130,   130,     0,    15,    51,    54,   118,   120,   130,
     131,    67,    67,     1,    36,    37,    38,    39,    55,    73,
     100,   101,   102,   103,    69,    61,    54,   120,   121,   122,
     130,    67,   119,   124,    32,    69,    67,    58,    73,    74,
      80,    80,   132,    61,    62,    63,    65,    69,   130,    25,
      80,    89,    67,    19,    25,    51,    55,    57,    62,    67,
      97,   120,    67,    69,    69,   132,    19,    55,    55,    80,
      67,    67,    80,    80,     1,    77,    67,    61,    61,    69,
     132,    80,   132,   132,    55,   132,     1,    67,    73,    69,
      71,    69,    71,    72,    54,   118,   127,   128,   129,    51,
     120,    70,   123,    69,    80,    69,   124,   119,   127,    80,
     116,   117,   119,    71,    71,    68,    71,    80,    80,    80,
      80,   130,    89,    20,    21,    22,    23,    24,    58,    59,
      60,    68,    97,    67,    97,    97,    97,    20,    21,    22,
      23,    24,    58,    59,    60,    61,    62,    63,    65,    68,
      80,    52,    80,    67,    69,    97,    97,    68,    80,   131,
      51,   130,    68,    68,   100,    55,   103,    99,   102,    99,
     102,    67,    69,     1,   120,   122,    68,    15,   119,    33,
      69,    68,    61,    63,    64,    63,    64,    80,    89,    89,
      89,    89,    89,    89,    89,    89,    68,    97,    68,    97,
      97,    97,    97,    97,    97,    97,    97,    97,    97,    97,
      97,    18,    68,    67,    97,    80,    68,    68,    58,    68,
      68,    67,    69,    69,    80,   127,   128,    71,    69,    71,
      79,    58,   117,   119,   119,   119,   119,   119,    68,   130,
      80,    68,    80,   100,   129,   129,    68,   120,    69,   116,
      68,    68,    71,    80,    68,    69,    80,    68
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrlab1

/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)           \
  Current.first_line   = Rhs[1].first_line;      \
  Current.first_column = Rhs[1].first_column;    \
  Current.last_line    = Rhs[N].last_line;       \
  Current.last_column  = Rhs[N].last_column;
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#define YYLEX	yylex ()

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*-----------------------------.
| Print this symbol on YYOUT.  |
`-----------------------------*/

static void
#if defined (__STDC__) || defined (__cplusplus)
yysymprint (FILE* yyout, int yytype, YYSTYPE yyvalue)
#else
yysymprint (yyout, yytype, yyvalue)
    FILE* yyout;
    int yytype;
    YYSTYPE yyvalue;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvalue;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyout, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyout, yytoknum[yytype], yyvalue);
# endif
    }
  else
    YYFPRINTF (yyout, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyout, ")");
}
#endif /* YYDEBUG. */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
#if defined (__STDC__) || defined (__cplusplus)
yydestruct (int yytype, YYSTYPE yyvalue)
#else
yydestruct (yytype, yyvalue)
    int yytype;
    YYSTYPE yyvalue;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvalue;

  switch (yytype)
    {
      default:
        break;
    }
}



/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
int yyparse (void *);
# else
int yyparse (void);
# endif
#endif


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of parse errors so far.  */
int yynerrs;


int
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with.  */

  if (yychar <= 0)		/* This means end of input.  */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more.  */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

      /* We have to keep this `#if YYDEBUG', since we use variables
	 which are defined only if `YYDEBUG' is set.  */
      YYDPRINTF ((stderr, "Next token is "));
      YYDSYMPRINT ((stderr, yychar1, yylval));
      YYDPRINTF ((stderr, "\n"));
    }

  /* If the proper action on seeing token YYCHAR1 is to reduce or to
     detect an error, take that action.  */
  yyn += yychar1;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yychar1)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];



#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn - 1, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] >= 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif
  switch (yyn)
    {
        case 2:
#line 167 "control_parse.y"
    { new_command(); }
    break;

  case 3:
#line 168 "control_parse.y"
    { new_command(); }
    break;

  case 5:
#line 170 "control_parse.y"
    { new_command(); }
    break;

  case 6:
#line 171 "control_parse.y"
    { new_command(); }
    break;

  case 32:
#line 201 "control_parse.y"
    {}
    break;

  case 33:
#line 204 "control_parse.y"
    { yyval.element=assign_var(yyvsp[-2].var,0,yyvsp[0].express); if(yyvsp[0].express) free(yyvsp[0].express);}
    break;

  case 34:
#line 205 "control_parse.y"
    { yyval.element=assign_var(yyvsp[-5].var,yyvsp[-3].express,yyvsp[0].express); if(yyvsp[-3].express) free(yyvsp[-3].express); if(yyvsp[0].express) free(yyvsp[0].express);}
    break;

  case 35:
#line 208 "control_parse.y"
    { yyval.express=alloc_express(); yyval.express->type=ST_REAL; yyval.express->arg.rvalue=yyvsp[0].rvalue; }
    break;

  case 36:
#line 209 "control_parse.y"
    { yyval.express=alloc_express(); yyval.express->type=ST_INTEGER; yyval.express->arg.value=yyvsp[0].value; }
    break;

  case 37:
#line 210 "control_parse.y"
    { yyval.express=alloc_express(); yyval.express->type=ST_STRING; yyval.express->arg.string=yyvsp[0].string; }
    break;

  case 38:
#line 211 "control_parse.y"
    { yyval.express=alloc_express(); yyval.express->type=yyvsp[0].element->type; if(yyvsp[0].element->type==ST_STRING) yyval.express->arg.string=string_copy(0,yyvsp[0].element->arg.string);
		  else yyval.express->arg=yyvsp[0].element->arg; }
    break;

  case 39:
#line 213 "control_parse.y"
    { yyval.express=do_express_op(yyvsp[-2].express,yyvsp[0].express,'+'); }
    break;

  case 40:
#line 214 "control_parse.y"
    { yyval.express=do_express_op(yyvsp[-2].express,yyvsp[0].express,'-'); }
    break;

  case 41:
#line 215 "control_parse.y"
    { yyval.express=do_express_op(yyvsp[-2].express,yyvsp[0].express,'*'); }
    break;

  case 42:
#line 216 "control_parse.y"
    { yyval.express=do_express_op(yyvsp[-2].express,yyvsp[0].express,'/'); }
    break;

  case 43:
#line 217 "control_parse.y"
    { yyval.express=do_express_op(yyvsp[0].express,0,'-'); }
    break;

  case 44:
#line 218 "control_parse.y"
    { yyval.express=yyvsp[-1].express; }
    break;

  case 45:
#line 221 "control_parse.y"
    { syst_var[yyvsp[-1].value]=yyvsp[0].value; }
    break;

  case 46:
#line 222 "control_parse.y"
    { yyerror("Unrecognized system variable"); }
    break;

  case 47:
#line 225 "control_parse.y"
    { enter_loop(); }
    break;

  case 48:
#line 228 "control_parse.y"
    {iflag=1;}
    break;

  case 49:
#line 228 "control_parse.y"
    {include_control_file(yyvsp[0].string);}
    break;

  case 50:
#line 231 "control_parse.y"
    { add_censored(yyvsp[-4].element,1); at_use=0;}
    break;

  case 51:
#line 234 "control_parse.y"
    { add_censored(0,0); at_use=0;}
    break;

  case 52:
#line 235 "control_parse.y"
    { add_censored(0,2); at_use=0;}
    break;

  case 53:
#line 238 "control_parse.y"
    { set_sex(yyvsp[-3].element,yyvsp[-2].express,yyvsp[0].express); }
    break;

  case 54:
#line 241 "control_parse.y"
    { do_while_com(yyvsp[-1].express); if(yyvsp[-1].express) free(yyvsp[-1].express);}
    break;

  case 56:
#line 245 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,'=');}
    break;

  case 57:
#line 246 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,NEQSYMBOL);}
    break;

  case 58:
#line 247 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,LEQSYMBOL);}
    break;

  case 59:
#line 248 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,GEQSYMBOL);}
    break;

  case 60:
#line 249 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,'<');}
    break;

  case 61:
#line 250 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,'>');}
    break;

  case 62:
#line 251 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,ORSYMBOL);}
    break;

  case 63:
#line 252 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[-2].express,yyvsp[0].express,ANDSYMBOL);}
    break;

  case 64:
#line 253 "control_parse.y"
    {yyval.express=do_logical_op(yyvsp[0].express,0,NOTSYMBOL);}
    break;

  case 65:
#line 256 "control_parse.y"
    { (void)fputc('\n',stdout); }
    break;

  case 66:
#line 259 "control_parse.y"
    { print_exp(yyvsp[0].express); if(yyvsp[0].express) free(yyvsp[0].express); }
    break;

  case 67:
#line 260 "control_parse.y"
    { print_exp(yyvsp[0].express); if(yyvsp[0].express) free(yyvsp[0].express); }
    break;

  case 68:
#line 263 "control_parse.y"
    {if(rsformat) free(rsformat); rsformat=yyvsp[0].string;}
    break;

  case 69:
#line 264 "control_parse.y"
    {if(fsformat) free(fsformat); fsformat=yyvsp[0].string;}
    break;

  case 70:
#line 265 "control_parse.y"
    {if(gsformat) free(gsformat); gsformat=yyvsp[0].string;}
    break;

  case 71:
#line 266 "control_parse.y"
    {file_skip=yyvsp[0].value;}
    break;

  case 73:
#line 271 "control_parse.y"
    {set_array_var(yyvsp[-3].var->data,yyvsp[-1].express); if(yyvsp[-1].express) free(yyvsp[-1].express); }
    break;

  case 74:
#line 272 "control_parse.y"
    {set_array_var(yyvsp[-3].var->data,yyvsp[-1].express); if(yyvsp[-1].express) free(yyvsp[-1].express); }
    break;

  case 75:
#line 275 "control_parse.y"
    {add_restriction(yyvsp[-4].var_list);at_use=0;}
    break;

  case 76:
#line 276 "control_parse.y"
    {add_restriction(0);at_use=0;}
    break;

  case 77:
#line 279 "control_parse.y"
    {add_restriction(yyvsp[0].var_list);at_use=0;}
    break;

  case 78:
#line 280 "control_parse.y"
    {add_restriction(0);at_use=0;}
    break;

  case 79:
#line 283 "control_parse.y"
    {add_operation(&(yyvsp[0].value),INTEGER,0);}
    break;

  case 80:
#line 284 "control_parse.y"
    {add_operation(&(yyvsp[0].rvalue),REAL,0);}
    break;

  case 81:
#line 285 "control_parse.y"
    {add_operation(yyvsp[0].string,STRING,0);}
    break;

  case 82:
#line 286 "control_parse.y"
    {if(yyvsp[0].element) check_element_add_op(yyvsp[0].element);}
    break;

  case 83:
#line 287 "control_parse.y"
    {add_operation(0,0,'+');}
    break;

  case 84:
#line 288 "control_parse.y"
    {add_operation(0,0,'-');}
    break;

  case 85:
#line 289 "control_parse.y"
    {add_operation(0,0,'*');}
    break;

  case 86:
#line 290 "control_parse.y"
    {add_operation(0,0,'/');}
    break;

  case 88:
#line 292 "control_parse.y"
    {add_operation(0,0,UMINUS);}
    break;

  case 89:
#line 293 "control_parse.y"
    {add_operation(0,0,'=');}
    break;

  case 90:
#line 294 "control_parse.y"
    {add_operation(0,0,NEQSYMBOL);}
    break;

  case 91:
#line 295 "control_parse.y"
    {add_operation(0,0,LEQSYMBOL);}
    break;

  case 92:
#line 296 "control_parse.y"
    {add_operation(0,0,GEQSYMBOL);}
    break;

  case 93:
#line 297 "control_parse.y"
    {add_operation(0,0,'<');}
    break;

  case 94:
#line 298 "control_parse.y"
    {add_operation(0,0,'>');}
    break;

  case 95:
#line 299 "control_parse.y"
    {add_operation(0,0,ORSYMBOL);}
    break;

  case 96:
#line 300 "control_parse.y"
    {add_operation(0,0,ANDSYMBOL);}
    break;

  case 97:
#line 301 "control_parse.y"
    {add_operation(0,0,NOTSYMBOL);}
    break;

  case 98:
#line 304 "control_parse.y"
    { do_file_com(yyvsp[-2].string,0,0,yyvsp[0].var_list); }
    break;

  case 99:
#line 305 "control_parse.y"
    { do_file_com(yyvsp[-2].string,yyvsp[-4].format_clause,0,yyvsp[0].var_list); }
    break;

  case 100:
#line 306 "control_parse.y"
    { do_file_com(yyvsp[-2].string,0,yyvsp[-4].fformat,yyvsp[0].var_list); }
    break;

  case 101:
#line 309 "control_parse.y"
    { yyval.string=yyvsp[0].string; }
    break;

  case 102:
#line 310 "control_parse.y"
    { yyval.string=yyvsp[-1].string; shell_flag=1; }
    break;

  case 103:
#line 313 "control_parse.y"
    {yyval.format_clause=add_f_atom(0,yyvsp[0].f_atom); }
    break;

  case 104:
#line 314 "control_parse.y"
    {yyval.format_clause=add_f_atom(yyvsp[-2].format_clause,yyvsp[0].f_atom); }
    break;

  case 105:
#line 315 "control_parse.y"
    {yyval.format_clause=add_f_list(0,yyvsp[-1].format_clause,yyvsp[-3].value); }
    break;

  case 106:
#line 316 "control_parse.y"
    {yyval.format_clause=add_f_list(yyvsp[-5].format_clause,yyvsp[-1].format_clause,yyvsp[-3].value); }
    break;

  case 108:
#line 320 "control_parse.y"
    {yyval.fformat=add_fformat(yyvsp[-2].fformat,yyvsp[0].fformat); }
    break;

  case 109:
#line 321 "control_parse.y"
    {yyval.fformat=add_fformat(yyvsp[-2].fformat,yyvsp[0].fformat); }
    break;

  case 110:
#line 324 "control_parse.y"
    {yyval.fformat=create_fformat(yyvsp[0].string,2); }
    break;

  case 111:
#line 325 "control_parse.y"
    {yyval.fformat=create_fformat(yyvsp[0].string,1); }
    break;

  case 112:
#line 326 "control_parse.y"
    {yyval.fformat=create_fformat(yyvsp[0].string,4); }
    break;

  case 113:
#line 327 "control_parse.y"
    {yyval.fformat=create_fformat(&yyvsp[0].value,3); }
    break;

  case 114:
#line 330 "control_parse.y"
    {yyval.f_atom=make_f_atom(yyvsp[0].value,0);}
    break;

  case 115:
#line 331 "control_parse.y"
    {yyval.f_atom=make_f_atom(yyvsp[-1].value,1);}
    break;

  case 116:
#line 332 "control_parse.y"
    {yyval.f_atom=make_f_atom(1,1);}
    break;

  case 117:
#line 333 "control_parse.y"
    {yyval.f_atom=make_f_atom(0,1); scan_error|=FORMAT_ERR; }
    break;

  case 118:
#line 334 "control_parse.y"
    {yyval.f_atom=make_f_atom(0,1); scan_error|=FORMAT_ERR; }
    break;

  case 119:
#line 338 "control_parse.y"
    { if(LogFile) free(LogFile); LogFile=yyvsp[0].string; }
    break;

  case 120:
#line 341 "control_parse.y"
    { if(OutputFile) free(OutputFile); OutputFile=yyvsp[0].string; }
    break;

  case 121:
#line 342 "control_parse.y"
    { if(OutputLaurFile) free(OutputLaurFile); OutputLaurFile=yyvsp[0].string; }
    break;

  case 122:
#line 343 "control_parse.y"
    { if(OutputRawFile) free(OutputRawFile); OutputRawFile=yyvsp[0].string; }
    break;

  case 123:
#line 347 "control_parse.y"
    { if(ErrorDir) free(ErrorDir); ErrorDir=yyvsp[0].string;}
    break;

  case 124:
#line 350 "control_parse.y"
    { do_missing_com(yyvsp[0].express,0,0); free(yyvsp[0].express); }
    break;

  case 125:
#line 351 "control_parse.y"
    { do_missing_com(yyvsp[-1].express,yyvsp[0].var_list,0); free(yyvsp[-1].express); }
    break;

  case 126:
#line 352 "control_parse.y"
    { do_missing_com(yyvsp[-2].express,yyvsp[0].var_list,0); free(yyvsp[-2].express); }
    break;

  case 127:
#line 353 "control_parse.y"
    { do_missing_com(yyvsp[0].express,0,yyvsp[-2].string); free(yyvsp[0].express); }
    break;

  case 128:
#line 356 "control_parse.y"
    { do_ped_com(yyvsp[0].var_list); }
    break;

  case 130:
#line 360 "control_parse.y"
    { change_type(ST_TRAITLOCUS,yyvsp[0].var_list); free_vlist(yyvsp[0].var_list);}
    break;

  case 131:
#line 363 "control_parse.y"
    { change_type(ST_CONSTANT,yyvsp[0].var_list); free_vlist(yyvsp[0].var_list);}
    break;

  case 132:
#line 364 "control_parse.y"
    { change_type(ST_MULTIPLE,yyvsp[0].var_list); free_vlist(yyvsp[0].var_list);}
    break;

  case 133:
#line 365 "control_parse.y"
    { change_type(ST_RANDOM|ST_FACTOR,yyvsp[0].var_list); free_vlist(yyvsp[0].var_list);}
    break;

  case 134:
#line 366 "control_parse.y"
    { change_type(ST_FACTOR,yyvsp[0].var_list); free_vlist(yyvsp[0].var_list);}
    break;

  case 135:
#line 367 "control_parse.y"
    {change_type(ST_REALTYPE,yyvsp[0].var_list); free_vlist(yyvsp[0].var_list);}
    break;

  case 136:
#line 368 "control_parse.y"
    {change_type(ST_INTTYPE,yyvsp[0].var_list); free_vlist(yyvsp[0].var_list);}
    break;

  case 137:
#line 371 "control_parse.y"
    { yyval.value=LINK_AUTO; }
    break;

  case 138:
#line 372 "control_parse.y"
    { yyval.value=LINK_X; }
    break;

  case 139:
#line 373 "control_parse.y"
    { yyval.value=LINK_Y; }
    break;

  case 140:
#line 376 "control_parse.y"
    { do_link_com(0,yyvsp[-1].value,yyvsp[0].var_list); }
    break;

  case 141:
#line 377 "control_parse.y"
    { do_link_com(yyvsp[-2].string,yyvsp[-3].value,yyvsp[0].var_list); }
    break;

  case 142:
#line 378 "control_parse.y"
    { do_link_com(yyvsp[0].string,yyvsp[-1].value,0); }
    break;

  case 143:
#line 381 "control_parse.y"
    {
	if(Filter) {
	  print_scan_warn("Line %d: Warning - Filter defined twice\n",lineno);
	  free(Filter);
   }
	Filter=yyvsp[0].string; }
    break;

  case 144:
#line 389 "control_parse.y"
    {do_model_com(yyvsp[0].model_list,yyvsp[-2].var,0);}
    break;

  case 145:
#line 390 "control_parse.y"
    {do_model_com(yyvsp[0].model_list,yyvsp[-5].var,yyvsp[-3].express); if(yyvsp[-3].express) free(yyvsp[-3].express); }
    break;

  case 146:
#line 393 "control_parse.y"
    {set_group(yyvsp[0].element);}
    break;

  case 147:
#line 396 "control_parse.y"
    { yyval.model_list=add_to_model(yyvsp[-2].model_list,yyvsp[0].var_list); }
    break;

  case 148:
#line 397 "control_parse.y"
    { yyval.model_list=add_to_model(yyvsp[-2].model_list,yyvsp[0].var_list); }
    break;

  case 149:
#line 398 "control_parse.y"
    { yyval.model_list=add_to_model(0,yyvsp[0].var_list); }
    break;

  case 150:
#line 399 "control_parse.y"
    { yyval.model_list=add_to_model(0,yyvsp[0].var_list); }
    break;

  case 151:
#line 402 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 152:
#line 403 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 153:
#line 404 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 154:
#line 405 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 156:
#line 409 "control_parse.y"
    { yyval.var=create_var("SEX"); }
    break;

  case 157:
#line 412 "control_parse.y"
    { yyval.var_list=add_to_var_list(0,yyvsp[0].var,0); }
    break;

  case 158:
#line 413 "control_parse.y"
    { yyval.var_list=add_to_var_list(0,yyvsp[0].var,0); }
    break;

  case 159:
#line 414 "control_parse.y"
    { yyval.var_list=add_to_var_list(0,yyvsp[-3].var,yyvsp[-1].express); if(yyvsp[-1].express) free(yyvsp[-1].express); }
    break;

  case 160:
#line 417 "control_parse.y"
    { yyval.element=get_element(yyvsp[0].var,0); }
    break;

  case 161:
#line 418 "control_parse.y"
    { yyval.element=get_element(yyvsp[-3].var,yyvsp[-1].express); if(yyvsp[-1].express) free(yyvsp[-1].express); }
    break;

  case 164:
#line 423 "control_parse.y"
    { if(yyvsp[-1].element) set_locus_element(yyvsp[-1].element); }
    break;

  case 165:
#line 424 "control_parse.y"
    { if(yyvsp[0].element) set_locus_element(yyvsp[0].element); }
    break;

  case 166:
#line 425 "control_parse.y"
    { if(yyvsp[0].var) set_locus_array(yyvsp[0].var); }
    break;

  case 167:
#line 428 "control_parse.y"
    { if(yyvsp[-1].element) set_haplo_element(yyvsp[-1].element,0); }
    break;

  case 168:
#line 429 "control_parse.y"
    { if(yyvsp[-3].element) set_haplo_element(yyvsp[-3].element,yyvsp[-1].element); }
    break;

  case 171:
#line 434 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 172:
#line 437 "control_parse.y"
    { start_loopclause(); }
    break;

  case 173:
#line 440 "control_parse.y"
    { free_vlist(yyvsp[-6].var_list); begin_looping(yyvsp[-3].element,yyvsp[-1].express,0); }
    break;

  case 174:
#line 441 "control_parse.y"
    { free_vlist(yyvsp[-8].var_list); begin_looping(yyvsp[-5].element,yyvsp[-3].express,yyvsp[-1].express); }
    break;

  case 175:
#line 444 "control_parse.y"
    { yyval.var_list=yyvsp[-1].var_list; in_loopclause=0; }
    break;

  case 176:
#line 445 "control_parse.y"
    { yyval.var_list=0; in_loopclause=0; }
    break;

  case 177:
#line 448 "control_parse.y"
    { yyval.var_list=add_to_var_list(0,0,0); }
    break;

  case 178:
#line 449 "control_parse.y"
    { yyval.var_list=add_to_var_list(0,yyvsp[0].var,0); }
    break;

  case 179:
#line 450 "control_parse.y"
    { yyval.var_list=add_to_var_list(0,yyvsp[0].var,0); }
    break;

  case 180:
#line 451 "control_parse.y"
    { yyval.var_list=add_to_var_list(0,yyvsp[-3].var,yyvsp[-1].express); if(yyvsp[-1].express) free(yyvsp[-1].express); }
    break;

  case 183:
#line 456 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 184:
#line 457 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 187:
#line 462 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 188:
#line 463 "control_parse.y"
    { yyval.var_list=add_var_lists(yyvsp[-2].var_list,yyvsp[0].var_list); }
    break;

  case 189:
#line 466 "control_parse.y"
    { yyval.string = yyvsp[0].string; }
    break;

  case 190:
#line 467 "control_parse.y"
    { yyval.string = string_copy(yyvsp[-2].string,yyvsp[0].string); free(yyvsp[0].string); }
    break;

  case 191:
#line 468 "control_parse.y"
    { if(yyvsp[-2].element && (yyvsp[-2].element->type&ST_STRING)) yyval.string = string_copy(yyvsp[0].string,yyvsp[-2].element->arg.string); else yyval.string=yyvsp[0].string; }
    break;

  case 192:
#line 471 "control_parse.y"
    { yyval.string = yyvsp[0].string; }
    break;

  case 193:
#line 472 "control_parse.y"
    { if(yyvsp[0].element && (yyvsp[0].element->type&ST_STRING)) yyval.string = string_copy(0,yyvsp[0].element->arg.string); else yyval.string=0; }
    break;

  case 194:
#line 473 "control_parse.y"
    { yyval.string = string_copy(yyvsp[-2].string,yyvsp[0].string); free(yyvsp[0].string); }
    break;

  case 195:
#line 474 "control_parse.y"
    { if(yyvsp[0].element && (yyvsp[0].element->type&ST_STRING)) yyval.string = string_copy(yyvsp[-2].string,yyvsp[0].element->arg.string); else yyval.string=yyvsp[-2].string; }
    break;


    }

/* Line 1016 of /usr/local/share/bison/yacc.c.  */
#line 2385 "y.tab.c"

  yyvsp -= yylen;
  yyssp -= yylen;


#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[yytype]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*----------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action.  |
`----------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* Return failure if at end of input.  */
      if (yychar == YYEOF)
        {
	  /* Pop the error token.  */
          YYPOPSTACK;
	  /* Pop the rest of the stack.  */
	  while (yyssp > yyss)
	    {
	      YYDPRINTF ((stderr, "Error: popping "));
	      YYDSYMPRINT ((stderr,
			    yystos[*yyssp],
			    *yyvsp));
	      YYDPRINTF ((stderr, "\n"));
	      yydestruct (yystos[*yyssp], *yyvsp);
	      YYPOPSTACK;
	    }
	  YYABORT;
        }

      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yydestruct (yychar1, yylval);
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDPRINTF ((stderr, "Error: popping "));
      YYDSYMPRINT ((stderr,
		    yystos[*yyssp], *yyvsp));
      YYDPRINTF ((stderr, "\n"));

      yydestruct (yystos[yystate], *yyvsp);
      yyvsp--;
      yystate = *--yyssp;


#if YYDEBUG
      if (yydebug)
	{
	  short *yyssp1 = yyss - 1;
	  YYFPRINTF (stderr, "Error: state stack now");
	  while (yyssp1 != yyssp)
	    YYFPRINTF (stderr, " %d", *++yyssp1);
	  YYFPRINTF (stderr, "\n");
	}
#endif
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 476 "control_parse.y"


static void enter_loop(void)
{	
	if(loop_level<MAX_LOOP)	{
		loop_stat[loop_level]=loop_record;
		loop_ptr[loop_level]=loop_main_ptr;
		loop_level++;
		if(!loop_record) loop_record=1;
	} else yyerror("Too many nested loops\n");
}

static void start_loopclause(void)
{	
	if(loop_level<MAX_LOOP)	{
		loop_stat[loop_level]=loop_record;
		loop_ptr[loop_level]=loop_main_ptr;
		in_loopclause=1;
		loop_level++;
		if(!loop_record) loop_record=1;
	} else yyerror("Too many nested loops\n");
}

static void begin_looping(struct var_element *element, struct express *exp1, struct express *exp2)
{	
	int er=0,i;
	
	if(element && exp1) {
		if(exp1->type==ST_INTEGER) loop_clause_end=(int)exp1->arg.value;
		else er=1;
		if(!er && exp2) {
			if(exp2->type==ST_INTEGER) loop_clause_step=(int)exp2->arg.value;
			else er=1;
		} else loop_clause_step=1;
		if(element->type&ST_INTEGER) {
			i=(int)element->arg.value;
			if(loop_clause_step<0) {
				if(i<loop_clause_end) er= -1;
			} else if(i>loop_clause_end) er= -1;
		} else er=1;
	} else er=2;
	if(er) {
		switch(er) {
		 case 1:
			yyerror("Loop variable not integer type\n");
			break;
		 case 2:
			yyerror("Syntax error\n");
			break;
		}
		loop_record=loop_stat[--loop_level];
		if(!loop_record) loop_main_ptr=loop_level?loop_ptr[loop_level-1]:0;
		in_loopclause=0;
	} else {
		in_loopclause= -1;
		loop_record= -1;
		loop_clause_ptr=loop_main_ptr;
		loop_clause_element=element;
		loop_main_ptr=loop_ptr[loop_level-1];
	}
	if(exp1) free(exp1);
	if(exp2) free(exp2);
}

static int if_true(struct express *express)
{
	int l=0;
	
	if(express)	{
		switch(express->type) {
		 case ST_INTEGER:
			l=(express->arg.value!=0);
			break;
		 case ST_REAL:
			l=(express->arg.rvalue!=0.0);
			break;
		 case ST_STRING:
			if(express->arg.string && express->arg.string[0]) l=1;
		}
	}
	return l;
}

static void do_while_com(struct express *express)
{
	int i;
	
	if(loop_level)	{
		if(!scan_error_n && if_true(express)) {
			loop_record= -1;
			loop_main_ptr=loop_ptr[loop_level-1];
		} else {
			loop_record=loop_stat[--loop_level];
			if(!loop_record) {
				i=loop_main_ptr-1;
				loop_main_ptr=loop_level?loop_ptr[loop_level-1]:0;
				for(;i>=loop_main_ptr;i--)	{
					if(loop_stack[i].token==STRING) free(loop_stack[i].yylval.string);
				}
			}
		}
	} else yyerror("WHILE outside of do loop\n");
}

static void print_exp(struct express *express)
{
	if(!express) return;
	switch(express->type) {
	 case ST_STRING:
		(void)fputs(express->arg.string,stdout);
		free(express->arg.string);
		break;
	 case ST_INTEGER:
		(void)printf("%ld",express->arg.value);
		break;
	 case ST_REAL:
		(void)printf("%g",express->arg.rvalue);
		break;
	}
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "set_sex"
static void set_sex(struct var_element *elem,struct express *exp1,struct express *exp2)
{
	struct sex_def *se;
	
	if(!exp1 || !exp2) yyerror1("Null arguments to sex command\n");
	else {
		if(exp1->type != exp2->type) yyerror1("Arguments to sex command of different type\n");
		else if(exp1->type!=ST_INTEGER && exp1->type!=ST_STRING) yyerror1("Arguments to sex command of invalid type\n");
		else {
			if(!(se=malloc(sizeof(struct sex_def)))) ABT_FUNC(MMsg);
			se->sex_exp[0]=exp1;
			se->sex_exp[1]=exp2;
			se->sex_elem=elem;
			elem->type|=(ST_SEX|ST_FACTOR|ST_CONSTANT|ST_DATA);
			if(exp1->type==ST_INTEGER) elem->type|=ST_INTTYPE;
			se->next=sex_def;
			sex_def=se;
		}
	}
}

static void set_group(struct var_element *elem)
{
	if(group_elem) yyerror1("Error: Multiple group commands");
	else {
		group_elem=elem;
		group_elem->type|=(ST_GROUP|ST_FACTOR|ST_CONSTANT);
	}
}

static void do_ped_com(struct var_list *vlist)
{
	int i,j,n;
	struct var_list *vlist1;
	struct scan_data *sd;
	
	if(pedflag) {
		yyerror1("Error: Multiple pedigree commands");
		scan_error|=PED_ERR;
	}
	pedflag=1;
	n=0;
	while(vlist) {
		sd=vlist->var->data;
		if(sd->vtype&ST_ARRAY) {
			if(vlist->index) {
				if(n<4) pedlist[n]=sd->element+vlist->index-1;
				n++;
			} else for(i=0;i<sd->n_elements;i++)	{
				if(n<4) pedlist[n]=sd->element+i;
				n++;
			}
		} else {
			if(n<4) pedlist[n]=sd->element;
			n++;
		}
		vlist1=vlist->next;
		free(vlist);
		vlist=vlist1;
	}
	if(n!=3 && n!=4) {
		yyerror1("Error: Wrong no. variables for pedigree command (3 or 4 required)");
		scan_error|=PED_ERR;
	} else {
		for(i=1;i<n;i++) {
			for(j=0;j<i;j++) if(pedlist[i]==pedlist[j]) {
				yyerror1("Error: Repeated variables in pedigree command");
				scan_error|=PED_ERR;
			}
		}
		if(n==4) {
			pedlist[0]->type|=(ST_FAMILY|ST_FACTOR|ST_CONSTANT);
			family_id=1;
			i=1;
		} else i=0;
		pedlist[i++]->type|=(ST_ID|ST_FACTOR|ST_CONSTANT);
		pedlist[i++]->type|=(ST_SIRE|ST_FACTOR|ST_CONSTANT);
		pedlist[i++]->type|=(ST_DAM|ST_FACTOR|ST_CONSTANT);
	}
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "alloc_express"
static struct express *alloc_express(void)
{
	struct express *e;
	
	if(!(e=malloc(sizeof(struct express)))) ABT_FUNC(MMsg);
	return e;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "do_logical_op"
static struct express *do_logical_op(struct express *ex1,struct express *ex2,int op)
{
	int i=0,l=0;
	double rv1,rv2;
	char *s1,*s2;
	
	s1=s2=0;
	if(ex1->type&ST_STRING) {
		s1=ex1->arg.string;
		i++;
	}
	if(ex2 && ex2->type&ST_STRING) {
		s2=ex2->arg.string;
		i++;
	}
	if(i==2)	{
		switch(op) {
		 case '=':
			l=mystrcmp(s1,s2)?0:1;
			break;
		 case NEQSYMBOL:
			l=mystrcmp(s1,s2)?1:0;
			break;
		 case '<':
			l=mystrcmp(s1,s2)<0?1:0;
			break;
		 case '>':
			l=mystrcmp(s1,s2)>0?1:0;
			break;
		 case LEQSYMBOL:
			l=mystrcmp(s1,s2)>=0?1:0;
			break;
		 case GEQSYMBOL:
			l=mystrcmp(s1,s2)>=0?1:0;
			break;
		 case ORSYMBOL:
			l=(s1 || s2);
			break;
		 case ANDSYMBOL:
			l=(s1 && s2);
			break;
		 default:
			ABT_FUNC("Internal error - invalid string op\n");
		}
	} else if(i && !ex2)	{
		if(op!=NOTSYMBOL) ABT_FUNC("Internal error - invalid unary string op\n");
		else l=s1?0:1;
	} else if(i) {
		switch(op) {
		 case ORSYMBOL:
			if(ex1->type&ST_STRING) l=(s1 || ex2->arg.value);
			else l=(s2 || ex1->arg.value);
			break;
		 case ANDSYMBOL:
			if(ex1->type&ST_STRING) l=(s1 && ex2->arg.value);
			else l=(s2 && ex1->arg.value);
			break;
		 default:
			ABT_FUNC("Internal error - invalid string op\n");
		}
	} else {
		rv1=rv2=0.0;
		if(ex1->type&ST_INTEGER) rv1=(double)ex1->arg.value;
		else if(ex1->type&ST_REAL) rv1=ex1->arg.rvalue;
		if(ex2) {
			if(ex2->type&ST_INTEGER) rv2=(double)ex2->arg.value;
			else if(ex2->type&ST_REAL) rv2=ex2->arg.rvalue;
		}
		switch(op) {
		 case '=':
			l=(rv1==rv2);
			break;
		 case NEQSYMBOL:
			l=(rv1!=rv2);
			break;
		 case '<':
			l=(rv1<rv2);
			break;
		 case '>':
			l=(rv1>rv2);
			break;
		 case LEQSYMBOL:
			l=(rv1<=rv2);
			break;
		 case GEQSYMBOL:
			l=(rv1>=rv2);
			break;
		 case ORSYMBOL:
			l=(rv1 || rv2);
			break;
		 case ANDSYMBOL:
			l=(rv1 && rv2);
			break;
		 case NOTSYMBOL:
			l=(rv1==0.0);
			break;
		 default:
			ABT_FUNC("Internal error - invalid op\n");
		}
	}
	if(ex2) free(ex2);
	ex1->type=ST_INTEGER;
	ex1->arg.value=l;
	return ex1;
}

static struct express *do_express_op(struct express *ex1,struct express *ex2,int op)
{
	double rv1,rv2;
	int i;
	
	if(ex1->type&ST_STRING)	{
		if(ex2 && ex2->type&ST_STRING) {
			if(op!='+') yyerror("Illegal string operation\n");
			else {
				ex1->arg.string=string_copy(ex1->arg.string,ex2->arg.string);
				free(ex2->arg.string);
			}
		} else yyerror("Can't mix numeric and string expressions\n");
	} else if(ex2 && ex2->type&ST_STRING) yyerror("Can't mix numeric and string expressions\n");
	else {
		rv1=rv2=0.0;
		if(ex1->type&ST_INTEGER) rv1=(double)ex1->arg.value;
		else if(ex1->type&ST_REAL) rv1=ex1->arg.rvalue;
		if(ex2) {
			if(ex2->type&ST_INTEGER) rv2=(double)ex2->arg.value;
			else if(ex2->type&ST_REAL) rv2=ex2->arg.rvalue;
		}
		switch(op) {
		 case '+': 
			rv1+=rv2;
			break;
		 case '-':
			if(ex2) rv1-=rv2;
			else rv1= -rv1;
			break;
		 case '*':
			rv1*=rv2;
			break;
		 case '/':
			if(rv2==0.0) {
				yyerror("Divide by zero error\n");
				rv1=0.0;
			} else rv1/=rv2;
			break;
		}
		i=(int)rv1;
		if((double)i==rv1) {
			ex1->type=ST_INTEGER;
			ex1->arg.value=i;
		} else {
			ex1->type=ST_REAL;
			ex1->arg.rvalue=rv1;
		}
	}
	if(ex2) free(ex2);
	return ex1;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "check_element_add_op"
static void check_element_add_op(struct var_element *element)
{
	switch(element->type&(ST_REAL|ST_INTEGER|ST_STRING)) {
	 case ST_STRING:
		add_operation(string_copy(0,element->arg.string),STRING,0);
		break;
	 case ST_INTEGER:
		add_operation(&element->arg,INTEGER,0);
		break;
	 case ST_REAL:
		add_operation(&element->arg,REAL,0);
		break;
	 case 0:
		add_operation(element,VARIABLE,0);
		break;
	 default:
		ABT_FUNC("Internal error - illegal element type\n");
	}
}

static int check_index(struct scan_data *sd,struct express *express)
{
	int i;

	if(express->type!=ST_INTEGER) {
		if(in_loopclause<=0) yyerror("Non-integral expression for array index");
	} else if(sd->vtype&ST_ARRAY) {
		i=(int)express->arg.value;
		if(i<1 || i>sd->n_elements) {
			if(in_loopclause<=0) yyerror("Array index out of bounds");
		} else return i;
	} else yyerror("Not an array");
	return 0;
}

static struct var_element *get_element(struct bin_node *node,struct express *express)
{
	int i;
	struct scan_data *sd;
	
	sd=node->data;
	if(express) {
		if(!(i=check_index(sd,express))) return 0;
		return sd->element+i-1;
	} else {
		if(sd->vtype&ST_ARRAY)	{
			yyerror("Illegal reference to array");
			return 0;
		}
	}
	return sd->element;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "set_array_var"
static void set_array_var(struct scan_data *sd,struct express *express)
{
	int i;

	if(express->type!=ST_INTEGER) yyerror("Non-integral expression for array size");
	else if((i=(int)express->arg.value)<1) yyerror("Illegal array size");
	else if(sd->vtype) yyerror("Can't redefine variable");
	else {
		sd->vtype|=ST_ARRAY;
		sd->n_elements=i;
		free(sd->element);
		if(!(sd->element=calloc((size_t)sd->n_elements,sizeof(struct var_element)))) ABT_FUNC(MMsg);
	}
}

static int count_var_list(struct var_list *vlist)
{
	int i=0;
	struct scan_data *sd=0;
	
	while(vlist) {
		sd=vlist->var?vlist->var->data:0;
		if(sd && (sd->vtype&ST_ARRAY) && !vlist->index) i+=sd->n_elements;
		else i++;
		vlist=vlist->next;
	}
	return i;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "assign_var"
static struct var_element *assign_var(struct bin_node *node,struct express *ix,struct express *express)
{
	struct var_element *element;
	struct scan_data *sd;
	
	if(!express) return 0;
	if(!(element=get_element(node,ix))) return 0;
	switch(express->type) {
	 case ST_STRING:
	     element->arg.string=express->arg.string;
		RemBlock=AddRemem(element->arg.string,RemBlock);
		break;
	 case ST_REAL:
	 case ST_INTEGER:
	     element->arg=express->arg;
		break;
	 case 0:
	     yyerror1("Undefined assignment\n");
		element->type=0;
		element->arg.string=0;
		break;
	 default:
		ABT_FUNC(IntErr);
	}
	if(!ix) {
		sd=node->data;
		sd->vtype|=ST_SCALAR;
	}
	element->type=express->type;
	return element;
}

void check_vars(struct bin_node *node,int *i,void check_func(struct bin_node *,int *))
{
	if(node->left) {
		check_vars(node->left,i,check_func);
	}
	check_func(node,i);
	if(node->right) {
		check_vars(node->right,i,check_func);
	}
}

static void check_vars_1(struct bin_node *node,void check_func(struct bin_node *))
{
	if(node->left) {
		check_vars_1(node->left,check_func);
	}
	check_func(node);
	if(node->right) {
		check_vars_1(node->right,check_func);
	}
}

void print_scan_err(char *fmt, ...)
{
	va_list args;
	
	va_start(args,fmt);
	(void)vfprintf(stderr,fmt,args);
	va_end(args);
	if((++scan_error_n)>=max_scan_errors) abt(__FILE__,__LINE__,"Too many errors - aborting\n");
}

void print_scan_warn(char *fmt, ...)
{
	va_list args;
	
	if(scan_warn_n<max_scan_warnings) {
		va_start(args,fmt);
		(void)vfprintf(stderr,fmt,args);
		va_end(args);
	}
	scan_warn_n++;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_operation"
static void add_operation(void *arg,int type, int op)
{
	struct operation *o;
	
	if(!arg && type) ABT_FUNC(IntErr);
	if(!(o=malloc(sizeof(struct operation)))) ABT_FUNC(MMsg);
	o->next=Op_List;
	Op_List=o;
	o->type=type;
	o->op=op;
	switch(type) {
	 case VARIABLE:
		o->arg.element= (struct var_element *)arg;
		break;
	 case INTEGER:
		o->arg.value= *(int *)arg;
		break;
	 case REAL:
		o->arg.rvalue= *(double *)arg;
		break;
	 case STRING:
		o->arg.string= (char *)arg;
		break;
	}
}

static void new_command(void)
{
	shell_flag=in_loopclause=0;
	Op_List=0;
	iflag=0;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_to_model"
static struct model_list *add_to_model(struct model_list *model,struct var_list *vlist)
{
	struct var_list *vlist1;
	struct scan_data *sd;
	struct model_list *m1;
	int i;
	
	if(!(m1=malloc(sizeof(struct model_list)))) ABT_FUNC(MMsg);
	if(vlist) {
		i=count_var_list(vlist);
		if(!(m1->element=malloc(sizeof(void *)*i))) ABT_FUNC(MMsg);
		i=0;
		while(vlist) {
			sd=vlist->var->data;
			if(sd->vtype&ST_ARRAY)	{
				if(vlist->index) {
					sd->element[vlist->index-1].type|=ST_MODEL;
					sd->element[vlist->index-1].index=vlist->index;
					sd->element[vlist->index-1].oindex=vlist->index;
					m1->element[i++]=sd->element+vlist->index-1;
				} else yyerror("Error - Can't use whole arrays as model parameters");
			} else {
				sd->element[0].type|=ST_MODEL;
				sd->element[0].index=0;
				m1->element[i++]=sd->element;
			}
			vlist1=vlist->next;
			free(vlist);
			vlist=vlist1;
		}
		m1->nvar=i;
	} else ABT_FUNC("Nothing to add...\n");
	m1->next=model;
	return m1;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "string_copy"
static char *string_copy(char *s1,char *s2)
{
	if(s1) {  
		if(!(s1=realloc(s1,strlen(s1)+strlen(s2)+1))) ABT_FUNC(MMsg);
		(void)strcat(s1,s2);
	} else {
		if(!(s1=malloc(strlen(s2)+1))) ABT_FUNC(MMsg);
		(void)strcpy(s1,s2);
	}
	return s1;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "setup_format"
static struct format *setup_format(struct format_clause *fc)
{
	int i,n=0,pp=0;
	struct format_atom **fa;
	struct format *format;
	
	fa=fc->f_atoms;
	for(i=0;i<fc->n_atoms;i++) if(!fa[i]->pos) n++;
	if(!n) {
		if(!(scan_error&FORMAT_ERR)) yyerror("Error - Empty format clause");
		free(fa);
		free(fc);
		scan_error|=FORMAT_ERR;
		scan_error_n++;
		return 0;
	}
	if(!(format=malloc(sizeof(struct format)))) ABT_FUNC(MMsg);
	format->line=lineno;
	if(!(format->f_atoms=malloc(sizeof(struct format_atom)*n))) ABT_FUNC(MMsg);
	for(i=n=0;i<fc->n_atoms;i++) {
		if(!fa[i]->pos) {
			format->f_atoms[n].size=fa[i]->size;
			format->f_atoms[n++].pos=pp;
		}
		pp+=fa[i]->size;
	}
	free(fa);
	format->n_atoms=n;
	f_atom_n=0;
	free(fc);
	return format;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "make_f_atom"
static struct format_atom *make_f_atom(int n,int flag)
{
	if(f_atom_n>=f_atom_size) {
		f_atom_size*=2;
		if(!(f_atom_list=realloc(f_atom_list,sizeof(struct format_atom)*f_atom_size))) ABT_FUNC(MMsg);
	}
	f_atom_list[f_atom_n].size=n;
	f_atom_list[f_atom_n].pos=flag;
	return &f_atom_list[f_atom_n++];
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_fformat"
static struct fformat *add_fformat(struct fformat *f1,struct fformat *f2)
{
	if(f2->rs) {
		if(f1->rs) free(f1->rs);
		f1->rs=f2->rs;
	}
	if(f2->fs) {
		if(f1->fs) free(f1->fs);
		f1->fs=f2->fs;
	}
	if(f2->gs) {
		if(f1->gs) free(f1->gs);
		f1->gs=f2->gs;
	}	
	if(f2->skip) f1->skip=f2->skip;
	free(f2);
	return f1;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "create_fformat"
static struct fformat *create_fformat(void *p,int fg)
{
	struct fformat *ff;
	int *i;
	
	if(!(ff=malloc(sizeof(struct fformat)))) ABT_FUNC(MMsg);
	ff->rs=ff->fs=ff->gs=0;
	ff->skip=0;
	switch(fg) {
	 case 1:
		ff->rs=p;
		break;
	 case 2:
		ff->fs=p;
		break;
	 case 3:
		i=p;
		ff->skip=*i;
		break;
	 case 4:
		ff->gs=p;
		break;
	 default:
		ABT_FUNC("Internal error - incorrect flag\n");
	}
	return ff;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_f_atom"
static struct format_clause *add_f_atom(struct format_clause *fc,struct format_atom *fa)
{
	if(!fc) {
		if(!(fc=malloc(sizeof(struct format_clause)))) ABT_FUNC(MMsg);
		fc->fc_size=16;
		fc->n_atoms=0;
		if(!(fc->f_atoms=malloc(sizeof(struct format_atom *)*fc->fc_size))) ABT_FUNC(MMsg);
	}
	if(fc->n_atoms>=fc->fc_size) {
		fc->fc_size*=2;
		if(!(fc->f_atoms=realloc(fc->f_atoms,sizeof(struct format_atom *)*fc->fc_size))) ABT_FUNC(MMsg);
	}
	fc->f_atoms[fc->n_atoms++]=fa;
	return fc;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_f_list"
static struct format_clause *add_f_list(struct format_clause *fc,struct format_clause *fc1,int n)
{
	int sz,i,j;
	
	sz=fc1->n_atoms*n;
	if(!fc) {
		if(!(fc=malloc(sizeof(struct format_clause)))) ABT_FUNC(MMsg);
		fc->fc_size=16;
		if(sz>16) fc->fc_size=sz;
		fc->n_atoms=0;
		if(!(fc->f_atoms=malloc(sizeof(struct format_atom *)*fc->fc_size))) ABT_FUNC(MMsg);
	} else {
		if(sz>(fc->fc_size-fc->n_atoms)) {
			fc->fc_size=sz+fc->n_atoms;
			if(!(fc->f_atoms=realloc(fc->f_atoms,sizeof(struct format_atom *)*fc->fc_size))) ABT_FUNC(MMsg);
		}
	}
	for(i=0;i<n;i++) for(j=0;j<fc1->n_atoms;j++)
	  fc->f_atoms[fc->n_atoms++]=fc1->f_atoms[j];
	free(fc1->f_atoms);
	free(fc1);
	return fc;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "alloc_var"
static struct bin_node *alloc_var(char *p)
{
	struct bin_node *node;
	struct scan_data *sd;
	int i;
	
	if(!(node=malloc(sizeof(struct bin_node)))) ABT_FUNC(MMsg);
	node->left=node->right=0;
	node->balance=0;
	if(!(sd=malloc(sizeof(struct scan_data)))) ABT_FUNC(MMsg);
	node->data=sd;
	sd->vtype=0;
	i=(int)strlen(p);
	if(!(sd->name=malloc((size_t)i+1))) ABT_FUNC(MMsg);
	sd->name[i--]=0;
	for(;i>=0;i--) sd->name[i]=toupper((int)p[i]);
	sd->n_elements=1;
	if(!(sd->element=calloc(1,sizeof(struct var_element)))) ABT_FUNC(MMsg);
	sd->element->arg.element=0;
	return node;
}

static struct bin_node *find_var(char *p,struct bin_node *node,struct bin_node **node1,int *balanced)
{
	int i;
	struct scan_data *sd;
	
	sd=node->data;
	if((i=strcasecmp(p,sd->name))) {
		if(i<0) {
			if(node->left) {
				node->left=find_var(p,node->left,node1,balanced);
			} else {
				*node1=node->left=alloc_var(p);
				*balanced=0;
			}
			if(!(*balanced)) {
				switch(node->balance) {
				 case -1:
					node=rotate_left(node);
					*balanced=1;
					break;
				 case 0:
					node->balance=-1;
					break;
				 case 1:
					node->balance=0;
					*balanced=1;
				}
			}
		} else {
			if(node->right) {
				node->right=find_var(p,node->right,node1,balanced);
			} else {
				*node1=node->right=alloc_var(p);
				*balanced=0;
			}
			if(!(*balanced)) {
				switch(node->balance) {
				 case -1:
					node->balance=0;
					*balanced=1;
					break;
				 case 0:
					node->balance=1;
					break;
				 case 1:
					node=rotate_right(node);
					*balanced=1;
				}
			}
		}
	} else {
		*node1=node;
		*balanced=1;
	}
	return node;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "Check_var"
static void Check_var(struct bin_node *node)
{
	int i;
	struct var_element *element;
	struct scan_data *sd;
	char *nbuf;
	
	if(node->left) Check_var(node->left);
	sd=node->data;
	i=strlen(sd->name)+4+log((double)(sd->n_elements+1))/log(10.0);
	if(!(nbuf=malloc((size_t)i))) ABT_FUNC(MMsg);
	for(i=0;i<sd->n_elements;i++) {
		if(sd->vtype&ST_ARRAY) (void)sprintf(nbuf,"%s(%d)",sd->name,i+1);
		else (void)strcpy(nbuf,sd->name);
		element=sd->element+i;
		if(!(element->type&(ST_DATA|ST_TRAITLOCUS|ST_LINKED))) {
			if(element->type&(ST_MODEL|ST_SEX|ST_ID|ST_FAMILY|ST_SIRE|ST_DAM|ST_TRAIT|ST_GROUP))
			  print_scan_err("Error: No data for variable %s\n",nbuf);
		}
		if((element->type&ST_DATA) && (element->type&ST_TRAITLOCUS))
		  print_scan_err("Error: Variable %s can not have data\n",nbuf);
		else if((element->type&ST_LINKED) && !(element->type&(ST_TRAITLOCUS|ST_MARKER)))
		  print_scan_err("Error: Variable %s is not a locus and so can not be linked\n",nbuf);
		else if((element->type&ST_TRAIT) && (element->type&(ST_GROUP|ST_MARKER|ST_TRAITLOCUS|ST_ID|ST_FAMILY|ST_SIRE|ST_DAM|ST_HAPLO|ST_LINKED|ST_STRING|ST_REAL|ST_INTEGER)))
		  print_scan_err("Error: Variable %s inappropriate type for trait\n",nbuf);
		else if((element->type&ST_TRAITLOCUS) && (element->type&(ST_SEX|ST_GROUP|ST_CENSORED|ST_RANDOM|ST_MARKER|ST_ID|ST_FAMILY|ST_SIRE|ST_DAM|ST_HAPLO|ST_STRING|ST_REAL|ST_INTEGER|ST_REALTYPE|ST_INTTYPE)))
		  print_scan_err("Error: Variable %s inappropriate type for trait locus\n",nbuf);
		else if((element->type&ST_MARKER) && (element->type&(ST_SEX|ST_GROUP|ST_CENSORED|ST_REAL|ST_INTEGER|ST_RANDOM|ST_ID|ST_FAMILY|ST_SIRE|ST_DAM|ST_HAPLO|ST_STRING|ST_INTEGER)))
		  print_scan_err("Error: Variable %s inappropriate type for marker\n",nbuf);
		else if((element->type&ST_HAPLO) && (element->type&(ST_SEX|ST_GROUP|ST_CENSORED|ST_REAL|ST_RANDOM|ST_ID|ST_FAMILY|ST_SIRE|ST_DAM|ST_STRING|ST_REAL|ST_INTEGER)))
		  print_scan_err("Error: Variable %s inappropriate type for haplotype\n",nbuf);
		else if((element->type&ST_RANDOM) && (element->type&(ST_SEX|ST_GROUP|ST_STRING|ST_REAL|ST_INTEGER|ST_REAL)))
		  print_scan_err("Error: Variable %s inappropriate type to be random\n",nbuf);
		else if((element->type&(ST_INTTYPE|ST_REALTYPE)) && (element->type&(ST_STRING|ST_REAL|ST_INTEGER)))
		  print_scan_err("Error: Type collision for variable %s\n",nbuf);
		else if((element->type&ST_INTTYPE) && (element->type&ST_REALTYPE))
		  print_scan_err("Error: Real variable %s can not also be integer type\n",nbuf);
		else if((element->type&(ST_STRING|ST_REAL)) && (element->type&(ST_SEX|ST_ID|ST_FAMILY|ST_SIRE|ST_DAM)))
		  print_scan_err("Error: Variable %s can not be a pedigree or sex variable\n",nbuf);
		else if((element->type&ST_REAL) && (element->type&(ST_ID|ST_FAMILY|ST_SIRE|ST_DAM|ST_SEX)))
		  print_scan_err("Error: Real variable %s can not be a pedigree or sex variable\n",nbuf);
		else if((element->type&ST_FACTOR) && (element->type&ST_REAL))
		  print_scan_err("Error: Real variable %s can not be a factor\n",nbuf);
		else if((element->type&ST_CONSTANT)&&(element->type&ST_MULTIPLE))
		  print_scan_err("Error: Variable %s can not be in multiple records and be constant\n",nbuf);
		else if(element->type&(ST_SEX|ST_ID|ST_FAMILY|ST_SIRE|ST_DAM|ST_TRAITLOCUS|ST_GROUP|ST_LINKED|ST_MODEL|ST_TRAIT))
		  element->type|=ST_REQUIRED;
		else if(element->type&ST_HAPLO) {
			if(element->arg.element && element->arg.element->type&ST_LINKED) {
				element->type|=ST_REQUIRED;
				if(!(element->type&ST_DATA)) print_scan_err("Error: No data for variable %s\n",nbuf);
			}
		}
		if(element->type&ST_MARKER) n_markers++;
		if(!(element->type&(ST_CONSTANT|ST_MULTIPLE))) 
		  element->type|=syst_var[MULTIPLE_RECORDS]?ST_MULTIPLE:ST_CONSTANT;
		if(element->type&(ST_MARKER|ST_REQUIRED|ST_RESTRICT))	{
			if(!(element->type&ST_HAPLO)) element->arg.var=node;
		} else element->type=0;
	}
	free(nbuf);
	if(node->right) Check_var(node->right);
}

static struct bin_node *create_var(char *p)
{
	int k;
	struct bin_node *node;
	
	if(!root_var) node=root_var=alloc_var(p);
	else {
		root_var=find_var(p,root_var,&node,&k);
	}
	return node;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "symbol_lookup"
int symbol_lookup(char *p,int fg)
{
	static char *Coms[] = {"FILE","LOCUS","LOCI","MARKER","DISCRETE","MODEL","PEDIGREE","LOG",
		"FILTER","MISSING","MODEL","LINK","RANDOM","TRAIT","WHERE","USE",
		"REAL","INTEGER","SHELL","ARRAY","PRINT","DO","WHILE","CONSTANT",
		"MULTIPLE","CENSORED","GROUP","SET","SEX","AFFECTED","UNAFFECTED","OUTPUT","INCLUDE","ERRORDIR",
		"LAUROUTPUT","RAWOUTPUT","POSITION","FREQUENCY",(char *)0};
	static int Com_token[] = {FILEC,LOCUS,LOCUS,MARKER,FACTOR,MODEL,PEDIGREE,LOG,
		FILTER,MISSING,MODEL,LINK,RANDOM,TRAIT,WHERE,USE,
		REAL,INTEGER,SHELL,ARRAY,PRINTEXP,DOLOOP,WHILE,CONSTANT,
		MULTIPLE,CENSORED,GROUP,SET,GENDER,AFFECTED,UNAFFECTED,OUTPUT,INCLUDE,ERRORDIR,
		LAUROUTPUT,RAWOUTPUT,POSITION,FREQUENCY,SYSTEM_VAR,VARIABLE,ARRAY_VAR};
	static char *Syst[] = {"PRUNE_OPTION","RECODE_OPTION","NO_EXTRA_ALLELE",
		"PEEL_OPTION","TRACE_RESTRICT","TRACE_CENSORED","TRACE_AFFECTED",
		"CORRECT_ERRORS","TRACE_PEEL","MULTIPLE_RECORDS","MULTIVARIATE_TEST",
		"ERROR_CHECK","NO_DEFAULT_MISSING","SKIP_BAD_REALS","SKIP_BAD_INTS","IGNORE_CASE",(char *)0};
	int i=0,j=0;
	static struct scan_data *sd;
	
	while(Coms[i])	{
		if(!strcasecmp(Coms[i],p)) break;
		i++;
	}
	at_file=0;
	if(Com_token[i]==FILEC || Com_token[i]==LINK) at_file=1;
	if(Com_token[i]==SYSTEM_VAR) {
		i++;
		while(Syst[j])	{
			if(!strcasecmp(Syst[j],p))	{
				yylval.value=j;
				i--;
				break;
			}
			j++;
		}
	}
	if(Com_token[i]==VARIABLE) {
		if(fg==1 && begin_comm) {
			begin_comm=0;
			return BREAK;
		}
		yylval.var=create_var(p);
		sd=yylval.var->data;
		if(sd->vtype&ST_ARRAY) i++;
		if(fg==1) {
			begin_comm=1;
			(void)strcpy(linebuf1,linebuf);
			lineno1=lineno;
		}
	} else if(begin_comm && Com_token[i]!=SYSTEM_VAR && Com_token[i]!=LOCUS && Com_token[i]!=SHELL
				 && !(at_use==1 && Com_token[i]==WHERE) && !(at_use==2 && Com_token[i]==USE))	{
		begin_comm=0;
		at_use=0;
		return BREAK;
	} else {
		begin_comm=1;
		(void)strcpy(linebuf1,linebuf);
		lineno1=lineno;
		if(Com_token[i]==MODEL) at_model=1;
		else at_model=0;
		if(Com_token[i]==USE || Com_token[i]==CENSORED || Com_token[i]==AFFECTED || Com_token[i]==UNAFFECTED) at_use|=1;
		else if(Com_token[i]==WHERE) at_use|=2;
		else at_use=0;
	}
	return Com_token[i];
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "symbol_lookup"
static struct var_list *add_to_var_list(struct var_list *vlist,struct bin_node *node,struct express *express)
{
	struct var_list *vlist1,*vlist2;
	struct scan_data *sd=0;
	int i;

	if(node)	sd=node->data;
	if(express) i=check_index(sd,express);
	else {
		i=0;
		if(sd && !(sd->vtype&ST_ARRAY)) sd->vtype|=ST_SCALAR;
	}
	if(!(vlist1=malloc(sizeof(struct var_list)))) ABT_FUNC(MMsg);
	vlist1->next=0;
	vlist1->var=node;
	vlist1->index=i;
	vlist2=vlist;
	if(vlist2) {
		while(vlist2->next) vlist2=vlist2->next;
		vlist2->next=vlist1;
	} else vlist=vlist1;
	return vlist;
}

struct var_list *add_var_lists(struct var_list *vlist,struct var_list *vlist1)
{
	struct var_list *vlist2;
	
	vlist2=vlist;
	if(vlist2) {
		while(vlist2->next) vlist2=vlist2->next;
		vlist2->next=vlist1;
	} else vlist=vlist1;
	return vlist;
}

static void set_locus_array(struct bin_node *node)
{
	struct scan_data *sd;
	int i;
	
	sd=node->data;
	if(sd->vtype&ST_ARRAY) {
		for(i=0;i<sd->n_elements;i++) {
			set_locus_element(sd->element+i);
		}
	} else yyerror("Not an array");
}

static void set_locus_element(struct var_element *element)
{
	element->type|=(ST_MARKER|ST_FACTOR|ST_CONSTANT);
	if(hap_list[0]) {
		if(hap_list[0]->arg.element && hap_list[0]->arg.element!=element)	{
			yyerror1("Haplotype vector (left) used twice");
			hap_list[0]->arg.element=0;
		} else hap_list[0]->arg.element=element;
	}
	if(hap_list[1]) {
		if(hap_list[1]->arg.element && hap_list[1]->arg.element!=element)	{
			yyerror1("Haplotype vector (right) used twice");
			hap_list[1]->arg.element=0;
		} else hap_list[1]->arg.element=element;
	}
	hap_list[0]=hap_list[1]=0;
}

static void set_haplo_element(struct var_element *element,struct var_element *element1)
{
	element->type|=(ST_HAPLO|ST_FACTOR|ST_CONSTANT);
	if(element1) element1->type|=(ST_HAPLO|ST_FACTOR|ST_CONSTANT);
	hap_list[0]=element;
	hap_list[1]=element1;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "do_file_com"
static void do_file_com(char *fname,struct format_clause *fc,struct fformat *ff,struct var_list *vlist)
{
	int i,j;
	struct InFile *file;
	struct format *format;
	struct var_list *vlist1;
	struct var_element *element;
	struct scan_data *sd;
	
	if(!vlist) {
		yyerror1("No variables listed for FILE command\n");
		return;
	} else if(!fname) {
		free_vlist(vlist);
		return;
	} else if(!fname[0]) {
		yyerror1("Zero length filename for FILE command\n");
		free_vlist(vlist);
		return;
	}
	file=Infiles;
	if(!(Infiles=calloc(1,sizeof(struct InFile)))) ABT_FUNC(MMsg);
	Infiles->next=file;
	Infiles->nvar=count_var_list(vlist);
	if(!(Infiles->element=malloc(sizeof(void *)*Infiles->nvar))) ABT_FUNC(MMsg);
	i=0;
	while(vlist) {
		if(vlist->var) {
			sd=vlist->var->data;
			if(sd->vtype&ST_ARRAY) {
				if(vlist->index) {
					element=sd->element+vlist->index-1;
					element->type|=ST_DATA;
					Infiles->element[i++]=element;
				} else {
					for(j=0;j<sd->n_elements;j++) {
						element=sd->element+j;
						element->type|=ST_DATA;
						Infiles->element[i++]=element;
					}
				}
			} else {
				element=sd->element;
				element->type|=ST_DATA;
				Infiles->element[i++]=element;
			}
		} else Infiles->element[i++]=0;
		vlist1=vlist->next;
		free(vlist);
		vlist=vlist1;
	}
	if(fc) {
		format=setup_format(fc);
		Infiles->format=format;
		if(!(scan_error&FORMAT_ERR)) {
			if(format->n_atoms<i) {
				(void)printf("format->n_atoms = %d\n",format->n_atoms);
				(void)printf("i = %d\n",i);
				print_scan_err("Line %d: Error - Too many variables for format clause\n",format->line);
				scan_error|=FORMAT_ERR;
			} else if(format->n_atoms>i)
				  print_scan_warn("Line %d: Warning - Too few variables for format clause\n",format->line);
		}
	} else if(ff) Infiles->fformat=ff;
	Infiles->name=fname;
	Infiles->shell_flag=shell_flag;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "change_type"
static void change_type(int type,struct var_list *vlist)
{
	int j;
	struct scan_data *sd;
	
	
	while(vlist) {
		sd=vlist->var->data;
		if(sd->vtype&ST_ARRAY) {
			if(vlist->index) sd->element[vlist->index-1].type|=type;
			else for(j=0;j<sd->n_elements;j++)
			  sd->element[j].type|=type;
		} else sd->element[0].type|=type;
		vlist=vlist->next;
	}
}

static void free_vlist(struct var_list *vlist)
{
	struct var_list *vlist1;
	
	while(vlist) {
		vlist1=vlist->next;
		free(vlist);
		vlist=vlist1;
	}
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "do_link_com"
static void do_link_com(char *s,int type,struct var_list *vlist)
{
	struct Link *l,*l1,**ll;
	struct var_list *vlist1;
	struct var_element *element;
	struct scan_data *sd=0;
	int i,j,k;
	
	if(vlist) sd=vlist->var->data;
	if(!s && sd) {
		if(sd->vtype&ST_ARRAY && vlist->index) {
			element=sd->element+vlist->index-1;
			if(element->type&ST_STRING) {
				s=element->arg.string;
				vlist1=vlist->next;
				free(vlist);
				vlist=vlist1;
			}
		} else {
			element=sd->element;
			if(element->type&ST_STRING) {
				s=element->arg.string;
				vlist1=vlist->next;
				free(vlist);
				vlist=vlist1;
			}
		}
	}
	ll=&links;
	while(*ll) {
		l=*ll;
		if(s) {
			if(l->name) {
				if(!strcasecmp(s,l->name)) break;
			}
		} else if(!l->name) break;
		ll=&l->next;
	}
	if(*ll) l1=*ll;
	else {
		if(!(l1=malloc(sizeof(struct Link)))) ABT_FUNC(MMsg);
		l1->next=0;
		l1->name=s;
		l1->n_loci=0;
		l1->element=0;
		l1->type=-1;
		*ll=l1;
	}
	i=count_var_list(vlist);
	if(l1->type>=0 && l1->type!=type) print_scan_err("Error: Linkage group has inconsistent linkage type\n");
	l1->type=type;
	if(i) {
		k=i+l1->n_loci;
		if(l1->element) {
			if(!(l1->element=realloc(l1->element,sizeof(void *)*k))) ABT_FUNC(MMsg);
		} else if(!(l1->element=malloc(sizeof(void *)*k))) ABT_FUNC(MMsg);
		while(vlist) {
			sd=vlist->var->data;
			if(sd->vtype&ST_ARRAY) {
				if(vlist->index) {
					element=sd->element+vlist->index-1;
					for(i=0;i<l1->n_loci;i++) {
						if(l1->element[i]==element) break;
					}
					if(i==l1->n_loci) {
						if(element->type&ST_LINKED) {
							print_scan_err("Error: %s(%d) appears in multiple linkage groups\n",sd->name,vlist->index);
							scan_error|=LINK_ERR;
						} else {
							element->type|=ST_LINKED;
							l1->element[l1->n_loci++]=element;
						}
					}
				} else {
					for(j=0;j<sd->n_elements;j++) {
						element=sd->element+j;
						for(i=0;i<l1->n_loci;i++) {
							if(l1->element[i]==element) break;
						}
						if(i==l1->n_loci) {
							if(element->type&ST_LINKED) {
								print_scan_err("Error: %s(%d) appears in multiple linkage groups\n",sd->name,vlist->index);
								scan_error|=LINK_ERR;
							} else {
								element->type|=ST_LINKED;
								l1->element[l1->n_loci++]=element;
							}
						}
						sd->vtype|=ST_LINKED;
					}
				}
			} else {
				element=sd->element;
				for(i=0;i<l1->n_loci;i++) {
					if(l1->element[i]==element) break;
				}
				if(i==l1->n_loci) {
					if(element->type&ST_LINKED) {
						print_scan_err("Error: %s appears in multiple linkage groups\n",sd->name);
						scan_error|=LINK_ERR;
					} else {
						element->type|=ST_LINKED;
						l1->element[l1->n_loci++]=element;
					}
				}
			}
			vlist1=vlist->next;
			free(vlist);
			vlist=vlist1;
		}
		if(l1->n_loci<k) if(!(l1->element=realloc(l1->element,sizeof(void *)*l1->n_loci))) ABT_FUNC(MMsg);
	}
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "do_missing_com"
static void do_missing_com(struct express *expr,struct var_list *vlist,char *s1)
{
	struct var_list *vlist1;
	struct scan_data *sd;
	struct var_element **elem;
	struct Miss *m;
	int i,j;
	char *p;
	
	if(s1) {
		if(vlist) ABT_FUNC("Can't have both explicit and implicit scope\n");
		if(s1[0]==0) {
			print_scan_err("Empty scope - MISSING directive ignored\n");
			if(expr->type==ST_STRING) free(expr->arg.string);
			free(s1);
			return;
		}
		qstrip(s1);
		p=s1;
		i=j=0;
		while(*p) {
			switch(toupper((int)*p)) {
			 case '!':
			 case 'F':
			 case 'G':
			 case 'P':
			 case 'C':
			 case 'R':
			 case 'I':
				break;
			 default: i=1;
			}
			if(i) break;
			p++;
		}
		if(*p) {
			j=1;
			print_scan_err("Illegal character '%c' in MISSING scope\n",*p);
		} else if(*(--p)=='!') {
			j=1;
			print_scan_err("MISSING scope can not end with a '!'\n",*p);
		}
		if(j) {
			free(s1);
			if(expr->type==ST_STRING) free(expr->arg.string);
			return;
		}
	}
	m=Miss;
	if(!(Miss=malloc(sizeof(struct Miss)))) ABT_FUNC(MMsg);
	Miss->Missing.arg=expr->arg;
	Miss->Missing.type=expr->type;
	Miss->next=m;
	Miss->element=0;
	Miss->scope=0;
	if((i=count_var_list(vlist))) {
		if(!(elem=malloc(sizeof(void *)*i))) ABT_FUNC(MMsg);
		i=0;
		while(vlist) {
			sd=vlist->var->data;
			if(sd->vtype&ST_ARRAY) {
				if(vlist->index) elem[i++]=sd->element+vlist->index-1;
				else for(j=0;j<sd->n_elements;j++) elem[i++]=sd->element+j;
			} else elem[i++]=sd->element;
			Miss->element=elem;
			vlist1=vlist->next;
			free(vlist);
			vlist=vlist1;
		}
	} else if(s1) Miss->scope=s1;
	Miss->nvar=i;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "do_model_com"
static void do_model_com(struct model_list *mlist,struct bin_node *node,struct express *express)
{
	struct model *model,*model1;
	struct var_element *element;
	struct scan_data *sd;
	
	sd=node->data;
	if(!(model=malloc(sizeof(struct model)))) ABT_FUNC(MMsg);
	model->next=0;
   if(Models) {
		model1=Models;
		while(model1->next) model1=model1->next;
		model1->next=model;
	} else Models=model;
	model->trait=sd;
	if(!express) {
		model->index=0;
		sd->element[0].type|=ST_TRAIT;
	} else {
		element=get_element(node,express);
		if(element) {
			model->index=(int)express->arg.value;
			element->index=element->oindex=model->index;
			element->type|=ST_TRAIT;
		} else model->trait=0;
	}
	model->model_list=mlist;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_censored"
static void add_censored(struct var_element *element,const int fg)
{
	struct operation *ops,*ops1,*ops2;
	struct Censor *cen;
	
	if(fg==1 && !element) {
		print_scan_err("Error: Nothing to censor!\n");
		return;
	}
	ops=Op_List;
	/* Reverse list order (really return list to original order! */
	if(ops) {
		ops1=ops->next;
		while(ops1) {
			ops2=ops1->next;
			ops1->next=ops;
			ops=ops1;
			ops1=ops2;
		}
		Op_List->next=0;
		Op_List=ops;
	}
	switch(fg) {
	 case 1:
		if(!(cen=malloc(sizeof(struct Censor)))) ABT_FUNC(MMsg);
		cen->next=Censored;
		Censored=cen;
		cen->Op_List=ops;
		cen->element=element;
		element->type|=ST_CENSORED;
		break;
	 case 0:
		if(Affected) print_scan_warn("Warning - new affected statement overrules previous statement\n");
		Affected=ops;
		break;
	 case 2:
		if(Unaffected) print_scan_warn("Warning - new unaffected statement overrules previous statement\n");
		Unaffected=ops;
		break;
	}
	ops=Op_List;
	while(ops) {
		if(ops->type==VARIABLE) ops->arg.element->type|=ST_RESTRICT;
		ops=ops->next;
	}
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_restriction"
static void add_restriction(struct var_list *vlist)
{
	struct operation *ops,*ops1,*ops2;
	struct Restrict *res;
	struct var_list *vlist1;
	struct scan_data *sd;
	int i,j;
	
	ops=Op_List;
	/* Reverse list order (really return list to original order! */
	if(ops) {
		ops1=ops->next;
		while(ops1) {
			ops2=ops1->next;
			ops1->next=ops;
			ops=ops1;
			ops1=ops2;
		}
		Op_List->next=0;
		Op_List=ops;
	}
	if(!(res=malloc(sizeof(struct Restrict)))) ABT_FUNC(MMsg);
	res->next=Restrictions;
	Restrictions=res;
	res->Op_List=ops;
	if((res->nvar=count_var_list(vlist))) {
		if(!(res->element=malloc(sizeof(void *)*res->nvar))) ABT_FUNC(MMsg);
		i=0;
		while(vlist) {
			sd=vlist->var->data;
			if(sd->vtype&ST_ARRAY) {
				if(vlist->index) res->element[i++]=sd->element+vlist->index-1;
				else for(j=0;j<sd->n_elements;j++) res->element[i++]=sd->element+j;
			} else res->element[i++]=sd->element;
			vlist1=vlist->next;
			free(vlist);
			vlist=vlist1;
		}	
	} else res->element=0;
	while(ops) {
		if(ops->type==VARIABLE) ops->arg.element->type|=ST_RESTRICT;
		ops=ops->next;
	}
}

static void find_markers(struct bin_node *node,int *i)
{
	int j;
	struct scan_data *sd;
	
	sd=node->data;
	for(j=0;j<sd->n_elements;j++) if(sd->element[j].type&ST_MARKER) {
		if(sd->element[j].type&ST_REQUIRED) {
			markers[*i].element=sd->element+j;
			markers[*i].var=sd;
			markers[(*i)++].index=j+1;
		}
	}
}

static void find_trait_loci(struct bin_node *node,int *i)
{
	int j;
	struct scan_data *sd;
	
	sd=node->data;
	for(j=0;j<sd->n_elements;j++) if(sd->element[j].type&ST_TRAITLOCUS) {
		if(traitlocus) {
			traitlocus[*i].element=sd->element+j;
			traitlocus[*i].var=sd;
			traitlocus[(*i)].index=j+1;
		}
		(*i)++;
	}
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "find_haplo"
static void find_haplo(struct bin_node *node)
{
	int j,k;
	struct scan_data *sd;
	
	sd=node->data;
	for(k=0;k<sd->n_elements;k++) if(sd->element[k].type&ST_HAPLO) {
		for(j=0;j<n_markers;j++) if(sd->element[k].arg.element==markers[j].element) {
			if(!markers[j].hap_element[0]) markers[j].hap_element[0]=sd->element+k;
			else if(!markers[j].hap_element[1])	markers[j].hap_element[1]=sd->element+k;
			else {
				if(markers[j].index) print_scan_err("Error: marker %s(%d) has >2 haplotype vectors associated with it\n",markers[j].var->name,markers[j].index);
				else print_scan_err("Error: marker %s has >2 haplotype vectors associated with it\n",markers[j].var->name);
			}
			break;
		}
		if(j==n_markers) ABT_FUNC("Internal error: can not find marker for haplotype vector\n");
	}
}

static void strip_names(struct bin_node *node)
{
	char *p;
	int i;
	struct scan_data *sd;
	
	sd=node->data;
	if((p=sd->name)) {
		i=strlen(p);
		if(i>2) {
			if(p[i-1]=='_' && p[0]=='_') {
				p[i-1]=0;
				memmove(p,p+1,i-1);
			}
		}
	}
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "ReadControl"
int ReadControl(FILE *fptr,char *cname,char **lfile)
{
	int i,j,k;
	void yy_cleanup(void);
	struct InFile *infile,**infile_p;
	struct Restrict *res,*res1,**res_p;
	struct Censor *cen,**cen_p;
	struct var_element *elem;
	struct Link *linkp;
	struct operation *ops;
	struct express tmp_expr;
	
	yyin=fptr;
	fname_list[0]=cname;
	list_ptr=0;
	if(!(f_atom_list=malloc(sizeof(struct format_atom)*f_atom_size))) ABT_FUNC(MMsg);
	for(i=0;i<NUM_SYSTEM_VAR;i++) syst_var[i]=0;
	syst_var[PRUNE_OPTION]=syst_var[RECODE_OPTION]=2;
	syst_var[ERROR_CHECK]=1;
	if((i=yyparse())) print_scan_err("Error: yyparse returned error %d\n",i);
	yy_cleanup();
	if(strip_vars) check_vars_1(root_var,strip_names);
	/* Sanity check! */
	if(!scan_error_n)	{
		if(!Infiles) print_scan_err("Error: No input files specified\n");
		if(!pedflag) print_scan_err("Error: No pedigree variables specified\n");
		else {
			for(i=0;i<3;i++) if(pedlist[i+family_id]->type&ST_INTTYPE) break;
			if(i<3) for(i=0;i<3;i++) pedlist[i+family_id]->type|=ST_INTTYPE;
		}
		if(root_var) Check_var(root_var);
		/* Flag variables used as the operands to a restriction statement *whose result is used* as ST_REQUIRED */
		res=0;
		while(res!=Restrictions) {	
			res1=Restrictions;
			while(res1->next!=res) res1=res1->next;
			for(i=j=0;i<res1->nvar;i++) if(res1->element[i]->type&ST_REQUIRED) {
				j=1;
				break;
			}
			if(!res1->nvar || j) {
				ops=res1->Op_List;
				while(ops) {
					if(ops->type==VARIABLE) ops->arg.element->type|=ST_REQUIRED;
					ops=ops->next;
				}
			}
			res=res1;
		}
		/* Delete restrict structures that are not used */
		res=Restrictions;
		res_p= &Restrictions;
		while(res) {
			for(i=j=0;i<res->nvar;i++) if(res->element[i]->type&ST_REQUIRED) {
				j=1;
				break;
			}
			if(res->nvar && !j) {
				*res_p=res->next;
				free_restrict(res);
				res= *res_p;
			} else {
				res_p= &res->next;
				res=res->next;
			}
		}
		if(Unaffected && !Affected) print_scan_err("Error: Unaffected definition without affected definition\n");
		/* Flag variables used in censored statements as required.  Delete unused censored statements */
		cen=Censored;
		cen_p= &Censored;
		while(cen) {
			if(cen->element->type&ST_TRAIT) {
				ops=cen->Op_List;
				while(ops) {
					if(ops->type==VARIABLE) ops->arg.element->type|=ST_REQUIRED;
					ops=ops->next;
				}
				cen_p= &cen->next;
				cen=cen->next;
			} else {
				*cen_p=cen->next;
				free_op(cen->Op_List);
				free(cen);
				cen= *cen_p;
			}
		}
		if((ops=Affected)) {
			while(ops) {
				if(ops->type==VARIABLE) ops->arg.element->type|=ST_REQUIRED;
				ops=ops->next;
			}
		}
		if((ops=Unaffected)) {
			while(ops) {
				if(ops->type==VARIABLE) ops->arg.element->type|=ST_REQUIRED;
				ops=ops->next;
			}
		}
		/* Check file structures - remove ones that aren't needed */
		infile=Infiles;
		infile_p= &Infiles;
		while(infile) {
			infile->ncol=0;
			for(i=0;i<infile->nvar;i++) {
				elem=infile->element[i];
			   if(elem) elem->type&=~ST_FLAG;
			}
			for(k=infile->nvar-1;k>=0;k--) {
				elem=infile->element[k];
				if(elem && (elem->type&ST_REQUIRED)) break;
			}
			for(i=j=0;i<infile->nvar;i++)	{
				elem=infile->element[i];
				if(elem) {
					if((elem->type&ST_MARKER) && !(elem->type&ST_REQUIRED)) {
						if(j<k) elem->type|=(ST_REQUIRED|ST_NOT_REALLY_REQUIRED);
						else elem->type=0;
					}
					if(elem->type&ST_REQUIRED) {
						if(elem->type&ST_FLAG) {
							print_scan_err("Error: Duplicate variables for file %s\n",infile->name);
							break;
						}
						elem->type|=ST_FLAG;
						if(elem->type&ST_ID)	{
							j|=1;
							infile->id_col=infile->ncol;
						} else if(elem->type&ST_FAMILY) {
							j|=2;
							infile->family_col=infile->ncol;
						}
						infile->ncol++;
					} else infile->element[i]=0;
				}
			}
			for(i=0;i<infile->nvar;i++) if(infile->element[i]) infile->element[i]->type&=~ST_FLAG;
			if(!(j&1)) print_scan_err("Error: No id column for file %s\n",infile->name);
			else if(family_id && j!=3) print_scan_err("Error: No family column for file %s\n",infile->name);
			if(infile->ncol==1) {
				*infile_p=infile->next;
				free_infile(infile);
				infile= *infile_p;
			} else {
				infile_p= &infile->next;
				infile=infile->next;
			}
		}
		if(!Infiles) print_scan_err("Error: No input files with data\n");
		free(f_atom_list);
		/* Count markers and link up with haplotype vectors */
		if(n_markers) {
			if(!(markers=calloc((size_t)n_markers,sizeof(struct Marker)))) ABT_FUNC(MMsg);
			for(i=0;i<n_markers;i++) {
				markers[i].allele_trans=0;
			   markers[i].order=0;
				markers[i].o_size=0;
			}
			i=0;
			if(root_var) {
				check_vars(root_var,&i,find_markers);
				check_vars_1(root_var,find_haplo);
			}
			n_markers=i;
			for(i=0;i<n_markers;i++) {
				if(markers[i].element->type&ST_NOT_REALLY_REQUIRED) continue;
				linkp=links;
				j=0;
				while(linkp) {
					j++;
					for(k=0;k<linkp->n_loci;k++) {
						if(linkp->element[k]==markers[i].element) {
							markers[i].link=j;
							break;
						}
					}
					if(k<linkp->n_loci) break;
					linkp=linkp->next;
				}
				if(!linkp) {
					if(markers[i].var->vtype&ST_ARRAY)
					  abt(__FILE__,__LINE__,"%s(): No linkage group specified for candidate gene %s(%d)\n",FUNC_NAME,markers[i].var->name,markers[i].index);
					else abt(__FILE__,__LINE__,"%s(): No linkage group specified for candidate gene %s\n",FUNC_NAME,markers[i].var->name);
				}
				if(markers[i].hap_element[0]) {
					if(markers[i].element->type&ST_DATA) {
						if(markers[i].var->vtype&ST_ARRAY)
						  print_scan_err("Error: marker variable %s(%d) can not have both genotype and haplotype data\n",markers[i].var->name,markers[i].index);
						else
						  print_scan_err("Error: marker variable %s can not have both genotype and haplotype data\n",markers[i].var->name);
					}
					if(markers[i].hap_element[0]->type&ST_INTTYPE) markers[i].hap_element[1]->type|=ST_INTTYPE;
					if(markers[i].hap_element[1] && markers[i].hap_element[1]->type&ST_INTTYPE) markers[i].hap_element[0]->type|=ST_INTTYPE;
				} else {
					if(!(markers[i].element->type&ST_DATA)) {
						if(markers[i].var->vtype&ST_ARRAY)
						  print_scan_err("Error: marker variable %s(%d) has no data\n",markers[i].var->name,markers[i].index);
						else
						  print_scan_err("Error: marker variable %s has no data\n",markers[i].var->name);
					}
				}
			}
		}
		i=0;
		if(root_var) check_vars(root_var,&i,find_trait_loci);
		if(i) {
			if(i>1) print_scan_err("Error: multiple trait loci indicated\n");
			else {
				if(!(traitlocus=calloc(1,sizeof(struct Marker)))) ABT_FUNC(MMsg);
				traitlocus->order=0;
				traitlocus->o_size=0;
				i=0;
				check_vars(root_var,&i,find_trait_loci);
			}
		}
		if(Models && Models->next && !syst_var[MULTIVARIATE_TEST]) {
			print_scan_err("Error: Multiple models not currently supported\n");
		}
	}
	*lfile=LogFile;
	if(!scan_error_n && !Miss && !syst_var[NO_DEFAULT_MISSING]) {
		tmp_expr.arg.string=strdup("0");
		tmp_expr.type=ST_STRING;
		do_missing_com(&tmp_expr,0,strdup("PF"));
	}
	return scan_error_n;
}

