/* A Bison parser, made from control_parse.y, by GNU bison 1.75.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef BISON_Y_TAB_H
# define BISON_Y_TAB_H

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     FILEC = 258,
     MARKER = 259,
     LOCUS = 260,
     TRAIT = 261,
     RANDOM = 262,
     PEDIGREE = 263,
     LOG = 264,
     MODEL = 265,
     FILTER = 266,
     LINK = 267,
     MISSING = 268,
     FACTOR = 269,
     BREAK = 270,
     DOLOOP = 271,
     WHILE = 272,
     USE = 273,
     WHERE = 274,
     ORSYMBOL = 275,
     ANDSYMBOL = 276,
     NEQSYMBOL = 277,
     LEQSYMBOL = 278,
     GEQSYMBOL = 279,
     NOTSYMBOL = 280,
     LOGICAL = 281,
     SHELL = 282,
     ARRAY = 283,
     PRINTEXP = 284,
     INCLUDE = 285,
     RAWOUTPUT = 286,
     LOOP_CLAUSE_START = 287,
     LOOP_CLAUSE_END = 288,
     CONSTANT = 289,
     MULTIPLE = 290,
     RSFORMAT = 291,
     FSFORMAT = 292,
     SKIPFORMAT = 293,
     GSFORMAT = 294,
     CENSORED = 295,
     GROUP = 296,
     SET = 297,
     GENDER = 298,
     AFFECTED = 299,
     OUTPUT = 300,
     ERRORDIR = 301,
     LAUROUTPUT = 302,
     UNAFFECTED = 303,
     POSITION = 304,
     FREQUENCY = 305,
     STRING = 306,
     VARIABLE = 307,
     ASSIGN = 308,
     ARRAY_VAR = 309,
     INTEGER = 310,
     SYSTEM_VAR = 311,
     REAL = 312,
     UMINUS = 313
   };
#endif
#define FILEC 258
#define MARKER 259
#define LOCUS 260
#define TRAIT 261
#define RANDOM 262
#define PEDIGREE 263
#define LOG 264
#define MODEL 265
#define FILTER 266
#define LINK 267
#define MISSING 268
#define FACTOR 269
#define BREAK 270
#define DOLOOP 271
#define WHILE 272
#define USE 273
#define WHERE 274
#define ORSYMBOL 275
#define ANDSYMBOL 276
#define NEQSYMBOL 277
#define LEQSYMBOL 278
#define GEQSYMBOL 279
#define NOTSYMBOL 280
#define LOGICAL 281
#define SHELL 282
#define ARRAY 283
#define PRINTEXP 284
#define INCLUDE 285
#define RAWOUTPUT 286
#define LOOP_CLAUSE_START 287
#define LOOP_CLAUSE_END 288
#define CONSTANT 289
#define MULTIPLE 290
#define RSFORMAT 291
#define FSFORMAT 292
#define SKIPFORMAT 293
#define GSFORMAT 294
#define CENSORED 295
#define GROUP 296
#define SET 297
#define GENDER 298
#define AFFECTED 299
#define OUTPUT 300
#define ERRORDIR 301
#define LAUROUTPUT 302
#define UNAFFECTED 303
#define POSITION 304
#define FREQUENCY 305
#define STRING 306
#define VARIABLE 307
#define ASSIGN 308
#define ARRAY_VAR 309
#define INTEGER 310
#define SYSTEM_VAR 311
#define REAL 312
#define UMINUS 313




#ifndef YYSTYPE
#line 44 "control_parse.y"
typedef union {
	char *string;
	struct bin_node *var;
	int value;
	double rvalue;
	struct format_clause *format_clause;
	struct fformat *fformat;
	struct format_atom *f_atom;
	struct model_list *model_list;
	struct var_list *var_list;
	struct var_element *element;
	struct express *express;
} yystype;
/* Line 1281 of /usr/local/share/bison/yacc.c.  */
#line 170 "y.tab.h"
# define YYSTYPE yystype
#endif

extern YYSTYPE yylval;


#endif /* not BISON_Y_TAB_H */

