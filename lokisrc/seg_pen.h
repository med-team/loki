extern int pass_founder_genes1(const int,int,int);
extern int pass_founder_genes1a(const int,const int *,const int,int);
extern int pass_founder_genes1b(const int,const int *,const int);
extern int pass_founder_genes2(const int locus,const int comp,int **seg);
extern void pass_founder_genes(const int);
extern double seg_pen(int,int,int *,int,int);
extern void seg_alloc(void),seg_dealloc(void);
extern void pass_founder_genes_alloc(void);
extern void pass_founder_genes_dealloc(void);
extern void seg_init_freq(int);
extern void seg_sample_freq(int);
extern void seg_update_aff_freq(int);

extern int **tl_group;
extern double **seg_count;

struct nuc_family {
	int *kids;
	int nkids;
};

struct cg_stack {
	int id;
	int kid_ptr;
	int par_flag;
};
