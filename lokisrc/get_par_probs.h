double get_par_probs(double *,const int,const int,pen_func,lk_ulong **,double **,
							struct R_Func *);
double get_par_probs_x(double *,const int,const int,pen_func,lk_ulong **,double **,
							struct R_Func *);
double get_trait_par_probs(double *,const int,const int,trait_pen_func *,double **,struct R_Func *);

