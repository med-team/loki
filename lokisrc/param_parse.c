/* A Bison parser, made from param_parse.y
   by GNU bison 1.35.  */

#define YYBISON 1  /* Identify Bison output.  */

# define	RESIDUAL	257
# define	GENETIC	258
# define	VARIANCE	259
# define	POSITION	260
# define	FREQUENCY	261
# define	VIRTUAL	262
# define	START	263
# define	MEAN	264
# define	ITERATIONS	265
# define	SAMPLE	266
# define	FROM	267
# define	OUTPUT	268
# define	MAP	269
# define	TOTAL	270
# define	SEED	271
# define	SFILE	272
# define	SEEDFILE	273
# define	TRAIT	274
# define	LOCI	275
# define	SET	276
# define	SYSTEM_VAR	277
# define	TIMECOM	278
# define	ESTIMATE	279
# define	IBD	280
# define	GROUP	281
# define	ORDER	282
# define	MALE	283
# define	FEMALE	284
# define	LIMIT	285
# define	AFFECTED	286
# define	PHENO	287
# define	GENO	288
# define	COUNTS	289
# define	DUMP	290
# define	TYPE	291
# define	ANALYZE	292
# define	NORMAL	293
# define	STUDENT_T	294
# define	HAPLO	295
# define	INCLUDE	296
# define	FUNCTION	297
# define	HALDANE	298
# define	KOSAMBI	299
# define	RECOMB	300
# define	POLYGENIC	301
# define	MARKERS	302
# define	GRID	303
# define	COMPRESS	304
# define	DIR	305
# define	STRING	306
# define	INTEGER	307
# define	REAL	308

#line 1 "param_parse.y"

/****************************************************************************
 *                                                                          *
 *     Loki - Programs for genetic analysis of complex traits using MCMC    *
 *                                                                          *
 *             Simon Heath - University of Washington                       *
 *                                                                          *
 *                       July 1997                                          *
 *                                                                          *
 * param_parse.y:                                                           *
 *                                                                          *
 * yacc source for parameter file parser.                                   *
 *                                                                          *
 ****************************************************************************/

#include <stdlib.h>
#include <string.h>
#ifdef USE_DMALLOC
#include <dmalloc.h>
#endif
#include <stdarg.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
	
#include "utils.h"
#include "loki_scan.h"
#include "loki_ibd.h"
#include "shared_peel.h"
#include "mat_utils.h"
#include "loki.h"
#include "ranlib.h"
#include "output_recomb.h"

#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#ifndef YYMAXDEPTH
#define YYMAXDEPTH 0
#endif
#ifndef __GNUC__
#define __GNUC__ 0
#endif

extern FILE *yyin;
double *residual_var,*residual_var_limit,*additive_var,*additive_var_limit,*grand_mean,limit_time,tloci_mean;
int *res_var_set,*add_var_set,*grand_mean_set,num_iter,max_tloci=16,min_tloci,sex_map,iflag;
int tloci_mean_set,start_tloci=-1,dump_freq,output_type=DEFAULT_OUTPUT_TYPE,limit_timer_type;
int sample_from[2]={0,0};
int sample_freq[2]={1,1};
static int start_flag=1,compress_ibd,ibd_mode;
static struct Marker *freq_marker;
static int check_variance(double,int);
static void set_position(struct lk_variable *, double, double);
static struct lk_variable *find_var(char *, int, int);
static struct Marker *check_marker(struct lk_variable *);
static int find_allele(char *, struct Marker *,int, int);
static int find_group(char *,int, int);
static int find_trait(struct lk_variable *);
static void set_output_gen(char *,char *);
static struct IBD_List *add_ibd_list(double,struct IBD_List *);
static void set_freq(struct Marker *, double,int);
static void set_map_range(char *,double,double, int);
static void set_tloci(int,int);	
static void set_ibd_list(char *,struct IBD_List *,int);
static void set_ibd_markers(char *);
static void set_output(struct lk_variable *);
static void set_group_order(int);
static void set_analyze(char *);
static void set_ibd_mode(char *);
static struct Variable *group_var;
static int group_ptr,*group_order,group_counter,freq_allele,c_flag;
extern void yyerror(char *s),print_scan_err(char *fmt, ...);
extern int yyparse(void),yylex(void),lineno,lineno1,tokenpos;
extern char *yytext,linebuf[];
char *Output_Phen,*Outputfile,*Dumpfile,*Freqfile,*Haplofile,*Polyfile;
char *OutputPosfile,*OutputIBDfile,*OutputIBDdir;

struct output_gen *Output_Gen;

static int max_scan_errors=30;
static int scan_warn_n,max_scan_warnings=30;
static int *ran_flag;
int scan_error_n,output_haplo;

struct id_data syst_var[NUM_SYSTEM_VAR];


#line 90 "param_parse.y"
#ifndef YYSTYPE
typedef union  {
	char *string;
	int value;
	double rvalue;
	struct IBD_List *rlist;
	struct lk_variable *lk_var;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
#ifndef YYDEBUG
# define YYDEBUG 0
#endif



#define	YYFINAL		293
#define	YYFLAG		-32768
#define	YYNTBASE	59

/* YYTRANSLATE(YYLEX) -- Bison token number corresponding to YYLEX. */
#define YYTRANSLATE(x) ((unsigned)(x) <= 308 ? yytranslate[x] : 109)

/* YYTRANSLATE[YYLEX] -- Bison token number corresponding to YYLEX. */
static const char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      56,    57,    58,     2,    55,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54
};

#if YYDEBUG
static const short yyprhs[] =
{
       0,     0,     1,     4,     6,     7,    11,    14,    16,    18,
      19,    23,    25,    27,    29,    31,    33,    35,    37,    39,
      41,    43,    45,    47,    49,    51,    53,    55,    57,    59,
      61,    63,    65,    69,    75,    79,    85,    89,    93,    97,
      98,   102,   103,   105,   111,   115,   120,   124,   131,   136,
     140,   144,   148,   150,   152,   154,   156,   160,   161,   165,
     169,   173,   178,   183,   186,   191,   195,   201,   204,   210,
     217,   224,   228,   234,   239,   244,   248,   254,   259,   264,
     268,   272,   276,   281,   287,   292,   295,   299,   305,   309,
     313,   319,   323,   327,   333,   336,   340,   344,   349,   354,
     359,   363,   367,   371,   374,   378,   382,   386,   392,   396,
     401,   406,   412,   417,   423,   427,   432,   437,   443,   448,
     454,   457,   461,   465,   471,   472,   477,   478,   484,   485,
     490,   492,   494,   499,   501,   505,   507,   508,   513,   515,
     517,   519,   521,   525,   527,   529,   531,   532,   537,   538,
     544,   546,   548,   552,   556,   558,   562,   564
};
static const short yyrhs[] =
{
      -1,    60,    62,     0,     1,     0,     0,    59,    61,    62,
       0,    59,     1,     0,    64,     0,    65,     0,     0,     9,
      63,    64,     0,    84,     0,    86,     0,    89,     0,    90,
       0,    88,     0,    82,     0,    66,     0,    83,     0,    80,
       0,    79,     0,    81,     0,    67,     0,    71,     0,    98,
       0,    85,     0,    87,     0,    76,     0,    68,     0,    73,
       0,    78,     0,    72,     0,    12,    13,    53,     0,    12,
      13,    53,    70,    53,     0,     9,    14,    53,     0,     9,
      14,    53,    70,    53,     0,    22,    23,    53,     0,    22,
      23,    54,     0,    22,    52,   108,     0,     0,    42,    69,
      52,     0,     0,    55,     0,    25,    26,    52,    70,   107,
       0,    25,    26,   107,     0,    25,    26,    48,    52,     0,
      25,    26,    48,     0,    25,    26,    49,    52,    70,   107,
       0,    25,    26,    49,   107,     0,    50,    26,    14,     0,
      50,    14,    26,     0,    25,    32,     7,     0,    52,     0,
      32,     0,    26,     0,    74,     0,    75,    55,    74,     0,
       0,    38,    77,    75,     0,    24,    31,   108,     0,    31,
      24,   108,     0,    31,     8,    24,   108,     0,     8,    24,
      31,   108,     0,    19,    52,     0,    19,    52,    55,    53,
       0,    17,    18,    52,     0,    17,    18,    52,    55,    53,
       0,    17,    53,     0,    15,    52,   108,    55,   108,     0,
      29,    15,    52,   108,    55,   108,     0,    30,    15,    52,
     108,    55,   108,     0,    16,    15,   108,     0,    16,    15,
     108,    55,   108,     0,    16,    29,    15,   108,     0,    16,
      30,    15,   108,     0,    15,    16,   108,     0,    15,    16,
     108,    55,   108,     0,    29,    15,    16,   108,     0,    30,
      15,    16,   108,     0,    15,    43,    44,     0,    15,    43,
      45,     0,    20,    21,    53,     0,     9,    20,    21,    53,
       0,    20,    21,    53,    55,    53,     0,    20,    21,    10,
     108,     0,    11,    53,     0,    14,     7,    53,     0,    14,
       7,    53,    55,    53,     0,    14,     7,    52,     0,    12,
       7,    53,     0,    12,     7,    53,    55,    53,     0,    14,
      33,    52,     0,    14,    34,    52,     0,    14,    34,    52,
      55,    52,     0,    14,    96,     0,    14,    37,    53,     0,
      14,    18,    52,     0,    14,     6,    18,    52,     0,    14,
      26,    18,    52,     0,    14,    26,    51,    52,     0,    36,
      18,    52,     0,    36,     7,    53,     0,    14,    41,    52,
       0,    14,    41,     0,    14,    46,    52,     0,    14,    47,
      52,     0,    14,    26,    52,     0,    14,    26,    52,    55,
      52,     0,     3,     5,   108,     0,     3,     5,    94,   108,
       0,     3,     5,    31,   108,     0,     3,     5,    31,    94,
     108,     0,    31,     3,     5,   108,     0,    31,     3,     5,
      94,   108,     0,     4,     5,   108,     0,     4,     5,    94,
     108,     0,     4,     5,    31,   108,     0,     4,     5,    31,
      94,   108,     0,    31,     4,     5,   108,     0,    31,     4,
       5,    94,   108,     0,    10,   108,     0,    10,    94,   108,
       0,     6,    95,   108,     0,     6,    95,   108,    55,   108,
       0,     0,     7,    91,    97,   103,     0,     0,     7,    92,
      35,    97,   103,     0,     0,    35,    93,    97,   103,     0,
      95,     0,    52,     0,    52,    56,    53,    57,     0,    95,
       0,    96,    55,    95,     0,    95,     0,     0,    27,    28,
      99,   101,     0,    53,     0,    54,     0,    52,     0,   100,
       0,   101,    55,   100,     0,    53,     0,    54,     0,    52,
       0,     0,   102,    55,   104,   106,     0,     0,   103,   102,
      55,   105,   106,     0,   108,     0,    58,     0,   106,    55,
     108,     0,   106,    55,    58,     0,   108,     0,   107,    55,
     108,     0,    54,     0,    53,     0
};

#endif

#if YYDEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined. */
static const short yyrline[] =
{
       0,   116,   116,   117,   118,   118,   119,   122,   123,   124,
     124,   127,   128,   129,   130,   131,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   152,   153,   154,   155,   158,   159,   160,   163,
     163,   166,   167,   170,   171,   172,   173,   174,   175,   178,
     179,   182,   185,   186,   187,   190,   191,   194,   194,   197,
     198,   199,   200,   203,   204,   205,   206,   207,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,   225,   226,
     227,   230,   231,   232,   233,   236,   238,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   254,   255,   256,   257,   258,   259,   262,   263,
     266,   267,   268,   269,   272,   273,   276,   277,   278,   279,
     282,   284,   287,   288,   291,   291,   292,   292,   293,   293,
     296,   298,   299,   302,   303,   306,   308,   308,   311,   312,
     313,   316,   317,   320,   321,   322,   325,   325,   326,   326,
     329,   330,   331,   332,   335,   336,   339,   340
};
#endif


#if (YYDEBUG) || defined YYERROR_VERBOSE

/* YYTNAME[TOKEN_NUM] -- String name of the token TOKEN_NUM. */
static const char *const yytname[] =
{
  "$", "error", "$undefined.", "RESIDUAL", "GENETIC", "VARIANCE", 
  "POSITION", "FREQUENCY", "VIRTUAL", "START", "MEAN", "ITERATIONS", 
  "SAMPLE", "FROM", "OUTPUT", "MAP", "TOTAL", "SEED", "SFILE", "SEEDFILE", 
  "TRAIT", "LOCI", "SET", "SYSTEM_VAR", "TIMECOM", "ESTIMATE", "IBD", 
  "GROUP", "ORDER", "MALE", "FEMALE", "LIMIT", "AFFECTED", "PHENO", 
  "GENO", "COUNTS", "DUMP", "TYPE", "ANALYZE", "NORMAL", "STUDENT_T", 
  "HAPLO", "INCLUDE", "FUNCTION", "HALDANE", "KOSAMBI", "RECOMB", 
  "POLYGENIC", "MARKERS", "GRID", "COMPRESS", "DIR", "STRING", "INTEGER", 
  "REAL", "','", "'('", "')'", "'*'", "parmfile", "@1", "@2", "command1", 
  "@3", "command", "command_a", "samplecommand", "setcommand", 
  "includecommand", "@4", "opt_comma", "ibdcommand", "compresscommand", 
  "aff_freqcommand", "analyzecom", "analyzelist", "analyzecommand", "@5", 
  "limit_timecommand", "seedcommand", "mapcommand", "tlocicommand", 
  "itercommand", "outputcommand", "resvarcommand", "limitresvarcommand", 
  "addvarcommand", "limitaddvarcommand", "meancommand", "positioncommand", 
  "frequencycommand", "@6", "@7", "@8", "trait_var", "lkvar", "lkvarlist", 
  "lkmarker", "groupcommand", "@9", "group", "grouplist", "allele", 
  "freqlist", "@10", "@11", "freqlist1", "ibdlist", "rnum", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives. */
static const short yyr1[] =
{
       0,    60,    59,    59,    61,    59,    59,    62,    62,    63,
      62,    64,    64,    64,    64,    64,    65,    65,    65,    65,
      65,    65,    65,    65,    65,    65,    65,    65,    65,    65,
      65,    65,    66,    66,    66,    66,    67,    67,    67,    69,
      68,    70,    70,    71,    71,    71,    71,    71,    71,    72,
      72,    73,    74,    74,    74,    75,    75,    77,    76,    78,
      78,    78,    78,    79,    79,    79,    79,    79,    80,    80,
      80,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      80,    81,    81,    81,    81,    82,    83,    83,    83,    83,
      83,    83,    83,    83,    83,    83,    83,    83,    83,    83,
      83,    83,    83,    83,    83,    83,    83,    83,    84,    84,
      85,    85,    85,    85,    86,    86,    87,    87,    87,    87,
      88,    88,    89,    89,    91,    90,    92,    90,    93,    90,
      94,    95,    95,    96,    96,    97,    99,    98,   100,   100,
     100,   101,   101,   102,   102,   102,   104,   103,   105,   103,
     106,   106,   106,   106,   107,   107,   108,   108
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN. */
static const short yyr2[] =
{
       0,     0,     2,     1,     0,     3,     2,     1,     1,     0,
       3,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     5,     3,     5,     3,     3,     3,     0,
       3,     0,     1,     5,     3,     4,     3,     6,     4,     3,
       3,     3,     1,     1,     1,     1,     3,     0,     3,     3,
       3,     4,     4,     2,     4,     3,     5,     2,     5,     6,
       6,     3,     5,     4,     4,     3,     5,     4,     4,     3,
       3,     3,     4,     5,     4,     2,     3,     5,     3,     3,
       5,     3,     3,     5,     2,     3,     3,     4,     4,     4,
       3,     3,     3,     2,     3,     3,     3,     5,     3,     4,
       4,     5,     4,     5,     3,     4,     4,     5,     4,     5,
       2,     3,     3,     5,     0,     4,     0,     5,     0,     4,
       1,     1,     4,     1,     3,     1,     0,     4,     1,     1,
       1,     1,     3,     1,     1,     1,     0,     4,     0,     5,
       1,     1,     3,     3,     1,     3,     1,     1
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error. */
static const short yydefact[] =
{
       0,     3,     0,     0,     6,     0,     0,     0,     0,   124,
       0,     9,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   128,     0,
      57,    39,     0,     2,     7,     8,    17,    22,    28,    23,
      31,    29,    27,    30,    20,    19,    21,    16,    18,    11,
      25,    12,    26,    15,    13,    14,    24,     5,     0,     0,
     131,     0,     0,     0,     0,     0,     0,     0,   157,   156,
       0,   130,   120,    85,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   103,     0,     0,   133,    94,     0,     0,
       0,     0,     0,     0,     0,    67,    63,     0,     0,     0,
       0,     0,     0,   136,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   108,
       0,     0,   114,     0,   122,   135,     0,     0,     0,    34,
       0,     0,     0,    10,   121,    89,    32,     0,    88,    86,
      96,     0,     0,   106,    91,    92,    95,   102,   104,   105,
       0,    75,    79,    80,     0,    71,     0,     0,    65,     0,
       0,    81,    36,    37,    38,    59,    46,     0,    41,    44,
     154,    51,     0,     0,     0,     0,     0,     0,     0,     0,
      60,     0,   101,   100,    54,    53,    52,    55,    58,    40,
      50,    49,     0,   110,   109,     0,   116,   115,     0,     0,
     145,   143,   144,     0,   125,     0,    62,    42,     0,    82,
       0,     0,     0,     0,    97,     0,    98,    99,     0,     0,
     134,     0,     0,     0,    73,    74,     0,    64,    84,     0,
      45,    41,    48,     0,     0,   140,   138,   139,   141,   137,
      77,     0,    78,     0,     0,   112,     0,   118,    61,   129,
       0,   111,   117,   132,   123,   146,     0,   127,    35,    90,
      33,    87,   107,    93,    76,    68,    72,    66,    83,     0,
      43,   155,     0,     0,     0,   113,   119,    56,     0,   148,
      47,   142,    69,    70,   151,   147,   150,     0,     0,   149,
     153,   152,     0,     0
};

static const short yydefgoto[] =
{
       2,     3,     5,    33,    67,    34,    35,    36,    37,    38,
     114,   208,    39,    40,    41,   187,   188,    42,   113,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    62,    63,   110,   118,    71,    87,   126,    56,
     172,   238,   239,   203,   204,   278,   287,   285,   169,   170
};

static const short yypact[] =
{
     322,-32768,   277,   359,-32768,   359,    10,    14,   -29,     5,
      33,    50,    85,   -17,    55,    48,    -9,    89,   -14,    23,
      59,     4,    52,    39,    68,    84,    92,    69,-32768,    11,
  -32768,-32768,    12,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   -21,    38,
      56,     7,   -29,    82,    91,    74,   102,    18,-32768,-32768,
       7,-32768,-32768,-32768,    80,    90,   117,    49,    97,   -10,
      98,   106,   107,   116,   120,   121,-32768,   104,     7,    70,
       7,     7,   154,   159,   123,-32768,   122,    -8,    93,     7,
       7,    72,   169,-32768,    -4,    -2,   173,   174,   157,     7,
     -29,   129,   132,   -15,   133,   160,   175,    85,     7,-32768,
      85,     7,-32768,   135,   136,-32768,    88,   -29,     7,    31,
     137,   187,   188,-32768,-32768,   139,    31,   143,-32768,   141,
  -32768,   145,   148,   146,-32768,   147,-32768,-32768,-32768,-32768,
     -29,   150,-32768,-32768,   151,   152,     7,     7,   153,   161,
       7,   158,-32768,-32768,-32768,-32768,   163,   100,   162,   164,
  -32768,-32768,   103,     7,     7,     7,     7,    85,    85,     7,
  -32768,    88,-32768,-32768,-32768,-32768,-32768,-32768,   165,-32768,
  -32768,-32768,     7,-32768,-32768,     7,-32768,-32768,   155,     7,
  -32768,-32768,-32768,   166,    88,    88,-32768,-32768,   170,-32768,
      85,    85,   171,   172,-32768,   176,-32768,-32768,   178,   179,
  -32768,     7,     7,     7,-32768,-32768,   180,-32768,-32768,   182,
  -32768,   162,   164,     7,     7,-32768,-32768,-32768,-32768,   181,
  -32768,   183,-32768,   184,     7,-32768,     7,-32768,-32768,    88,
     -15,-32768,-32768,-32768,-32768,-32768,   185,    88,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,     7,
     164,-32768,   103,     7,     7,-32768,-32768,-32768,    76,-32768,
     164,-32768,-32768,-32768,-32768,   186,-32768,    76,    78,   186,
  -32768,-32768,   203,-32768
};

static const short yypgoto[] =
{
  -32768,-32768,-32768,   211,-32768,   177,-32768,-32768,-32768,-32768,
  -32768,  -133,-32768,-32768,-32768,   -32,-32768,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,    -7,     1,-32768,   -97,-32768,
  -32768,   -46,-32768,  -198,  -161,-32768,-32768,   -60,  -166,   -12
};


#define	YYLAST		409


static const short yytable[] =
{
      72,   232,   160,   213,    94,    70,   256,    88,   141,    61,
     117,   184,   173,   181,   175,    58,    86,   185,   111,    59,
     249,   131,   132,    60,     8,     9,   115,    98,    12,   112,
     205,    60,    68,    69,    89,   233,    73,   186,   116,    95,
    -126,   142,   143,    90,   257,   161,   119,   122,   174,   124,
     176,   256,   121,    28,    76,    77,    99,    64,   134,   256,
      68,    69,    74,   125,    65,   101,    78,   270,    75,   120,
      66,   102,   106,   107,    79,    96,   151,   108,   154,   155,
      97,    80,    81,   100,   -41,    82,   207,   164,   165,    83,
      60,    68,    69,   109,    84,    85,   103,   180,   269,   104,
      60,   138,   139,   280,    91,   193,   194,   105,   196,   197,
     192,   125,   123,   195,   152,   153,   206,   127,    92,    93,
     166,   167,   128,   130,   168,    68,    69,   129,   125,    68,
      69,    68,    69,   135,   284,   137,   290,    60,    68,    69,
     200,   201,   202,   136,   224,   225,   162,   163,   228,   140,
     144,   220,   231,    68,    69,   235,   236,   237,   145,   150,
     146,   240,   241,   242,   243,   245,   247,   248,   147,   156,
     244,   246,   148,   149,   157,   158,   171,   159,   177,   178,
     251,   179,   182,   252,   183,   189,   190,   254,   198,   191,
     209,   199,   210,   211,   212,   214,   215,   216,   119,   122,
     217,   218,   219,   293,   121,   221,   222,   223,   226,   264,
     265,   266,   253,   229,   227,   230,    57,   207,   277,   234,
     250,   255,   271,   258,   259,   260,   281,   289,     0,   261,
     262,   263,   275,   267,   276,   268,   272,     0,   273,   274,
     279,   288,     0,     0,   133,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   282,   283,     0,     0,     0,   286,     0,     0,     0,
       0,     0,     0,     0,     0,   286,   291,   292,     4,     0,
      -4,    -4,     0,    -4,    -4,    -4,    -4,    -4,    -4,    -4,
       0,    -4,    -4,    -4,    -4,     0,    -4,    -4,     0,    -4,
       0,    -4,    -4,     0,    -4,     0,    -4,    -4,    -4,     0,
       0,     0,    -4,    -4,     0,    -4,     0,     0,     0,    -4,
       0,     0,     0,     1,     0,    -1,    -1,    -4,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     0,    -1,    -1,    -1,    -1,
       0,    -1,    -1,     0,    -1,     0,    -1,    -1,     0,    -1,
       0,    -1,    -1,    -1,     0,     0,     0,    -1,    -1,     0,
      -1,     0,     6,     7,    -1,     8,     9,    10,    11,    12,
      13,    14,    -1,    15,    16,    17,    18,     0,    19,    20,
       0,    21,     0,    22,    23,     0,    24,     0,    25,    26,
      27,     0,     0,     0,    28,    29,     0,    30,     0,     0,
       0,    31,     0,     0,     0,     0,     0,     0,     0,    32
};

static const short yycheck[] =
{
      12,   167,    10,   136,    18,    12,   204,    16,    18,     8,
      31,    26,    16,   110,    16,     5,    15,    32,     7,     5,
     181,     3,     4,    52,     6,     7,    14,    23,    10,    18,
     127,    52,    53,    54,    43,   168,    53,    52,    26,    53,
      35,    51,    52,    52,   205,    53,    58,    59,    52,    61,
      52,   249,    59,    35,     6,     7,    52,    24,    70,   257,
      53,    54,     7,    62,    14,    26,    18,   233,    13,    31,
      20,    32,     3,     4,    26,    52,    88,     8,    90,    91,
      21,    33,    34,    31,    53,    37,    55,    99,   100,    41,
      52,    53,    54,    24,    46,    47,    28,   109,   231,    15,
      52,    52,    53,   269,    15,   117,   118,    15,   120,   121,
     117,   110,    56,   120,    44,    45,   128,    35,    29,    30,
      48,    49,    31,    21,    52,    53,    54,    53,   127,    53,
      54,    53,    54,    53,    58,    18,    58,    52,    53,    54,
      52,    53,    54,    53,   156,   157,    53,    54,   160,    52,
      52,   150,    52,    53,    54,    52,    53,    54,    52,    55,
      53,   173,   174,   175,   176,   177,   178,   179,    52,    15,
     177,   178,    52,    52,    15,    52,     7,    55,     5,     5,
     192,    24,    53,   195,    52,    52,    26,   199,    53,    14,
      53,    55,     5,     5,    55,    52,    55,    52,   210,   211,
      52,    55,    55,     0,   211,    55,    55,    55,    55,   221,
     222,   223,    57,    55,    53,    52,     5,    55,   250,    55,
      55,    55,   234,    53,    53,    53,   272,   287,    -1,    53,
      52,    52,   244,    53,   246,    53,    55,    -1,    55,    55,
      55,    55,    -1,    -1,    67,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   273,   274,    -1,    -1,    -1,   278,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   287,   288,     0,     1,    -1,
       3,     4,    -1,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    15,    16,    17,    -1,    19,    20,    -1,    22,
      -1,    24,    25,    -1,    27,    -1,    29,    30,    31,    -1,
      -1,    -1,    35,    36,    -1,    38,    -1,    -1,    -1,    42,
      -1,    -1,    -1,     1,    -1,     3,     4,    50,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    15,    16,    17,
      -1,    19,    20,    -1,    22,    -1,    24,    25,    -1,    27,
      -1,    29,    30,    31,    -1,    -1,    -1,    35,    36,    -1,
      38,    -1,     3,     4,    42,     6,     7,     8,     9,    10,
      11,    12,    50,    14,    15,    16,    17,    -1,    19,    20,
      -1,    22,    -1,    24,    25,    -1,    27,    -1,    29,    30,
      31,    -1,    -1,    -1,    35,    36,    -1,    38,    -1,    -1,
      -1,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/local/share/bison/bison.simple"

/* Skeleton output parser for bison,

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software
   Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser when
   the %semantic_parser declaration is not specified in the grammar.
   It was written by Richard Stallman by simplifying the hairy parser
   used when %semantic_parser is specified.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#if ! defined (yyoverflow) || defined (YYERROR_VERBOSE)

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || defined (YYERROR_VERBOSE) */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYLTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
# if YYLSP_NEEDED
  YYLTYPE yyls;
# endif
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# if YYLSP_NEEDED
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAX)
# else
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)
# endif

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif


#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).

   When YYLLOC_DEFAULT is run, CURRENT is set the location of the
   first token.  By default, to implement support for ranges, extend
   its range to the last symbol.  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)       	\
   Current.last_line   = Rhs[N].last_line;	\
   Current.last_column = Rhs[N].last_column;
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#if YYPURE
# if YYLSP_NEEDED
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, &yylloc, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval, &yylloc)
#  endif
# else /* !YYLSP_NEEDED */
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval)
#  endif
# endif /* !YYLSP_NEEDED */
#else /* !YYPURE */
# define YYLEX			yylex ()
#endif /* !YYPURE */


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

#ifdef YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif
#endif

#line 315 "/usr/local/share/bison/bison.simple"


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
int yyparse (void *);
# else
int yyparse (void);
# endif
#endif

/* YY_DECL_VARIABLES -- depending whether we use a pure parser,
   variables are global, or local to YYPARSE.  */

#define YY_DECL_NON_LSP_VARIABLES			\
/* The lookahead symbol.  */				\
int yychar;						\
							\
/* The semantic value of the lookahead symbol. */	\
YYSTYPE yylval;						\
							\
/* Number of parse errors so far.  */			\
int yynerrs;

#if YYLSP_NEEDED
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES			\
						\
/* Location data for the lookahead symbol.  */	\
YYLTYPE yylloc;
#else
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES
#endif


/* If nonreentrant, generate the variables here. */

#if !YYPURE
YY_DECL_VARIABLES
#endif  /* !YYPURE */

int
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  /* If reentrant, generate the variables here. */
#if YYPURE
  YY_DECL_VARIABLES
#endif  /* !YYPURE */

  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack. */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

#if YYLSP_NEEDED
  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
#endif

#if YYLSP_NEEDED
# define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
# define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  YYSIZE_T yystacksize = YYINITDEPTH;


  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
#if YYLSP_NEEDED
  YYLTYPE yyloc;
#endif

  /* When reducing, the number of symbols on the RHS of the reduced
     rule. */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
#if YYLSP_NEEDED
  yylsp = yyls;
#endif
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  */
# if YYLSP_NEEDED
	YYLTYPE *yyls1 = yyls;
	/* This used to be a conditional around just the two extra args,
	   but that might be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
# else
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);
# endif
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
# if YYLSP_NEEDED
	YYSTACK_RELOCATE (yyls);
# endif
# undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
#if YYLSP_NEEDED
      yylsp = yyls + yysize - 1;
#endif

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

#if YYDEBUG
     /* We have to keep this `#if YYDEBUG', since we use variables
	which are defined only if `YYDEBUG' is set.  */
      if (yydebug)
	{
	  YYFPRINTF (stderr, "Next token is %d (%s",
		     yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise
	     meaning of a token, for further debugging info.  */
# ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
# endif
	  YYFPRINTF (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to the semantic value of
     the lookahead token.  This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

#if YYLSP_NEEDED
  /* Similarly for the default location.  Let the user run additional
     commands if for instance locations are ranges.  */
  yyloc = yylsp[1-yylen];
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
#endif

#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] > 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif

  switch (yyn) {

case 1:
#line 116 "param_parse.y"
{lineno1=lineno;}
    break;
case 2:
#line 116 "param_parse.y"
{iflag=0;}
    break;
case 3:
#line 117 "param_parse.y"
{iflag=0;}
    break;
case 4:
#line 118 "param_parse.y"
{lineno1=lineno;}
    break;
case 5:
#line 118 "param_parse.y"
{iflag=0;}
    break;
case 6:
#line 119 "param_parse.y"
{iflag=0;}
    break;
case 9:
#line 124 "param_parse.y"
{start_flag=2;}
    break;
case 10:
#line 124 "param_parse.y"
{start_flag=1;}
    break;
case 32:
#line 152 "param_parse.y"
{sample_from[0]=sample_from[1]=yyvsp[0].value;}
    break;
case 33:
#line 153 "param_parse.y"
{sample_from[0]=yyvsp[-2].value; sample_from[1]=yyvsp[0].value;}
    break;
case 34:
#line 154 "param_parse.y"
{sample_from[0]=sample_from[1]=yyvsp[0].value;}
    break;
case 35:
#line 155 "param_parse.y"
{sample_from[0]=yyvsp[-2].value; sample_from[1]=yyvsp[0].value;}
    break;
case 36:
#line 158 "param_parse.y"
{ syst_var[yyvsp[-1].value].data.value=yyvsp[0].value; syst_var[yyvsp[-1].value].flag=ST_INTEGER; }
    break;
case 37:
#line 159 "param_parse.y"
{ syst_var[yyvsp[-1].value].data.rvalue=yyvsp[0].rvalue; syst_var[yyvsp[-1].value].flag=ST_REAL; }
    break;
case 38:
#line 160 "param_parse.y"
{ free(yyvsp[-1].string); yyerror("Unrecognized system variable"); }
    break;
case 39:
#line 163 "param_parse.y"
{iflag=1;}
    break;
case 40:
#line 163 "param_parse.y"
{include_param_file(yyvsp[0].string);}
    break;
case 43:
#line 170 "param_parse.y"
{ set_ibd_list(yyvsp[-2].string,yyvsp[0].rlist,IBD_EST_DISCRETE); free(yyvsp[-2].string);}
    break;
case 44:
#line 171 "param_parse.y"
{ set_ibd_list(0,yyvsp[0].rlist,IBD_EST_DISCRETE); }
    break;
case 45:
#line 172 "param_parse.y"
{ set_ibd_markers(yyvsp[0].string); free(yyvsp[0].string);}
    break;
case 46:
#line 173 "param_parse.y"
{ set_ibd_markers(0); }
    break;
case 47:
#line 174 "param_parse.y"
{ set_ibd_list(yyvsp[-2].string,yyvsp[0].rlist,IBD_EST_GRID); free(yyvsp[-2].string);}
    break;
case 48:
#line 175 "param_parse.y"
{ set_ibd_list(0,yyvsp[0].rlist,IBD_EST_GRID); }
    break;
case 49:
#line 178 "param_parse.y"
{compress_ibd=1;}
    break;
case 50:
#line 179 "param_parse.y"
{compress_ibd=1;}
    break;
case 51:
#line 182 "param_parse.y"
{est_aff_freq=1;}
    break;
case 52:
#line 185 "param_parse.y"
{set_analyze(yyvsp[0].string); free(yyvsp[0].string);}
    break;
case 53:
#line 186 "param_parse.y"
{set_analyze("AFFECTED");}
    break;
case 54:
#line 187 "param_parse.y"
{set_analyze("IBD");}
    break;
case 57:
#line 194 "param_parse.y"
{analysis=0;}
    break;
case 59:
#line 197 "param_parse.y"
{limit_time=yyvsp[0].rvalue,limit_timer_type=ITIMER_REAL;}
    break;
case 60:
#line 198 "param_parse.y"
{limit_time=yyvsp[0].rvalue,limit_timer_type=ITIMER_REAL;}
    break;
case 61:
#line 199 "param_parse.y"
{limit_time=yyvsp[0].rvalue,limit_timer_type=ITIMER_VIRTUAL;}
    break;
case 62:
#line 200 "param_parse.y"
{limit_time=yyvsp[0].rvalue,limit_timer_type=ITIMER_VIRTUAL;}
    break;
case 63:
#line 203 "param_parse.y"
{if(Seedfile) free(Seedfile); Seedfile=yyvsp[0].string;}
    break;
case 64:
#line 204 "param_parse.y"
{if(Seedfile) free(Seedfile); Seedfile=yyvsp[-2].string; if(yyvsp[0].value) *ran_flag|=1; else *ran_flag&=~1;}
    break;
case 65:
#line 205 "param_parse.y"
{if(Seedfile) free(Seedfile); Seedfile=yyvsp[0].string;}
    break;
case 66:
#line 206 "param_parse.y"
{if(Seedfile) free(Seedfile); Seedfile=yyvsp[-2].string; if(yyvsp[0].value) *ran_flag|=1; else *ran_flag&=~1;}
    break;
case 67:
#line 207 "param_parse.y"
{
			 if(yyvsp[0].value<0) yyerror("Seedvalue out of range");
			 else {
				 init_ranf(yyvsp[0].value);
				 *ran_flag|=2;
			 }
		 }
    break;
case 68:
#line 215 "param_parse.y"
{set_map_range(yyvsp[-3].string,yyvsp[-2].rvalue,yyvsp[0].rvalue,-1); free(yyvsp[-3].string);}
    break;
case 69:
#line 216 "param_parse.y"
{set_map_range(yyvsp[-3].string,yyvsp[-2].rvalue,yyvsp[0].rvalue,X_PAT); free(yyvsp[-3].string); }
    break;
case 70:
#line 217 "param_parse.y"
{set_map_range(yyvsp[-3].string,yyvsp[-2].rvalue,yyvsp[0].rvalue,X_MAT); free(yyvsp[-3].string); }
    break;
case 71:
#line 218 "param_parse.y"
{set_map_range(0,yyvsp[0].rvalue,yyvsp[0].rvalue,-1);}
    break;
case 72:
#line 219 "param_parse.y"
{set_map_range(0,yyvsp[-2].rvalue,yyvsp[0].rvalue,-2);}
    break;
case 73:
#line 220 "param_parse.y"
{set_map_range(0,yyvsp[0].rvalue,yyvsp[0].rvalue,X_PAT);}
    break;
case 74:
#line 221 "param_parse.y"
{set_map_range(0,yyvsp[0].rvalue,yyvsp[0].rvalue,X_MAT);}
    break;
case 75:
#line 222 "param_parse.y"
{set_map_range(0,yyvsp[0].rvalue,yyvsp[0].rvalue,-1);}
    break;
case 76:
#line 223 "param_parse.y"
{set_map_range(0,yyvsp[-2].rvalue,yyvsp[0].rvalue,-2);}
    break;
case 77:
#line 224 "param_parse.y"
{set_map_range(0,yyvsp[0].rvalue,yyvsp[0].rvalue,X_PAT);}
    break;
case 78:
#line 225 "param_parse.y"
{set_map_range(0,yyvsp[0].rvalue,yyvsp[0].rvalue,X_MAT);}
    break;
case 79:
#line 226 "param_parse.y"
{map_function=MAP_HALDANE;}
    break;
case 80:
#line 227 "param_parse.y"
{map_function=MAP_KOSAMBI;}
    break;
case 81:
#line 230 "param_parse.y"
{set_tloci(-1,yyvsp[0].value);}
    break;
case 82:
#line 231 "param_parse.y"
{set_tloci(-2,yyvsp[0].value);}
    break;
case 83:
#line 232 "param_parse.y"
{set_tloci(yyvsp[-2].value,yyvsp[0].value);}
    break;
case 84:
#line 233 "param_parse.y"
{tloci_mean=yyvsp[0].rvalue; tloci_mean_set=1;}
    break;
case 85:
#line 236 "param_parse.y"
{ num_iter=yyvsp[0].value; }
    break;
case 86:
#line 238 "param_parse.y"
{sample_freq[0]=sample_freq[1]=yyvsp[0].value;}
    break;
case 87:
#line 239 "param_parse.y"
{sample_freq[0]=yyvsp[-2].value; sample_freq[1]=yyvsp[0].value;}
    break;
case 88:
#line 240 "param_parse.y"
{if(Freqfile) free(Freqfile); Freqfile=yyvsp[0].string;}
    break;
case 89:
#line 241 "param_parse.y"
{sample_freq[0]=sample_freq[1]=yyvsp[0].value;}
    break;
case 90:
#line 242 "param_parse.y"
{sample_freq[0]=yyvsp[-2].value; sample_freq[1]=yyvsp[0].value;}
    break;
case 91:
#line 243 "param_parse.y"
{Output_Phen=yyvsp[0].string; }
    break;
case 92:
#line 244 "param_parse.y"
{set_output_gen(yyvsp[0].string,0);}
    break;
case 93:
#line 245 "param_parse.y"
{set_output_gen(yyvsp[-2].string,yyvsp[0].string); free(yyvsp[0].string); }
    break;
case 95:
#line 247 "param_parse.y"
{output_type=yyvsp[0].value;}
    break;
case 96:
#line 248 "param_parse.y"
{if(Outputfile) free(Outputfile); Outputfile=yyvsp[0].string;}
    break;
case 97:
#line 249 "param_parse.y"
{if(OutputPosfile) free(OutputPosfile); OutputPosfile=yyvsp[0].string;}
    break;
case 98:
#line 250 "param_parse.y"
{if(OutputIBDfile) free(OutputIBDfile); OutputIBDfile=yyvsp[0].string;}
    break;
case 99:
#line 251 "param_parse.y"
{if(OutputIBDdir) free(OutputIBDdir); OutputIBDdir=yyvsp[0].string;}
    break;
case 100:
#line 252 "param_parse.y"
{if(Dumpfile) free(Dumpfile); Dumpfile=yyvsp[0].string;}
    break;
case 101:
#line 253 "param_parse.y"
{dump_freq=yyvsp[0].value;}
    break;
case 102:
#line 254 "param_parse.y"
{output_haplo=1;if(Haplofile) free(Haplofile); Haplofile=yyvsp[0].string;}
    break;
case 103:
#line 255 "param_parse.y"
{output_haplo=1;}
    break;
case 104:
#line 256 "param_parse.y"
{output_recomb=1;if(Recombfile) free(Recombfile); Recombfile=yyvsp[0].string;}
    break;
case 105:
#line 257 "param_parse.y"
{if(Polyfile) free(Polyfile); Polyfile=yyvsp[0].string;}
    break;
case 106:
#line 258 "param_parse.y"
{set_ibd_mode(yyvsp[0].string); free(yyvsp[0].string);}
    break;
case 107:
#line 259 "param_parse.y"
{set_ibd_mode(yyvsp[-2].string); free(yyvsp[-2].string); set_ibd_mode(yyvsp[0].string); free(yyvsp[0].string);}
    break;
case 108:
#line 262 "param_parse.y"
{ if(!check_variance(yyvsp[0].rvalue,0)) {res_var_set[0]=start_flag; residual_var[0]=yyvsp[0].rvalue;} }
    break;
case 109:
#line 263 "param_parse.y"
{ if(yyvsp[-1].value>=0 && !check_variance(yyvsp[0].rvalue,1)) {res_var_set[yyvsp[-1].value]=start_flag; BB(residual_var,yyvsp[-1].value,yyvsp[-1].value)=yyvsp[0].rvalue;} }
    break;
case 110:
#line 266 "param_parse.y"
{ if(!check_variance(yyvsp[0].rvalue,0)) residual_var_limit[0]=yyvsp[0].rvalue; }
    break;
case 111:
#line 267 "param_parse.y"
{ if(yyvsp[-1].value>=0 && !check_variance(yyvsp[0].rvalue,0)) residual_var_limit[yyvsp[-1].value]=yyvsp[0].rvalue; }
    break;
case 112:
#line 268 "param_parse.y"
{ if(!check_variance(yyvsp[0].rvalue,0)) residual_var_limit[0]=yyvsp[0].rvalue; }
    break;
case 113:
#line 269 "param_parse.y"
{ if(yyvsp[-1].value>=0 && !check_variance(yyvsp[0].rvalue,0)) residual_var_limit[yyvsp[-1].value]=yyvsp[0].rvalue; }
    break;
case 114:
#line 272 "param_parse.y"
{ if(!check_variance(yyvsp[0].rvalue,0)) {add_var_set[0]=start_flag; additive_var[0]=yyvsp[0].rvalue;} }
    break;
case 115:
#line 273 "param_parse.y"
{ if(yyvsp[-1].value>=0 && !check_variance(yyvsp[0].rvalue,0)) {add_var_set[yyvsp[-1].value]=start_flag; BB(additive_var,yyvsp[-1].value,yyvsp[-1].value)=yyvsp[0].rvalue;} }
    break;
case 116:
#line 276 "param_parse.y"
{ if(!check_variance(yyvsp[0].rvalue,0)) additive_var_limit[0]=yyvsp[0].rvalue; }
    break;
case 117:
#line 277 "param_parse.y"
{ if(yyvsp[-1].value>=0 && !check_variance(yyvsp[0].rvalue,0)) additive_var_limit[yyvsp[-1].value]=yyvsp[0].rvalue; }
    break;
case 118:
#line 278 "param_parse.y"
{ if(!check_variance(yyvsp[0].rvalue,0)) additive_var_limit[0]=yyvsp[0].rvalue; }
    break;
case 119:
#line 279 "param_parse.y"
{ if(yyvsp[-1].value>=0 && !check_variance(yyvsp[0].rvalue,0)) additive_var_limit[yyvsp[-1].value]=yyvsp[0].rvalue; }
    break;
case 120:
#line 282 "param_parse.y"
{ if(n_models>1) yyerror("Model must be specified when multiple models are present"); 
	                      else {grand_mean_set[0]=start_flag; grand_mean[0]=yyvsp[0].rvalue; } }
    break;
case 121:
#line 284 "param_parse.y"
{ if(yyvsp[-1].value>=0) {grand_mean_set[yyvsp[-1].value]=start_flag; grand_mean[yyvsp[-1].value]=yyvsp[0].rvalue;} }
    break;
case 122:
#line 287 "param_parse.y"
{ set_position(yyvsp[-1].lk_var,yyvsp[0].rvalue,yyvsp[0].rvalue); }
    break;
case 123:
#line 288 "param_parse.y"
{ sex_map=1; set_position(yyvsp[-3].lk_var,yyvsp[-2].rvalue,yyvsp[0].rvalue); }
    break;
case 124:
#line 291 "param_parse.y"
{c_flag=0;}
    break;
case 126:
#line 292 "param_parse.y"
{c_flag=1;}
    break;
case 128:
#line 293 "param_parse.y"
{c_flag=1;}
    break;
case 130:
#line 296 "param_parse.y"
{yyval.value=find_trait(yyvsp[0].lk_var);}
    break;
case 131:
#line 298 "param_parse.y"
{ yyval.lk_var=find_var(yyvsp[0].string,0,0); free(yyvsp[0].string);}
    break;
case 132:
#line 299 "param_parse.y"
{ yyval.lk_var=find_var(yyvsp[-3].string,yyvsp[-1].value,1); free(yyvsp[-3].string);}
    break;
case 133:
#line 302 "param_parse.y"
{ if(yyvsp[0].lk_var) {set_output(yyvsp[0].lk_var); free(yyvsp[0].lk_var);} }
    break;
case 134:
#line 303 "param_parse.y"
{ if(yyvsp[0].lk_var) {set_output(yyvsp[0].lk_var); free(yyvsp[0].lk_var);} }
    break;
case 135:
#line 306 "param_parse.y"
{ freq_marker=check_marker(yyvsp[0].lk_var); }
    break;
case 136:
#line 308 "param_parse.y"
{group_ptr=0;}
    break;
case 137:
#line 308 "param_parse.y"
{if(group_var && group_ptr<group_var->n_levels) print_scan_err("Line %d: Too few groups in order statement\n",lineno1);}
    break;
case 138:
#line 311 "param_parse.y"
{ yyval.value=find_group(yytext,yyvsp[0].value,1); }
    break;
case 139:
#line 312 "param_parse.y"
{ yyval.value=find_group(yytext,0,0); }
    break;
case 140:
#line 313 "param_parse.y"
{ yyval.value=find_group(yyvsp[0].string,0,0); free(yyvsp[0].string);}
    break;
case 141:
#line 316 "param_parse.y"
{set_group_order(yyvsp[0].value);}
    break;
case 142:
#line 317 "param_parse.y"
{set_group_order(yyvsp[0].value);}
    break;
case 143:
#line 320 "param_parse.y"
{ yyval.value=find_allele(yytext,freq_marker,yyvsp[0].value,1); }
    break;
case 144:
#line 321 "param_parse.y"
{ yyval.value=find_allele(yytext,freq_marker,0,0); }
    break;
case 145:
#line 322 "param_parse.y"
{ yyval.value=find_allele(yyvsp[0].string,freq_marker,0,0); free(yyvsp[0].string); }
    break;
case 146:
#line 325 "param_parse.y"
{group_counter=0; freq_allele=yyvsp[-1].value;}
    break;
case 147:
#line 325 "param_parse.y"
{if(freq_marker && group_var && group_counter<group_var->n_levels) print_scan_err("Line %d: Too few frequencies specified\n",lineno);}
    break;
case 148:
#line 326 "param_parse.y"
{group_counter=0; freq_allele=yyvsp[-1].value;}
    break;
case 149:
#line 326 "param_parse.y"
{if(freq_marker && group_var && group_counter<group_var->n_levels) print_scan_err("Line %d: Too few frequencies specified\n",lineno);}
    break;
case 150:
#line 329 "param_parse.y"
{set_freq(freq_marker,yyvsp[0].rvalue,freq_allele);}
    break;
case 151:
#line 330 "param_parse.y"
{group_counter++;}
    break;
case 152:
#line 331 "param_parse.y"
{set_freq(freq_marker,yyvsp[0].rvalue,freq_allele);}
    break;
case 153:
#line 332 "param_parse.y"
{group_counter++;}
    break;
case 154:
#line 335 "param_parse.y"
{ yyval.rlist=add_ibd_list(yyvsp[0].rvalue,0); }
    break;
case 155:
#line 336 "param_parse.y"
{ yyval.rlist=add_ibd_list(yyvsp[0].rvalue,yyvsp[-2].rlist); }
    break;
case 157:
#line 340 "param_parse.y"
{ yyval.rvalue=(double)yyvsp[0].value; }
    break;
}

#line 705 "/usr/local/share/bison/bison.simple"


  yyvsp -= yylen;
  yyssp -= yylen;
#if YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;
#if YYLSP_NEEDED
  *++yylsp = yyloc;
#endif

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[YYTRANSLATE (yychar)]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[YYTRANSLATE (yychar)]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* defined (YYERROR_VERBOSE) */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*--------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action |
`--------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;
      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;


/*-------------------------------------------------------------------.
| yyerrdefault -- current state does not do anything special for the |
| error token.                                                       |
`-------------------------------------------------------------------*/
yyerrdefault:
#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */

  /* If its default is to accept any token, ok.  Otherwise pop it.  */
  yyn = yydefact[yystate];
  if (yyn)
    goto yydefault;
#endif


/*---------------------------------------------------------------.
| yyerrpop -- pop the current state because it cannot handle the |
| error token                                                    |
`---------------------------------------------------------------*/
yyerrpop:
  if (yyssp == yyss)
    YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#if YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "Error: state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

/*--------------.
| yyerrhandle.  |
`--------------*/
yyerrhandle:
  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

/*---------------------------------------------.
| yyoverflowab -- parser overflow comes here.  |
`---------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}
#line 343 "param_parse.y"


static int find_trait(struct lk_variable *lkv)
{
	int i,type,mod;
	struct Variable *var;
	
	if(!n_models || (lkv->type!=LK_TYPE_IDVAR && lkv->type!=LK_TYPE_NONIDVAR)) {
		yyerror("Not a trait variable");
		return -1;
	}
	for(mod=0;mod<n_models;mod++) {
		type=models[mod].var.type;
		i=models[mod].var.var_index;
		var=(type&ST_CONSTANT)?id_variable+i:nonid_variable+i;
		if(var==lkv->var.var) break;
	}
	if(mod==n_models) {
		yyerror("Not a trait variable");
		mod= -1;
	}
	return mod;
}

static void set_analyze(char *p)
{
	int i;
	char *com[]={"AFFECTED","NULL","IBD",0};
		
	if(p) {
		i=0;
		while(com[i]) {
			if(!strcasecmp(com[i],p)) break;
			i++;
		}
		if(com[i]) analysis|=(1<<i);
		else yyerror("Invalid parameter to analyze statement");
	}
}

static void set_ibd_mode(char *p)
{
	int i;
	char *com[]={"LOKI","MERLIN","SOLAR","SINGLEPOINT","SINGLE","SINGLE_POINT",0};
		
	if(p) {
		i=0;
		while(com[i]) {
			if(!strcasecmp(com[i],p)) break;
			i++;
		}
		if(com[i]) {
			if(i<3) ibd_mode=i;
			else ibd_mode |=4;
		} else yyerror("Unknown ibd mode");
	}
}

static void set_group_order( int gp)
{
	int i;
	
	if(!group_var || gp<0) return;
	for(i=0;i<group_ptr;i++) if(group_order[i]==gp)	{
		yyerror("Group repeated in order statement");
		return;
	}
	if(group_ptr>=group_var->n_levels) yyerror("Too many groups - internal error?");
	else group_order[group_ptr++]=gp;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "set_output_gen"
static void set_output_gen(char *file,char *link)
{
	int i;
	struct output_gen *p;

	if(link)	{
		for(i=0;i<n_links;i++) if(!strcasecmp(link,linkage[i].name)) break;
		if(i==n_links) return;
		i++;
	} else i=0;
	p=Output_Gen;
	if(!(Output_Gen=malloc(sizeof(struct output_gen)))) ABT_FUNC(MMsg);
	Output_Gen->next=p;
	Output_Gen->file=file;
	Output_Gen->link_group=i;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "add_ibd_list"
static struct IBD_List *add_ibd_list(double x,struct IBD_List *p)
{
	if(!p) {
		if(!(p=malloc(sizeof(struct IBD_List)))) ABT_FUNC(MMsg);
		p->idx=0;
		p->size=32;
		if(!(p->pos=malloc(sizeof(double)*p->size))) ABT_FUNC(MMsg);
	}
	if(p->size==p->idx) {
		p->size*=2;
		if(!(p->pos=realloc(p->pos,sizeof(double)*p->size))) ABT_FUNC(MMsg);
	}
	p->pos[p->idx++]=x;
	return p;
}

static int check_link_name(char *name,int flag)
{
	int i=0;
	
	if(name)	{
		for(i=0;i<n_links;i++) if(!strcasecmp(name,linkage[i].name)) break;
		if(i==n_links) {
			(void)fprintf(stderr,"Warning: linkage group %s not found; IBD positions ignored\n",name);
			i= -1;
		}
	} else {
		if(n_links>1 && !flag) {
			i= -1;
			(void)fprintf(stderr,"Warning: linkage group not specified when number of linkage groups >1; IBD positions ignored\n");
		}
	}
	return i;
}

static void check_previous_list(int i)
{
	if(linkage[i].ibd_est_type) {
		(void)fprintf(stderr,"Warning: overwriting previous IBD settings for linkage group %s\n",linkage[i].name);
		if(linkage[i].ibd_list) {
			free(linkage[i].ibd_list->pos);
			free(linkage[i].ibd_list);
		}
		linkage[i].ibd_list=0;
		linkage[i].ibd_est_type=0;
	}
}

static void set_ibd_list(char *name,struct IBD_List *p,int type)
{
	int i=0,k;
	
	if(type==IBD_EST_GRID) {
		i=-1;
		if(p->idx<3) (void)fprintf(stderr,"Warning: too few parameters (%d) for IBD Grid (3 required); IBD request ignored\n",p->idx);
		else if(p->idx>3) (void)fprintf(stderr,"Warning: too many parameters (%d) for IBD Grid (3 required); IBD request ignored\n",p->idx);
		else if(fabs(p->pos[2])<IBD_MIN_GRID_STEP) (void)fprintf(stderr,"Warning: step size (%g) for IBD Grid < IBD_MIN_GRID_STEP (%g) in loki_ibd.h ; IBD request ignored\n",p->pos[2],IBD_MIN_GRID_STEP);
		else {
			k=1+(int)(.5+(p->pos[1]-p->pos[0])/p->pos[2]);
			if(k>IBD_MAX_GRID) (void)fprintf(stderr,"Warning: grid evaluations requested (%d) for IBD Grid > IBD_MAX_GRID (%d) in loki_ibd.h ; IBD request ignored\n",k,IBD_MAX_GRID);
			else i=0;
		}
	}
	if(!i) i=check_link_name(name,0);
	if(i<0) {
		free(p->pos);
		free(p);
	} else {
		check_previous_list(i);
		linkage[i].ibd_list=p;
		linkage[i].ibd_est_type=type;
	}
}

static void set_ibd_markers(char *name) 
{
	int i;
	
	i=check_link_name(name,1);
	if(i>=0) {
		for(;i<n_links;i++) {
			check_previous_list(i);
			linkage[i].ibd_est_type=IBD_EST_MARKERS;
			if(name) break;
		}
	}
}

static void set_tloci(int a,int b)
{
	if(a<0) {
		if(a== -1) min_tloci=max_tloci=b;
		else start_tloci=b;
	} else if(a<b) {
		min_tloci=a;
		max_tloci=b;
	} else {
		min_tloci=b;
		max_tloci=a;
	}
}

static struct Marker *check_marker(struct lk_variable *lkvar)
{
	struct Marker *mk=0;
	
	if(!lkvar) return 0;
	if(lkvar->type!=LK_TYPE_MARKER)
		yyerror("Attempting to set frequency of a non-marker");
	else mk=lkvar->var.marker;
	free(lkvar);
	return mk;
}

static void set_output(struct lk_variable *lkvar)
{
	int i,j,type,mod;
	struct Variable *var;
	
	if(!lkvar) return;
	for(mod=0;mod<n_models;mod++) {
		for(i=0;i<models[mod].n_terms;i++) {
			type=models[mod].term[i].vars[0].type;
			j=models[mod].term[i].vars[0].var_index;
			if(type&ST_MARKER) {
				if(lkvar->type==LK_TYPE_MARKER && lkvar->var.marker==marker+j) {
					models[mod].term[i].out_flag=1;
				}
			} else if(type&(ST_TRAITLOCUS|ST_ID|ST_SIRE|ST_DAM)) continue;
			else {
				if(type&ST_CONSTANT) var=id_variable+j;
				else var=nonid_variable+j;
				if((lkvar->type==LK_TYPE_IDVAR || lkvar->type==LK_TYPE_NONIDVAR) && lkvar->var.var==var) {
					models[mod].term[i].out_flag=1;
				}
			}
		}
	}
}

static void set_map_range( char *name,double r1,double r2, int flag)
{
	int i;
	double t;
	static char *sexstr[2]={"female","male"};
	
	if(flag!= -1) sex_map=1;
	
	if(name)	{
		for(i=0;i<n_links;i++) if(!strcasecmp(name,linkage[i].name)) {
			if(r2<r1) {
				t=r2;
				r2=r1;
				r1=t;
			}
			if(flag== -1) {
				linkage[i].r1[0]=linkage[i].r1[1]=r1;
				linkage[i].r2[0]=linkage[i].r2[1]=r2;
				linkage[i].range_set[0]=linkage[i].range_set[1]=1;
				(void)printf("Map range for linkage group '%s' set to %g-%gcM\n",name,r1,r2);
			} else {
				linkage[i].r1[flag]=r1;
				linkage[i].r2[flag]=r2;
				linkage[i].range_set[flag]=1;
				(void)printf("Map range (%s) for linkage group '%s' set to %g-%gcM\n",sexstr[flag],name,r1,r2);
			}
			break;
		}
	} else {
		if(flag<0) {
			total_maplength[X_PAT]=r1;
			total_maplength[X_MAT]=r2;
			(void)printf("Total (genome) map length set to (%g,%g)cM\n",r1,r2);
		} else {
			total_maplength[flag]=r1;
			(void)printf("Total (genome) %s map length set to %gcM\n",sexstr[flag],r1);
		}
	}
}

static int find_group( char *p,int gp, int flag)
{
	int i,j;
	char *s;
	
	if(!group_var) return -1;
	j=group_var->n_levels;
	
	if(group_var->rec_flag==ST_STRING) {
		for(i=0;i<j;i++) if(!(strcasecmp(p,group_var->recode[i].string))) return i;
	} else {
		if(!flag) {
			gp=strtol(p,&s,10);
			if(!(*s)) for(i=0;i<j;i++) if(gp==group_var->recode[i].value) return i;
		} else for(i=0;i<j;i++) if(gp==group_var->recode[i].value) return i;
	}
	yyerror("Group not found\n");
	return -1;
}

static int find_allele( char *p, struct Marker *mk,int all, int flag)
{
	int i,j;
	char *s;
	
	if(!mk) return -1;
	j=extra_allele_flag?mk->locus.n_alleles:mk->locus.n_alleles-1;
	
	if(mk->rec_flag==ST_STRING) {
		for(i=0;i<j;i++) if(!(strcmp(p,mk->recode[i].string))) return i;
	} else {
		if(!flag) {
			all=strtol(p,&s,10);
			if(!(*s)) for(i=0;i<j;i++) if(all==mk->recode[i].value) return i;
		} else for(i=0;i<j;i++) if(all==mk->recode[i].value) return i;
	}
	return -1;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "find_var"
static struct lk_variable *find_var( char *p, int idx, int flag)
{
	int i,j=0;
	struct lk_variable *lkv=0;
	
	if(flag) if(idx<1) {
		yyerror("Illegal array index");
		return 0;
	}
	for(i=0;i<n_markers;i++) if(!strcasecmp(marker[i].name,p)) {
		if(flag && idx!=marker[i].index) continue;
		j=LK_TYPE_MARKER;
		break;
	}
	if(!j) for(i=0;i<n_id_records;i++) if(!strcasecmp(id_variable[i].name,p)) {
		if(flag && idx!=id_variable[i].index) continue;
		j=LK_TYPE_IDVAR;
		break;
	}
	if(!j) for(i=0;i<n_nonid_records;i++) if(!strcasecmp(nonid_variable[i].name,p)) {
		if(flag && idx!=nonid_variable[i].index) continue;
		j=LK_TYPE_NONIDVAR;
		break;
	}
	if(!j && !flag) for(i=0;i<n_links;i++) if(!strcasecmp(linkage[i].name,p)) {
		j=LK_TYPE_LINK;
		break;
	}
	if(j) {
		if(!(lkv=malloc(sizeof(struct lk_variable)))) ABT_FUNC(MMsg);
		lkv->type=j;
		switch(j) {
		 case LK_TYPE_MARKER:
			lkv->var.marker=marker+i;
			break;
		 case LK_TYPE_LINK:
			lkv->var.link=linkage+i;
			break;
		 case LK_TYPE_IDVAR:
			lkv->var.var=id_variable+i;
			break;
		 case LK_TYPE_NONIDVAR:
			lkv->var.var=nonid_variable+i;
			break;
		}
	}
	return lkv;
}

static void set_freq(struct Marker *mk, double freq,int allele)
{
	static int fg,fg1;
	int i=0;
	
	group_counter++;
	if(mk) {
		if(group_var) {
			if(!group_ptr)	{
				if(!fg) yyerror("Genetic group order not set");
				fg=1;
				return;
			}
			if(group_counter>group_var->n_levels) {
				if(!fg1) yyerror("Too many frequencies specified (only 1 per genetic group)");
				fg1=1;
				return;
			}
			i=group_order[group_counter-1];
			if(i<0) return;
		}
		if(freq<0.0) {
			yyerror("Invalid (negative) frequency");
			return;
		}
		if(allele>=0 && freq==0.0)	{
			yyerror("Can not set frequency of observed allele to zero\n");
			return;
		}
		if(allele>=0) {
			mk->locus.freq[i][allele]=freq;
		} else {
			if(extra_allele_flag) {
				yyerror("Cannot set frequency of unknown allele\n");
				return;
			}
			allele=mk->locus.n_alleles-1;
			mk->locus.freq[i][allele]+=freq;
		}
		mk->freq_set[i][allele]=start_flag;
		mk->count_flag[i]=c_flag;
	}
	fg1=0;
}

static void set_position(struct lk_variable *lkvar, double pos1, double pos2)
{
	if(lkvar) {
		if(lkvar->type!=LK_TYPE_MARKER) {
			yyerror("Attempting to set position of a non-marker");
			return;
		}
		lkvar->var.marker->locus.pos[X_PAT]=pos1;
		lkvar->var.marker->locus.pos[X_MAT]=pos2;
		lkvar->var.marker->pos_set=start_flag;
		free(lkvar);
	}
}

static int check_variance(const double v,const int fg)
{
	if(v<=0.0) {
		yyerror("Variance must be positive");
		return 1;
	}
	if(n_models>1 && !fg) {
		yyerror("Must specify which model when multiple models are present");
		return 1;
	}
	return 0;
}

void print_scan_err(char *fmt, ...)
{
	va_list args;
	
	va_start(args,fmt);
	(void)vfprintf(stderr,fmt,args);
	va_end(args);
	if((++scan_error_n)>=max_scan_errors) abt(__FILE__,__LINE__,"Too many errors - aborting\n");
}

void print_scan_warn(char *fmt, ...)
{
	va_list args;
	
	if(scan_warn_n<max_scan_warnings)
	{
		va_start(args,fmt);
		(void)vfprintf(stderr,fmt,args);
		va_end(args);
	}
	scan_warn_n++;
}

void yyerror(char *s)
{
     int i;
	
	print_scan_err("Line %d: %s\n%s\n",lineno,s,linebuf);
	if(scan_error_n<=max_scan_errors)
	{
		for(i=1;i<tokenpos;i++) (void)putc('-',stderr);
		(void)fputs("^\n",stderr);
	}
}

int yywrap(void)
{
	return 1;
}

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "ReadParam"
int ReadParam(FILE *fptr,char *cname,int *flag)
{
	int i,j;
	void yy_cleanup(void);
	
	fname_list[0]=cname;
	list_ptr=0;
	ran_flag=flag;
	for(i=0;i<NUM_SYSTEM_VAR;i++) syst_var[i].flag=0;
	for(i=0;i<n_id_records;i++) if(id_variable[i].type&ST_GROUP) {
		group_var=id_variable+i;
		j=group_var->n_levels;
		if(!(group_order=malloc(sizeof(int)*j))) ABT_FUNC(MMsg);
		break;
	}
	yyin=fptr;
	if((i=yyparse())) {
	  (void)fprintf(stderr,"Error: yyparse returned error %d\n",i);
	  scan_error_n++;
	}
	yy_cleanup();
	if(group_order) free(group_order);
	if(start_tloci<0) start_tloci=min_tloci;
	else if(start_tloci<min_tloci || start_tloci>max_tloci) {
		(void)fprintf(stderr,"ReadParam(): Starting no. trait loci (%d) is outside set range (%d-%d)\n",start_tloci,min_tloci,max_tloci);
		scan_error_n++;
	}
	if(n_models>1 && output_type<2) {
		(void)fprintf(stderr,"ReadParam(): Ouput type %d not supported with multilpe trait loci\n",output_type);
		scan_error_n++;
	}
	if(!syst_var[SYST_IBD_OUTPUT].flag) {
		j=ibd_mode;
		if(compress_ibd) j|=COMPRESS_IBD;
		if(j) {
			syst_var[SYST_IBD_OUTPUT].flag=ST_INTEGER;
			syst_var[SYST_IBD_OUTPUT].data.value=j;
		}
	}
	if(scan_error_n) return 1;
	return 0;
}
