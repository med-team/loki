#ifndef BISON_Y_TAB_H
# define BISON_Y_TAB_H

#ifndef YYSTYPE
typedef union  {
	char *string;
	int value;
	double rvalue;
	struct IBD_List *rlist;
	struct lk_variable *lk_var;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
# define	RESIDUAL	257
# define	GENETIC	258
# define	VARIANCE	259
# define	POSITION	260
# define	FREQUENCY	261
# define	VIRTUAL	262
# define	START	263
# define	MEAN	264
# define	ITERATIONS	265
# define	SAMPLE	266
# define	FROM	267
# define	OUTPUT	268
# define	MAP	269
# define	TOTAL	270
# define	SEED	271
# define	SFILE	272
# define	SEEDFILE	273
# define	TRAIT	274
# define	LOCI	275
# define	SET	276
# define	SYSTEM_VAR	277
# define	TIMECOM	278
# define	ESTIMATE	279
# define	IBD	280
# define	GROUP	281
# define	ORDER	282
# define	MALE	283
# define	FEMALE	284
# define	LIMIT	285
# define	AFFECTED	286
# define	PHENO	287
# define	GENO	288
# define	COUNTS	289
# define	DUMP	290
# define	TYPE	291
# define	ANALYZE	292
# define	NORMAL	293
# define	STUDENT_T	294
# define	HAPLO	295
# define	INCLUDE	296
# define	FUNCTION	297
# define	HALDANE	298
# define	KOSAMBI	299
# define	RECOMB	300
# define	POLYGENIC	301
# define	MARKERS	302
# define	GRID	303
# define	COMPRESS	304
# define	DIR	305
# define	STRING	306
# define	INTEGER	307
# define	REAL	308


extern YYSTYPE yylval;

#endif /* not BISON_Y_TAB_H */
