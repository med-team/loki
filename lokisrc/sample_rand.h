#define RES_PRIOR_VC0 1.0
#define RES_PRIOR_SC0 1.0
#define RES_PRIOR_VA0 1.0
#define RES_PRIOR_SA0 1.0

extern double **c_var;
extern unsigned long *rand_flag;
extern int n_random;
extern struct Variable **rand_list;

extern void init_rand(void),free_rand(void),sample_rand(void);
extern void sample_additive_var(void);
