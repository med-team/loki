/****************************************************************************
 *                                                                          *
 *     Loki - Programs for genetic analysis of complex traits using MCMC    *
 *                                                                          *
 *                   Simon Heath - MSKCC                                    *
 *                                                                          *
 *                       August 2000                                        *
 *                                                                          *
 * kinship.c:                                                               *
 *                                                                          *
 * Calculate kinship coefficients                                           *
 *                                                                          *
 * Copyright (C) Simon C. Heath 1997, 2000, 2002                            *
 * This is free software.  You can distribute it and/or modify it           *
 * under the terms of the Modified BSD license, see the file COPYING        *
 *                                                                          *
 ****************************************************************************/

#include <stdio.h>

#include "utils.h"
#include "loki.h"
#include "kinship.h"

double kinship(const int a, const int b)
{
	if(!(a&&b)) return 0.0;
	if (a==b) return 0.5+0.5*kinship(id_array[a-1].sire,id_array[a-1].dam);
	if(!id_array[a-1].sire) {
		if(b<a) return 0.0;
		if(!id_array[b-1].sire) return 0.0;
		return (kinship(a,id_array[b-1].sire)+kinship(a,id_array[b-1].dam))*0.5;
	}
	if(!id_array[b-1].sire) {
		if(a<b) return 0.0;
		return (kinship(b,id_array[a-1].sire)+kinship(b,id_array[a-1].dam))*0.5;
	}
	if(a<b) return (kinship(a,id_array[b-1].sire)+kinship(a,id_array[b-1].dam))*0.5;
	return (kinship(b,id_array[a-1].sire)+kinship(b,id_array[a-1].dam))*0.5;
}

