/****************************************************************************
 *                                                                          *
 *     Loki - Programs for genetic analysis of complex traits using MCMC    *
 *                                                                          *
 *             Simon Heath - University of Washington                       *
 *                                                                          *
 *                       July 1997                                          *
 *                                                                          *
 * loki.h:                                                                  *
 *                                                                          *
 ****************************************************************************/

#include "loki_struct.h"
#include "lk_long.h"

#define LUMPED_ALLELE "__LUMPED__"

#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif

struct Id_Record {
	int *pruned_flag; /* Pruning on a marker x marker basis */
	int *kids;
	double **res,**cens,**vv,*bv;
	double *bvsum,*bvsum2;
	struct id_data *data,**data1;
	double tp[4],tpp[2][2];
	lk_ulong *temp;
	int sire,dam;
	int comp; /* Component */
	int fam_code; /* Original family designation */
	int ngens;
	int rfp;
	int sex,affected,group;
	int nkids;
	int flag;
	int n_rec;
	int allele[2];
	int *lumped,*nhaps;
};

struct output_gen {
	struct output_gen *next;
	char *file;
	int link_group;
};

struct peel_mem
{
	struct fset *s0;
	int *s1;
	double *s2;
	lk_ulong *s3;
	int *s4;
	void **s5;
	void **s6;
	double *s7;
};

struct loki_data {
	char *file_prefix;
	char *Filter,*Seedfile;
	unsigned int RunID;
	int nrm_flag;
	int multiple_rec;
	int map_function,singleton_flag,est_aff_freq;
};

union arg_type {
	char *string;
	int value;
};

struct Id_Recode {
	union arg_type *recode;
	int flag;
};

struct IBD_List {
	double *pos;
	int idx,size;
};

struct Locus {
	double pos[2];
	int *gt; /* genotypes */
	int *seg[2]; /* segregation pattern */
	int *genes[2]; /* Founder genes */
	double *lk_store;
	double *variance;
	double **freq;
	double *aff_freq,*diff_freq;
	int n_alleles; /* In entire pedigree */
	int link_group;
	int flag;
};

struct Marker {
	struct Locus locus;
	char *name;
	int **group;
	double **counts;
	struct Model_Term **mterm;
	int *haplo;
	int *count_flag;
	union arg_type *recode;
	signed char **freq_set;
	int *m_flag;
	int *n_all1;   /* In each component */
	int **nhaps,**lump,*ngens;
	lk_ulong **temp;
	int index;
	int rec_flag;
	int lumped;
	int pos_set;
};

struct TraitLocus {
	struct Locus locus;
	double **eff;
	double dom_par;
	unsigned long model_flag; /* Which models are affected by this locus? */
};

struct Link {
	char *name;
	int *mk_index;
	struct IBD_List *ibd_list;
	double r1[2],r2[2]; /* Map range */
	int ibd_est_type;
	int n_markers;
	int type;
	int sample_pos;
	int range_set[2];
};

struct Variable {
	char *name;
	union arg_type *recode;
	int type;
	int n_levels,index,rec_flag;
};

struct Model_Var {
	int type;
	int var_index;
};

struct Model_Term {
	double *eff;
	struct Model_Var *vars;
	int n_vars;
	int df;
	int out_flag;
};

struct Model {
	struct Model_Term *term;
	struct Model_Var var;
	int n_terms;
	int polygenic_flag;
};

extern void sample_segs(void);
extern void ReadBinFiles(char **,int);
extern int ReadParam(FILE *,char *,int *);
extern void AllocLokiStruct(void);
extern void AllocEffects(void);
extern void print_marker_name(FILE *,const int);
extern size_t print_orig_id(FILE *,const int);
extern size_t print_orig_id1(FILE *,const int);
extern size_t print_orig_family(FILE *,const int,const int);
extern void print_orig_triple(FILE *,const int);
extern void print_orig_allele_id(FILE *,const int);
extern size_t get_max_idlen(void);
extern void LokiSetup(void);
extern void FreeStuff(void);
extern void InitValues(int *);
extern void SampleLoop(struct peel_mem *,int *,int,int,loki_time *);
extern void delete_traitlocus(const int);
extern void print_allele_type1(FILE *,const int,const int);
extern void print_allele_type(FILE *,const int,const int,const int);
extern int get_new_traitlocus(const int);
extern void loki_identity(double *,int,int);
extern double phi2(int,int);
extern char *fget_line(FILE *);
extern char *Filter,*Seedfile,*Output_Phen,*Dumpfile,*Outputfile,*Freqfile,*Haplofile,*Polyfile;
extern char *OutputPosfile,*OutputIBDfile,*OutputIBDdir;
extern unsigned int RunID;
extern int ped_size,n_markers,n_links,n_id_records,n_nonid_records,polygenic_flag,bv_iter,n_tloci,sex_map;
extern int n_genetic_groups,n_comp,*comp_ngenes,*comp_start,*comp_size,*comp_npeel,max_tloci,min_tloci,start_tloci,extra_allele_flag;
extern int catch_sigs,sig_caught,use_student_t,output_haplo,family_id,n_models,limit_timer_type;
extern struct lmon_param *lpar;

extern struct remember *RemBlock;
extern struct Id_Record *id_array;
extern struct Id_Recode id_recode,fam_recode;
extern struct Marker *marker;
extern struct Link *linkage;
extern struct Variable *id_variable,*nonid_variable;
extern struct Model *models;
extern struct SparseMatRec **AIMatrix;
extern struct TraitLocus *tlocus;
extern struct output_gen *Output_Gen;
extern double mjrgene_var,*residual_var,*additive_var,*residual_var_limit,*additive_var_limit,*grand_mean,total_maplength[2];
extern double *tau,*tau_beta,tloci_mean,lm_ratio,limit_time;
extern int *res_var_set,*add_var_set,*grand_mean_set,num_iter,sample_from[2],sample_freq[2],tlocus_flag,tloci_mean_set;
extern int no_overdominant,censored_flag,censor_mode,tau_mode,*debug_level;
extern int *peel_trace,dump_freq,output_type,analysis;
extern int multiple_rec,**founder_flag;
extern int map_function,singleton_flag,est_aff_freq;
extern int has_orig_id(int);
extern int genv_out; /* for stat 5 EWD */

#define MAP_HALDANE 0
#define MAP_KOSAMBI 1

struct move_stats {
	int success;
	int n;
};

extern struct move_stats move_stats[];

#define N_MOVE_STATS 8
#define NUM_SYSTEM_VAR 12 /* increse for stat5 EWD */

extern struct id_data syst_var[];

#define SYST_NO_OVERDOMINANT 0
#define SYST_TAU 1
#define SYST_TAU_MODE 2
#define SYST_CENSOR_MODE 3
#define SYST_DEBUG_LEVEL 4
#define SYST_LM_RATIO 5
#define SYST_PEEL_TRACE 6
#define SYST_BACKUPS 7
#define SYST_SI_MODE 8
#define SYST_IBD_OUTPUT 9
#define SYST_RNG 10
#define SYST_GENV_OUT 11 /* EWD - added to indicate
                            whether or not to output trait genotype
                            vectors for stat 5 computation */


#define DEFAULT_ANALYSIS 0
#define AFFECTED_ANALYSIS 1
#define NULL_ANALYSIS 2
#define IBD_ANALYSIS 4
#define ESTIMATE_IBD 8

#define OUTPUT_TYPE_ORIGINAL 0
#define OUTPUT_VERSION_2_1 1
#define OUTPUT_VERSION_2_2 2
#define OUTPUT_VERSION_2_3 3
#define DEFAULT_OUTPUT_TYPE OUTPUT_VERSION_2_3
