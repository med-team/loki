void start_monitor(void);
void reaper(int);
void ignore_handler(int);

struct lmon_param {
	double extra_time,extra_utime;
	clock_t utime;
	int command;
	unsigned int magic;
	int it;
	int nq,nq1;
	int num_iter;
	int sample_from[2],sample_freq[2];
	int debug_level;
	int peel_trace;
	int si_mode;
	int ibd_mode;
	int dbr_flag;
	int dbr_shm_id;
	size_t dbr_mem_size;
};

extern int child_alive,lmon_shm_id;

#define LMON_WIN_SIZE 64
#define LMON_MAGIC 17062000
#define LMON_START_DBR 1
