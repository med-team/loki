#define RES_PRIOR_V0 1.0
#define RES_PRIOR_S0 1.0

extern double Calc_Res_Ratio(double,double),Calc_Resprop(void);
extern double Calc_CensResLike(void),Calc_ResLike(void);
extern double Sample_ResVar(void);
extern double Calc_Var_Prior(double);
extern double Recalc_Res(int);

extern double res_prior_konst;
