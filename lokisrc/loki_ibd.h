void free_IBD(void);
void set_n_ibd_rec(int);
int get_n_ibd_rec(void);
int SetupIBD(void);
void Handle_IBD(void);
double score_ibd(int,int *,int,int,int *,double *,int);
void get_founders(unsigned long **,int **,int **,int **);
void Output_Sample_IBD(int,int);
void Output_Merlin_IBD(int,int);
void Output_Solar_IBD(int,int,int *);
void get_founder_params(unsigned long **,int **,int **,int **);
int write_ibd_dump(FILE *,int);
int read_ibd_dump(FILE *,int *,char *);

#define IBD_MIN_GRID_STEP .0001
#define IBD_MAX_GRID 10000

#define IBD_EST_DISCRETE 1
#define IBD_EST_MARKERS 2
#define IBD_EST_GRID 3

#define DEFAULT_IBD_MODE 0
#define MERLIN_IBD_MODE 1
#define SOLAR_IBD_MODE 2
#define IBD_MODE_MASK 3
#define IBD_SINGLE_POINT 4
#define COMPRESS_IBD 8
